/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file dispatch.cpp
 *
 * Dispatch engine.
 */

#include "fmt/format.h"

#include "dispatch.h"

#include <objects.h>
#include <script.h>
#include <utility>
#include <video.h>

#include "config.h"
#include "util.h"
#include "assets.h"
#include "game.h"

#include "../games/catalog.h"

// ===================================================================================================================
// GSDispatch
//

void gs::GSDispatch::prerender() const
{
    // Do not do anything dependent on state here.
    SDL_SetRenderDrawColor(context_->get_renderer(), 0, 0, 0, 255);
    SDL_RenderClear(context_->get_renderer());

    const SDL_Rect src = {0, 0, context_->get_main_config()->x_resolution, context_->get_main_config()->y_resolution };
    auto background = context_->get_game()->get_background();
    SDL_RenderCopy(context_->get_renderer(), background->get_texture(), nullptr, & src );
}

void gs::GSDispatch::postrender() const
{
    context_->get_object_manager()->game_loop_action();

    // Render video last
    if (context_->get_video_manager()->get_texture() != nullptr)
    {
        context_->get_video_manager()->play_loop();
        SDL_RenderCopy(context_->get_renderer(), context_->get_video_manager()->get_texture(), NULL,
                       context_->get_video_manager()->get_rect());
    }

}

void gs::GSDispatch::complete_render() const
{
    SDL_RenderPresent(context_->get_renderer());
}

gs::GSGameState gs::GSDispatch::live_render(const GSGameState state)
{
    GSGameState new_state = GSGameState::NEW;
    if (frame_done_time_ < SDL_GetTicks())  // Guard against underflow.
    {
        reset_frame_time();  // Eat into the next frame.
        prerender();
        new_state = context_->get_game()->game_loop(state);
        postrender();
        complete_render();
    }

    return new_state;
}

void  gs::GSDispatch::reset_frame_time()
{
    frame_done_time_ = SDL_GetTicks() + milliseconds_per_frame;
}

void gs::GSDispatch::setup_coin_key()
{
    if (context_->get_main_config()->coin_key > 0)
    {
        coin_key_ = context_->get_main_config()->coin_key;
    }
}

void gs::GSDispatch::entry_point(std::shared_ptr<GSContext> context)
{
    context_ = std::move(context);
    pending_quit_key = false;

    setup_coin_key();

    SDL_Event event;
    GSGameState state = GSGameState::NEW;

    // Get entrypoint game.
    context_->switch_to_game(context_->get_main_config()->entry_game);

    // Primary game loop
    context_->set_state(GSSystemState::RUN_LOOP);
    reset_frame_time();
    while(context_->get_state() != GSSystemState::QUIT_ORDERED) {

        // SDL events
        while (SDL_PollEvent(& event) != 0)
        {

            if (event.type == SDL_QUIT)
            {
                context_->order_quit();
                break;
            }
            if(event.type == SDL_KEYDOWN)
            {
                // Quit confirmed.
                if (pending_quit_key && (event.key.keysym.sym == special_confirm_key)) {
                    return;
                }

                // Quit pending.
                if (event.key.keysym.sym == special_quit_key) {
                    pending_quit_key = true;
                    continue;
                }
                pending_quit_key = false;

                // Coin is a special event.
                if (event.key.keysym.sym == coin_key_) {
                    context_->add_coins(1);

                    // TODO doesn't really belong here.
                    Mix_PlayChannel(3, context_->get_asset_manager()->get_named_audio("bellding")->get_chunk(),0);
                }


                bool is_bound = context_->get_loaded_game()->config->keys.contains(event.key.keysym.sym);
                if (is_bound)
                {
                    auto kbind = context_->get_loaded_game()->config->keys[event.key.keysym.sym];
                    if (!kbind->signal.empty())
                    {

                        if (context_->get_wait_signal() != "")
                        {
                            if (kbind->signal[0] == context_->get_wait_signal())
                            {
                                context_->wait_signal("");
                            }
                        }
                        else if (!context_->get_game()->is_game_frozen())
                        {
                            context_->get_game()->offer_signal(kbind->signal);
                        }

                    }
                    if (!kbind->key_script.empty())
                    {
                        context_->get_script_manager()->run(kbind->key_script);
                    }
                }

                /*

                // Not frozen and is it bound.
                bool frozen = context_->get_game()->is_game_frozen();
                bool is_bound = context_->get_loaded_game()->config->keys.contains(event.key.keysym.sym);
                if (!frozen && is_bound)
                {
                    auto kbind = context_->get_loaded_game()->config->keys[event.key.keysym.sym];
                    if (!kbind->signal.empty())
                    {
                        if ( kbind->signal[0] != context_->get_wait_signal())
                        {
                            context_->get_game()->offer_signal(kbind->signal);
                        }
                        else
                        {
                            context->wait_signal("");
                        }

                    }
                    if (!kbind->key_script.empty())
                    {
                        context_->get_script_manager()->run(kbind->key_script);
                    }
                }
                */

                // Default processing.
                context_->get_game()->offer_key(event.key.keysym.sym);

            } // end if quit

        } // SDL end of events

        // Let script engine run.
        if (context_->get_wait_signal() == "")
        {
            context_->get_script_manager()->slice();
        }

        //  Render time.
        state = live_render(state);

        // TODO this should be scaled base on how much work has been done.
        SDL_Delay(20);

        if (state == GSGameState::ALLDONE)
        {
            // Game has ordered quit.
            context_->set_state(GSSystemState::QUIT_ORDERED);
        }

    }
}

