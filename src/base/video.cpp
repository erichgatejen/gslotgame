/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021, 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file video.cpp
 *
 * Video player
 */

#include "stdio.h"

#include <SDL.h>

#include "video.h"
#include "../../3rdparty/theora.h"

#include "fmt/format.h"

theora_t gs_video_player_ctx = { 0 };

void gs::VideoManager::play_video(const Video& video, const pixel_size x_loc, const pixel_size y_loc)
{
    if (target_texture_ != nullptr)
    {
        throw new std::runtime_error(fmt::format("Cannot play video {} because on is already playing.", video.name));
    }

    file_ = std::fopen((context_->get_root_path() + '/' + video.path + video_file_suffix).c_str(), "rb");
    if (! file_) {
        throw new std::runtime_error(fmt::format("Could not open video file {}.", video.path));
    }

    theora_start(& gs_video_player_ctx, file_);
    if (gs_video_player_ctx.hasVideo)
    {
        target_texture_ = SDL_CreateTexture(context_->get_renderer(), SDL_PIXELFORMAT_IYUV,
                                     SDL_TEXTUREACCESS_STREAMING, gs_video_player_ctx.w, gs_video_player_ctx.h);
        target_rect_.x = x_loc;
        target_rect_.y = y_loc;
        //target_rect_.w = video.x_size;
        //target_rect_.h = video.y_size;
        target_rect_.w = gs_video_player_ctx.w;
        target_rect_.h = gs_video_player_ctx.h;

        music_ = Mix_LoadMUS((context_->get_root_path() + '/' + video.path + audio_file_suffix).c_str());
        if(music_ == nullptr)
        {
            throw std::runtime_error(fmt::format("Unable to load audio for video {} : {}.", video.path, Mix_GetError()) );
        }

        Mix_PlayMusic(music_, 0);
    }
    else
    {
        target_texture_ = nullptr;
        throw new std::runtime_error(fmt::format("No video present in video file {}.", video.path));
    }
}

bool gs::VideoManager::play_loop()
{
    if (target_texture_ == nullptr)
    {
        return false;
    }

    if ((! theora_playing(& gs_video_player_ctx) || (gs_video_player_ctx.vhead == gs_video_player_ctx.vtail))  )
    {
        theora_stop(& gs_video_player_ctx);
        fclose(file_);

        SDL_DestroyTexture(target_texture_);
        target_texture_ = nullptr;

        if(music_ != nullptr)
        {
            Mix_FreeMusic(music_);
        }

        return false;
    }

    theora_video(& gs_video_player_ctx, target_texture_);
    return true;
}


