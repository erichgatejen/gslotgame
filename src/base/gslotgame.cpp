/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file gslotgame.cpp
 *
 * Main entrypoint.
 */

#include <iostream>
#include "gslotgame.h"

#include <assets.h>
#include <dispatch.h>

#include "config.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <SDL_image.h>

// ===================================================================================================================
// GSlotGame
//

void gs::GSlotGame::init() const
{

    // All persistent data should go into the context and destroyed by the context destructor.
    auto r = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    if(r < 0)
    {
        std::string error(SDL_GetError());
        throw std::runtime_error("Could not initialize SDL.  " + error);
    }
    SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );

    if(TTF_Init() < 0)
    {
        throw std::runtime_error("Could not initialize TTF : " + *TTF_GetError());
    }

    #ifdef APPLE
    auto window = SDL_CreateWindow( "GSLOTGAME", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0,
                                    0, SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP );
    #else
    auto window = SDL_CreateWindow( "GSLOTGAME", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0,
        SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS);
    #endif

    Mix_Init(MIX_INIT_MP3);
    if( Mix_OpenAudio( 44100, AUDIO_S16SYS, number_audio_channels, 4096 ) < 0 )
    {
        throw std::runtime_error("Could not initialize SDL: " + *Mix_GetError());
    }
    //Mix_AllocateChannels(gs::number_audio_channels);

    //SDL_DisplayMode display_mode;
    //SDL_GetDesktopDisplayMode(0, & display_mode);
    //display_mode.h = gs::gs_window_size_y;
    //display_mode.w = gs::gs_window_size_x;

    //SDL_SetWindowDisplayMode(window, & display_mode);
    //SDL_Delay(100);
    //SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    //SDL_ShowWindow(window);

    int x;
    int y;
    SDL_GL_GetDrawableSize(window, &x, &y);
    std::cout << "X: " << x << std::endl;
    std::cout << "Y: " << y << std::endl;

    if(window == nullptr)
    {
        throw std::runtime_error("Could not create main window: " + *SDL_GetError());
    }

    auto renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
    if(renderer == nullptr)
    {
        throw std::runtime_error("Could not create renderer: " + *SDL_GetError());
    }
    context_->set_window(window);
    context_->set_renderer(renderer);

    SDL_RenderSetLogicalSize(renderer, this->context_->get_main_config()->x_resolution,
        this->context_->get_main_config()->y_resolution);

    context_->set_state(GSSystemState::INIT_COMPLETE);
}

void gs::GSlotGame::setup()
{
 //   loader_ = std::make_shared<gs::GSLoader>();
    //context_->setup(root_path, loader_);
}

void gs::GSlotGame::load_assets() const
{
    auto config_assets = std::make_shared<ConfigAssets>(this->context_->get_root_path());
    context_->get_asset_manager()->load(config_assets);
    context_->set_state(GSSystemState::LOAD_COMPLETE);
}

void gs::GSlotGame::load_games()
{
  //  json metadata;
  //  loader_->load(context_, game_registration_catalog);
  //  context_->set_game(context_->get_config()->start_game);
  //  context_->get_game()->context_switch(metadata);
}

void gs::GSlotGame::close()
{
    // -- Dispose context.  This will take out assets too ---
    context_ = nullptr;

    // -- Shut down subsystems  ------------------------------
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

    //context_->set_state(GSSystemState::CLOSE_COMPLETE);
}

void gs::GSlotGame::entry_point(const std::filesystem::path& path)
{
    try
    {
        // Load main config
        auto config_main = std::make_shared<ConfigMain>(path);

        // Setup global video context
        auto video_context = std::make_shared<GSVideoContext>();

        // Setup context
        context_ = std::make_shared<GSContext>(config_main, video_context);
        context_->init();

        // Prep the various managers
        init();
        setup();
        load_assets();
        load_games();

        GSDispatch dispatch;
        dispatch.entry_point(context_);
    }
    catch (std::runtime_error & e)
    {
        std::cout << "Exception stopped application." << std::endl << e.what() << std::endl;
    }

    close();
}
