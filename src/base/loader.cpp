/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file loader.cpp
 *
 * Loader.
 */

#include "fmt/format.h"

#include "loader.h"
#include "game.h"
#include "../games/catalog.h"

gs::GSLoadedGame::GSLoadedGame(const std::shared_ptr<ConfigGame>& config, const std::shared_ptr<GSGame>& game) :
    config(config), game(game) {};

std::shared_ptr<gs::GSLoadedGame> gs::GSLoader::get_game(const std::string& name)
{
    if (currently_loaded_.contains(name))
    {
        return  this->currently_loaded_[name];
    }

    // Is it configured?  Not caching these for now as each game will be loaded only once.
    std::shared_ptr<ConfigGame> gconfig;
    try
    {
        gconfig = std::make_shared<ConfigGame>(this->context_->get_main_config()->root_path, name+".json");
    }
    catch (std::exception & e)
    {
        throw std::runtime_error("Could not load game '" + name + "' : " + e.what());
    }

    // Build it
    auto game = get_instance(name, gconfig->implementation, this->context_);
    if (game == nullptr)
    {
        throw std::runtime_error("Implementation '" + gconfig->implementation + "' for game '" + name + "' does not exist." );
    }

    game->init(GSGameState::NEW, gconfig);
    auto gload = std::make_shared<GSLoadedGame>(gconfig, game);
    this->currently_loaded_[name] = gload;

    return gload;
}
