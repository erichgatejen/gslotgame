/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file game.cpp
 *
 * Game stuff.
 */

#include "game.h"

#include <utility>

// ===================================================================================================================
// GAME
//

/**
 * WARNING: Do not do anything that relies on the context in the constructor.  It will not be fully loaded at this point.
 * Do it in init_impl() instead.
 */
gs::GSGame::GSGame(std::string registration_name, std::shared_ptr<GSContext> context) : state_(),
    context_(std::move(context)),
    registration_name_(std::move(registration_name))
{
}

void gs::GSGame::init(const GSGameState state, const std::shared_ptr<ConfigGame>& config)
{
    this->name_current_background_ = config->background;
    this->init_impl(state, config);
}

std::shared_ptr<gs::GSTexture> gs::GSGame::get_background()
{
    if (frozen_texture_background_ != nullptr)
    {
        return frozen_texture_background_;
    }

    if (name_current_background_ == name_texture_background_)
    {
        return texture_background_;
    }

    name_texture_background_ = name_current_background_;
    texture_background_ = context_->get_asset_manager()->get_background_texture(name_current_background_);
    return this->texture_background_;
}

gs::GSGameState gs::GSGame::game_loop(const GSGameState state)
{
    if (!is_game_frozen())
    {
        return game_loop_impl(state);
    }
    return GSGameState::FROZEN;
}

