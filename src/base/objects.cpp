/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021, 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file objects.cpp
 *
 * Objects and handlers.
 */

#include "objects.h"
#include "util.h"


void find_rates(float* scale_rate_x, float* scale_rate_y, const gs::pixel_size start_x, const gs::pixel_size start_y,
    const gs::pixel_size target_x, const gs::pixel_size target_y, const uint rate_x)
{
    // TODO convert the rates to float instead of int.

    auto const dist_x = abs(target_x - start_x);
    if (auto const dist_y = abs(target_y - start_y); dist_x != dist_y)
    {

        if (dist_x == 0)
        {
            *scale_rate_x = 0;
            *scale_rate_y = rate_x;
        }
        else if (dist_y == 0)
        {
            *scale_rate_x = rate_x;
            *scale_rate_y = 0;
        }
        else if (dist_x > dist_y)
        {
            auto slew = static_cast<float>(dist_y) / static_cast<float>(dist_x);
            *scale_rate_x = rate_x;
            *scale_rate_y = static_cast<float>(rate_x) * slew;
        }
        else
        {
            auto slew = static_cast<float>(dist_x) / static_cast<float>(dist_y);
            *scale_rate_x = static_cast<float>(rate_x) * slew;
            *scale_rate_y = rate_x;
        }
    }
    else
    {
        *scale_rate_x = rate_x;
        *scale_rate_y = rate_x;
    }

    //
    if (start_x > target_x)
    {
        *scale_rate_x = 0 - *scale_rate_x;
    }
    if (start_y > target_y)
    {
        *scale_rate_y = 0 - *scale_rate_y;
    }
}


gs::GSObjectOrder gs::GSObjectOrder::get_move_order(const pixel_size start_x, const pixel_size start_y, const pixel_size target_x,
                                                     const pixel_size target_y, const uint rate_x, const time_value start_time,
                                                     const time_value end_time)
{
    GSObjectMover mover{};
    find_rates(&mover.rate_x, &mover.rate_y, start_x, start_y, target_x, target_y, rate_x);
    mover.current_x = start_x;
    mover.current_y = start_y;
    mover.target_x = target_x;
    mover.target_y = target_y;

    GSObjectOrder result;
    result.type = GSObjectOrderType::MOVE;
    result.oper = mover;
    result.start_time = start_time;
    result.end_time = end_time;
    return result;
}

gs::GSObjectOrder gs::GSObjectOrder::get_scale_order(const pixel_size start_x, const pixel_size start_y, const pixel_size target_x,
                                                     const pixel_size target_y, const uint rate_x, const time_value start_time,
                                                     const time_value end_time)
{
    GSObjectScaler scaler{};
    find_rates(&scaler.rate_x, &scaler.rate_y, start_x, start_y, target_x, target_y, rate_x);
    scaler.current_x = start_x;
    scaler.current_y = start_y;
    scaler.target_x = target_x;
    scaler.target_y = target_y;

    GSObjectOrder result;
    result.type = GSObjectOrderType::SCALE;
    result.oper = scaler;
    result.start_time = start_time;
    result.end_time = end_time;
    return result;
}

// ===================================================================================================================
// OBJECT
//

gs::GSObject::GSObject(const std::shared_ptr<GSContext>& context, const std::string& name, std::shared_ptr<GSTexture> texture,
                      const uint priority) :
    context_(context), name_(name), texture_(texture), priority_(priority)
{
    type_ = GSObjectType::STATIC;
    show_ = false;
    x_ = 0;
    y_ = 0;
    x_size_ = texture->get_width();
    y_size_ = texture->get_height();
    x_size_original_ = x_size_;
    y_size_original_ = y_size_;
}

void gs::GSObject::show(const bool show)
{
    show_ = show;
}

void gs::GSObject::hide()
{
     show_ = false;
}

void gs::GSObject::object_action() const
{
    // Switch on variant type.
    switch(texture_.index())
    {
    case 0:  // std::shared_ptr<GSTexture>
        {
            auto texture = std::get<std::shared_ptr<GSTexture>>(texture_);
            SDL_Rect src = {0, 0, x_size_original_, y_size_original_ };
            SDL_Rect dest = {x_, y_, x_size_, y_size_ };

            if (type_ == GSObjectType::CENTERED)
            {
                dest.x = dest.x - x_size_/2;
                dest.y = dest.y - y_size_/2;
            }

            SDL_RenderCopy(context_->get_renderer(), texture->get_texture(), & src, & dest );
        }
        break;
    default:
        throw std::runtime_error("BUG: object type not implemented yet.");
    }
}

void gs::GSObject::handle_order_action__move(GSObjectOrderActive * order, const time_value fixed_time)
{
    auto mover = std::get<GSObjectMover>(order->order.oper);

    if ((x_ == mover.target_x) && (y_ == mover.target_y))
    {
        order->state = GSObjectOrderState::DONE;
    }
    else
    {
        bool change = false;
        //time_value slew = fixed_time - order->last_time;

        // I'm going to brute force these for now.
        // Move X.
        if (x_ != mover.target_x)
        {
            mover.current_x += mover.rate_x;
            if (mover.rate_x < 0)
            {
                if (mover.current_x <= mover.target_x)
                {
                    x_ = mover.target_x;
                }
                else
                {
                    x_ = mover.current_x;
                }
                change = true;
            }
            else if (mover.rate_x > 0)
            {
                if (mover.current_x >= mover.target_x)
                {
                    x_ = mover.target_x;
                }
                else
                {
                    x_ = mover.current_x;
                }
                change = true;
            }
        }

        // Move Y
        if (y_ != mover.target_y)
        {
            mover.current_y += mover.rate_y;
            if (mover.rate_y < 0)
            {
                if (mover.current_y <= mover.target_y)
                {
                    y_ = mover.target_y;
                }
                else
                {
                    y_ = mover.current_y;
                }
                change = true;
            }
            else if (mover.rate_y > 0)
            {
                if (mover.current_y >= mover.target_y)
                {
                    y_ = mover.target_y;
                }
                else
                {
                    y_ = mover.current_y;
                }
                change = true;
            }
        }

        if (change)
        {
            order->last_time = fixed_time;
        }

        order->order.oper = mover;
    }
}

void gs::GSObject::handle_order_action__scale(GSObjectOrderActive * order, const time_value fixed_time)
{
    auto scaler = std::get<GSObjectScaler>(order->order.oper);

    if ((x_size_ == scaler.target_x) && (y_size_ == scaler.target_y))
    {
        order->state = GSObjectOrderState::DONE;
    }
    else
    {
        bool change = false;
        //time_value slew = fixed_time - order->last_time;

        // I'm going to brute force these for now.
        // Scale X.
        if (x_size_ != scaler.target_x)
        {
            scaler.current_x += scaler.rate_x;
            if (scaler.rate_x < 0)
            {
                if (scaler.current_x <= scaler.target_x)
                {
                    x_size_ = scaler.target_x;
                }
                else
                {
                    x_size_ = scaler.current_x;
                }
                change = true;
            }
            else if (scaler.rate_x > 0)
            {
                if (scaler.current_x >= scaler.target_x)
                {
                    x_size_ = scaler.target_x;
                }
                else
                {
                    x_size_ = scaler.current_x;
                }
                change = true;
            }
        }

        // Scale Y
        if (y_size_ != scaler.target_y)
        {
            scaler.current_y += scaler.rate_y;
            if (scaler.rate_y < 0)
            {
                if (scaler.current_y <= scaler.target_y)
                {
                    y_size_ = scaler.target_y;
                }
                else
                {
                    y_size_ = scaler.current_y;
                }
                change = true;
            }
            else if (scaler.rate_y > 0)
            {
                if (scaler.current_y >= scaler.target_y)
                {
                    y_size_ = scaler.target_y;
                }
                else
                {
                    y_size_ = scaler.current_y;
                }
                change = true;
            }
        }

        if (change)
        {
            order->last_time = fixed_time;
        }

        order->order.oper = scaler;
    }
}

void gs::GSObject::handle_order_action(GSObjectOrderActive * order, const time_value fixed_time)
{
    if (fixed_time >= order->order.end_time)
    {
        order->state = GSObjectOrderState::DONE;
    }
    else
    {
        switch(order->order.type)
        {
        case GSObjectOrderType::STATIC:
            break;

        case GSObjectOrderType::MOVE:
            handle_order_action__move(order, fixed_time);
            break;

        case GSObjectOrderType::MIGRATE:
            break;

        case GSObjectOrderType::SCALE:
            handle_order_action__scale(order, fixed_time);
            break;

        // case GSObjectOrderType::RELEASE_PENDING:
        //    order->state = GSObjectOrderState::DONE;
        //    pending_action_ = false;
        //    break;

        case GSObjectOrderType::NONE:
                break;

        default:
            assert(false);
        }
    }
}

void gs::GSObject::order_action()
{
    auto time = SDL_GetTicks();
    bool scaler_running_ = false;

    NumericallyOrderedListWalker walker(order_queue_);
    auto current = walker.next();
    while (current->order.type != GSObjectOrderType::NONE)
    {
        if (current->order.start_time <= time)
        {
            switch(current->state)
            {
            case GSObjectOrderState::WAITING:
                current->state = GSObjectOrderState::ACTIVE;
                current->last_time = time;
                // Fall through

            case GSObjectOrderState::ACTIVE:
                if (current->order.type == GSObjectOrderType::SCALE)
                {
                    if  (!scaler_running_)
                    {
                        current->state = GSObjectOrderState::ACTIVE;
                        auto bump = time - current->order.start_time;
                        current->order.start_time = time;
                        if (current->order.end_time != GS_MAX_TIME)
                        {
                            current->order.end_time += bump;
                        }
                        scaler_running_ = true;
                    }
                    handle_order_action(current, time);
                }
                else
                {
                    handle_order_action(current, time);
                }
                break;

            case GSObjectOrderState::DONE:
                walker.remove();
                break;
            }

        }
        else
        {
            break;
        }

        current = walker.next();
    }

    walker.done();
}

void gs::GSObject::game_loop_action()
{
    if (show_)
    {
        if (order_queue_ != nullptr)
        {
            order_action();
        }

        if (! pending_action_)
        {
            object_action();
        }
    }
    else
    {
        if (order_queue_ != nullptr)
        {
            // Expire any that are past due.
           // const auto time = SDL_GetTicks();
           // while(order_queue_->has_more() && order_queue_->first_numeric_value() <= time)
           // {
           //     order_queue_->pop_first();
           // }
        }
    }
}

void gs::GSObject::reset()
{
    x_size_ = x_size_original_;
    y_size_ = y_size_original_;
    type_ = GSObjectType::STATIC;
}

void gs::GSObject::accept_order(const GSObjectOrder & order)
{
    auto now = SDL_GetTicks();

    if (order_queue_ == nullptr)
    {
        GSObjectOrderActive null_value;
        null_value.order.type = GSObjectOrderType::NONE;
        order_queue_ = std::make_shared<NumericallyOrderedList<GSObjectOrderActive, time_value>>(null_value);
    }

    GSObjectOrderActive active;
    active.order = order;
    active.order.start_time = order.start_time + now;
    active.last_time = 0;
    if (order.end_time != GS_MAX_TIME)
    {
        active.order.end_time = order.end_time + now;
    }
    order_queue_->insert(active.order.start_time, active);
}

void gs::GSObject::accept_orders(object_orders & orders)
{
    for (auto order : orders)
    {
        accept_order(order);
    }
}


// ===================================================================================================================
// OBJECT MANAGER
//

gs::ObjectManager::ObjectManager(const std::shared_ptr<GSContext>& context) : context_(context)
{
    type_name_2_enum_mapping["STATIC"] = GSObjectType::STATIC;
    type_name_2_enum_mapping["CENTERED"] = GSObjectType::CENTERED;
    priority_list_ = std::make_shared<gs::NumericallyOrderedList<std::shared_ptr<GSObject>, uint>>(nullptr);
}

std::shared_ptr<gs::GSObject> gs::ObjectManager::get_object(const std::string& name, const std::string& asset_name,
                                                            const bool reset, const int x_pos, const int y_pos,
                                                            const uint priority, const int x_size, const int y_size)
{
    std::shared_ptr<GSObject> result;
    if (! is_registered(name))
    {
        auto tex = context_->get_asset_manager()->get_named_texture(asset_name);
        assert(tex != nullptr);

        result = std::make_shared<GSObject>(context_, name,tex, priority);
        register_object(result);
    }
    else
    {
        result = get(name);
        //result->pending_action(pending_action);
    }

    if ((x_pos >= 0) && (y_pos >= 0))
    {
        result->move(x_pos, y_pos);
    }

    if (reset)
    {
        result->reset();
    }

    if ((x_size >= 0) && (y_size >= 0))
    {
        result->resize(x_size, y_size);
    }

    return result;
}

// HAX  dont have time to test combining these.
std::shared_ptr<gs::GSObject> gs::ObjectManager::get_object(const std::string& name, std::shared_ptr<GSTexture> texture,
                                                            const bool reset, const int x_pos, const int y_pos,
                                                            const uint priority, const int x_size, const int y_size)
{
    std::shared_ptr<GSObject> result;
    if (! is_registered(name))
    {
        result = std::make_shared<GSObject>(context_, name, texture, priority);
        register_object(result);
    }
    else
    {
        result = get(name);
    }

    if ((x_pos >= 0) && (y_pos >= 0))
    {
        result->move(x_pos, y_pos);
    }

    if (reset)
    {
        result->reset();
    }

    if ((x_size >= 0) && (y_size >= 0))
    {
        result->resize(x_size, y_size);
    }

    return result;
}


void gs::ObjectManager::register_object(const std::shared_ptr<GSObject>& object)
{
    if (!object_map_.contains(object->get_name()))
    {
        object_map_[object->get_name()] = object;
        priority_list_->insert(object->get_priority(), object);
    }
}

void gs::ObjectManager::deregister_object(const std::string& name)
{
    if (object_map_.contains(name))
    {
        priority_list_->remove(object_map_[name]);
        object_map_.erase(name);
    }
}

void gs::ObjectManager::set_object_asset(const std::string& name, const std::string& asset_name)
{
    std::shared_ptr<GSObject> obj;
    if (is_registered(name))
    {
        obj = object_map_[name];
    }
    else
    {
        throw std::runtime_error("set_object_asset: Object does not exist:  " + name);
    }

    const auto tex = context_->get_asset_manager()->get_named_texture(asset_name);
    if (tex == nullptr)
    {
        throw std::runtime_error("set_object_asset: Asset '" + asset_name + "' does not exist for object '" + name + "'.");
    }
    obj->set_texture(tex);
}

std::shared_ptr<gs::GSObject> gs::ObjectManager::get(const std::string& name)
{
    std::shared_ptr<GSObject> result;
    if (is_registered(name))
    {
        result = object_map_[name];
    }
    return result;
}

bool gs::ObjectManager::is_registered(const std::string& name) const
{
    if (object_map_.contains(name))
    {
        return true;
    }
    return false;
}

void gs::ObjectManager::game_loop_action() const
{
    NumericallyOrderedListWalker walker(priority_list_);
    while(walker.has_more())
    {
        (*walker.next())->game_loop_action();
    }
}

gs::GSObjectType gs::ObjectManager::get_type(const std::string& type_name)
{
    if (!type_name_2_enum_mapping.contains(type_name))
    {
        throw std::runtime_error("Bad object type: " + type_name);
    }
    return type_name_2_enum_mapping[type_name];
}
