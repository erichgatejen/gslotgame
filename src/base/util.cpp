/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file util.cpp
 *
 * Utilities.
 */

#include <iostream>

#include "util.h"

#include <fstream>

// ===================================================================================================================
// JSON/Config

json gs::load_config(const std::string& name, const std::filesystem::path& path)
{
    std::ifstream json_file(path);
    if  (! json_file.is_open()) {
        throw std::runtime_error("Could not open '" + name + "' configuration file at " + path.string());
    }
    return json::parse(json_file);
}

int gs::config_get_int(json& j, const std::string& key) {
    return j.at(key).get<int>();
}

std::string gs::config_get_string(json& j, const std::string& key) {
    return j.at(key).get<std::string>();
}

// ===================================================================================================================
// Formatting

std::string gs::lazy_to_upper(const std::string_view view)
{
    std::locale locale;
    std::stringstream result;
    for(auto cr : view)
    {
        result << std::toupper(cr, locale);
    }
    return result.str();
}

// ===================================================================================================================
// General

bool gs::ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

std::vector<std::string> gs::split(const std::string& text, const char delim) {
    std::string line;
    std::vector<std::string> vec;
    std::stringstream ss(text);
    while(std::getline(ss, line, delim)) {
        vec.push_back(line);
    }
    return vec;
}
