/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file main.cpp
 *
 * Main context and entrypoint.
 */

#include <iostream>
#include "main.h"
#include "gslotgame.h"

// ===================================================================================================================
// ENTRY POINT
//

/**
 * Entry point.
 * @return
 */
int main(int argc, char * argv[])
{
    if (argc < 2)
    {
        std::cout << "Expecting path to game definition." << std::endl;
        return 1;
    }

    try
    {

        std::filesystem::path rpath(argv[1]);
        gs::GSlotGame game;
        game.entry_point(rpath);
    }
    catch (std::runtime_error& err)
    {
        std::cout << err.what() << std::endl << std::endl;
    }

    return 0;
}


