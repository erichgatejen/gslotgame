/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file config.cpp
 *
 * Main context and entrypoint.
 */

#include "config.h"
#include "util.h"

// ===================================================================================================================
// ConfigMain

gs::ConfigMain::ConfigMain(const std::filesystem::path& path)
{
    this->root_path = path;
    this->load(path);
}

void gs::ConfigMain::load(const std::filesystem::path& path)
{
    const std::filesystem::path fp(config_file_name_main);
    const std::filesystem::path fullfp(path / fp);
    try
    {

        auto jconfig = load_config("Main", fullfp);
        this->x_resolution = config_get_int(jconfig, config_main_resolution_x);
        this->y_resolution = config_get_int(jconfig, config_main_resolution_y);
        this->entry_game = jconfig[config_main_entry_game][config_name];

        if (jconfig.contains(config_main_coin_key))
        {
            if (const auto kk = jconfig[config_main_coin_key].get<std::string>(); !kk.empty())
            {
                coin_key = kk[0];
            }
        }

    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Failed to load Main config from " + fullfp.string() + " : " + e.what());
    }
}

// ===================================================================================================================
// ConfigAssets

gs::ConfigAssets::ConfigAssets(const std::filesystem::path& path)
{
    this->load(path);
}

void gs::ConfigAssets::load(const std::filesystem::path& path)
{
    const std::filesystem::path fp(config_file_name_assets);
    const std::filesystem::path fullfp(path / fp);
    try
    {

        auto jconfig = load_config("Assets", fullfp);

        // Fonts
        if (jconfig.contains(config_assets_fonts))
        {
            for (const auto& v : jconfig[config_assets_fonts].items())
            {
                this->fonts[v.key()] = ConfigNamedAsset{v.key(), std::filesystem::path(v.value())};
            }
        }

        // Backgrounds
        if (jconfig.contains(config_assets_backgrounds))
        {
            for (const auto& v : jconfig[config_assets_backgrounds].items())
            {
                this->backgrounds[v.key()] = ConfigNamedAsset{v.key(), std::filesystem::path(v.value())};
            }
        }

        // Audio
        if (jconfig.contains(config_assets_audio))
        {
            for (const auto& v : jconfig[config_assets_audio].items())
            {
                this->audio[v.key()] = ConfigNamedAsset{v.key(), std::filesystem::path(v.value())};
            }
        }

        // Textures
        if (jconfig.contains(config_assets_textures))
        {
            int idx = 1;
            for (const auto& v : jconfig[config_assets_textures].items())
            {
                try
                {
                    ConfigAssetTextureTransparency  tt;
                    tt.t = ConfigNamedAsset{v.key(), std::filesystem::path(v.value()[0])};
                    tt.r = v.value()[1].get<int>();
                    tt.g = v.value()[2].get<int>();
                    tt.b = v.value()[3].get<int>();
                    this->textures[v.key()] = tt;
                    idx++;
                } catch (std::exception& e)
                {
                    throw std::runtime_error("Failed to load Textures from " + fullfp.string() + ".  Item #" +
                        std::to_string(idx) + " broken.  : " + e.what());
                }
            }
        }

    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Failed to load Assets config from " + fullfp.string() + " : " + e.what());
    }
}

// ===================================================================================================================
// ConfigGame

gs::ConfigGame::ConfigGame(const std::filesystem::path& path, const std::string& name)
{
    this->load(path, name);
}

void gs::ConfigGame::load(const std::filesystem::path& path, const std::string& name)
{
    const std::filesystem::path fp(name);
    const std::filesystem::path fullfp(path / fp);
    try
    {

        this->game_name = name;

        auto jconfig = load_config("Game", fullfp);
        this->implementation = jconfig[config_game_implementation].get<std::string>();
        this->background = jconfig[config_game_background].get<std::string>();
        this->persist = jconfig[config_game_persist].get<bool>();

        if (jconfig.contains(config_game_keys))
        {
            int idx = 1;
            for (const auto& v : jconfig[config_game_keys].items())
            {
                try
                {
                    const auto kbind = std::make_shared<ConfigGameKeys>();
                    if (v.value().contains(config_game_keys_signal))
                    {
                        kbind->signal = split(v.value()[config_game_keys_signal].get<std::string>(),
                            script_term_delimiter);
                    }
                    if (v.value().contains(config_game_keys_script))
                    {
                        for (const auto& line : v.value()[config_game_keys_script])
                        {
                            auto pl = split(line, gs::script_term_delimiter);
                            kbind->key_script.push_back(pl);
                        }
                    }

                    for (const auto c : v.key())
                    {
                        this->keys[c] = kbind;
                    }

                    idx++;
                } catch (std::exception& e)
                {
                    throw std::runtime_error("Failed to load Game Keys from " + fullfp.string() + ".  Item #" +
                        std::to_string(idx) + " broken.  : " + e.what());
                }
            }
        }

        if (jconfig.contains(config_game_outcomes))
        {
            int idx = 1;
            for (const auto& v : jconfig[config_game_outcomes].items())
            {
                try
                {

                    auto pl = split(v.value().get<std::string>(), gs::script_term_delimiter);
                    this->outcomes.push_back(pl);

                    idx++;
                } catch (std::exception& e)
                {
                    throw std::runtime_error("Failed to load Game Outcomes from " + fullfp.string() + ".  Item #" +
                        std::to_string(idx) + " broken.  : " + e.what());
                }
            }
        }

        if (jconfig.contains(config_game_config))
        {
            int idx = 1;
            for (const auto& v : jconfig[config_game_config].items())
            {
                try
                {
                    // TODO: convert to use variants.
                    if (v.value().is_string())
                    {
                        this->configs[v.key()] = v.value().get<std::string>();
                    }
                    else if (v.value().is_number_integer())
                    {
                        this->configs[v.key()] = std::to_string(v.value().get<int>());
                    }
                    else if (v.value().is_number_float())
                    {
                        this->configs[v.key()] = std::to_string(v.value().get<float>());
                    }
                    else
                    {
                        throw std::runtime_error("Unsupported type in json.");
                    }

                    idx++;
                } catch (std::exception& e)
                {
                    throw std::runtime_error("Failed to load Game Config from " + fullfp.string() + ".  Item #" +
                        std::to_string(idx) + " broken.  Key=" + v.key() + ": " + e.what());
                }
            }
        }

        if (jconfig.contains(config_game_script))
        {
            std::string iname;
            for (const auto& v : jconfig[config_game_script].items())
            {
                try
                {
                    iname = std::string(v.key());
                    script rscript;
                    for (const auto& line : v.value().get<std::vector<std::string>>())
                    {
                        for (const auto& term : split(line, script_order_delimiter))
                        {
                            directive d = split(term, script_term_delimiter);
                            rscript.push_back(d);
                        }
                    }
                    this->scripts[v.key()] = rscript;

                } catch (std::exception& e)
                {
                    throw std::runtime_error("Failed to load Game Script from " + fullfp.string() + ".  Script named " +
                        iname + " is broken.  : " + e.what());
                }
            }
        }
    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Failed to load Game config from " + fullfp.string() + " : " + e.what());
    }
}

gs::script gs::ConfigGame::get_script(const std::string& name)
{
    if (!scripts.contains(name))
    {
        throw std::runtime_error("Requested script '" + name + "' does not exist for game '" + game_name + "'.");
    }
    return scripts[name];
}

