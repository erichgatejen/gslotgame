/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file assets.cpp
 *
 * Assets and handlers.
 */

#include "assets.h"
#include "util.h"

#include "fmt/format.h"

gs::GSTexture::GSTexture(const std::shared_ptr<GSContext>& context, const std::string file_path, int red, int green, int blue)
{
    auto surface = IMG_Load((file_path).c_str());
    if(surface == nullptr)
    {
        throw std::runtime_error( gs::format_string("Unable to load asset %s.  %s", file_path.c_str(), SDL_GetError()) );
    }

    // TODO what are we doing there?  This sets a transparent color.
    if (red >= 0)
    {
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, red, green, blue));
    }

    //Create texture from surface pixels
    texture_ = SDL_CreateTextureFromSurface(context->get_renderer(), surface);
    if(texture_ == nullptr)
    {
        throw std::runtime_error(gs::format_string("Could not load texture from file %s", file_path.c_str(),
                                                   SDL_GetError()));
    }
    else
    {
        width_ = surface->w;
        height_ = surface->h;
    }

    SDL_FreeSurface( surface );
}

gs::GSTexture::GSTexture(SDL_Texture * texture, const pixel_size width, const pixel_size height) :
        texture_(texture), width_(width), height_(height)
{
}

gs::GSTexture::~GSTexture()
{
    if(texture_ != nullptr)
    {
        SDL_DestroyTexture(texture_);
    }
}

gs::GSAudio::GSAudio(const std::shared_ptr<GSContext>& context, const std::string& asset_path)
{
    chunk_ = Mix_LoadWAV((context->get_root_path() + '/' + asset_path).c_str());
    if(chunk_ == nullptr)
    {
        throw std::runtime_error( format_string("Unable to load asset %s.  %s", asset_path.c_str(), Mix_GetError()) );
    }
}

gs::GSAudio::~GSAudio()
{
    if(chunk_ != nullptr)
    {
        Mix_FreeChunk(chunk_);
    }
}

std::shared_ptr<gs::GSTexture> gs::AssetManager::get_background_texture(const std::string& name)
{
    if (!background_textures_.contains(name))
    {
        throw std::runtime_error("Background texture named '" + name + "' does not exist.");
    }
    return background_textures_[name];
}

std::shared_ptr<gs::GSTexture> gs::AssetManager::get_named_texture(const std::string& name)
{
    if (!named_textures_.contains(name))
    {
        throw std::runtime_error("Texture named '" + name + "' does not exist.");
    }
    return named_textures_[name];
}

std::shared_ptr<gs::GSAudio> gs::AssetManager::get_named_audio(const std::string& name)
{
    if (!named_audio_.contains(name))
    {
        throw std::runtime_error("Audio named '" + name + "' does not exist.");
    }
    return named_audio_[name];
}

void gs::AssetManager::load(const std::shared_ptr<ConfigAssets>& config)
{

    // TODO : we preload all assets.  Probably shouldn't do that.

    // Backgrounds
    for(const auto& [fst, snd] : config->backgrounds)
    {
        auto tex = std::make_shared<GSTexture>(context_, this->context_->get_root_path() / snd.path);
        background_textures_[fst] = tex;
    }

    // Textures
    for(const auto& [fst, snd] : config->textures)
    {
        auto tex = std::make_shared<GSTexture>(context_, this->context_->get_root_path() / snd.t.path,
            snd.r, snd.g, snd.b);
        named_textures_[fst] = tex;
    }

    // Fonts
    for(const auto& [fst, snd] : config->fonts)
    {
        const auto font = TTF_OpenFont( std::string(context_->get_root_path() / snd.path ).c_str(), 90 );
        if (font == nullptr)
        {
            throw std::runtime_error( format_string("Unable to load font.  %s", TTF_GetError()) );
        }
        fonts_[fst] = font;
        if (fst == config_default)
        {
            default_font_ = font;
        }
    }
    if (default_font_ == nullptr)
    {
        throw std::runtime_error("You must specify a default font.");
    }

    // Audio
    for(const auto& [fst, snd] : config->audio)
    {
        auto tex = std::make_shared<GSAudio>(context_, snd.path);
        this->named_audio_[fst] = tex;
    }

    context_->set_state(GSSystemState::LOAD_COMPLETE);
}

void gs::AssetManager::dispose_assets()
{

    for(const auto& [fst, snd] : background_textures_)
    {
        SDL_DestroyTexture(snd->get_texture());
    }
    background_textures_.clear();

    for(const auto& [fst, snd] : named_textures_)
    {
        SDL_DestroyTexture(snd->get_texture());
    }
    named_textures_.clear();

    for(const auto& [fst, snd] : named_audio_)
    {
        Mix_FreeChunk(snd->get_chunk());
    }
    named_audio_.clear();

    for (const auto& [fst, snd] : fonts_)
    {
        TTF_CloseFont(snd);
    }

}

std::shared_ptr<gs::GSTexture> gs::AssetManager::register_dynamic_background(const std::string& name)
{
    // const auto surface = SDL_GetWindowSurface(context_->get_window());
    const Uint32 format = SDL_PIXELFORMAT_ARGB8888;
    SDL_Surface *surface = SDL_CreateRGBSurfaceWithFormat(0, context_->get_main_config()->x_resolution,
        context_->get_main_config()->y_resolution, 32, format);
    SDL_RenderReadPixels(context_->get_renderer(), NULL, format, surface->pixels, surface->pitch);

    auto texture = SDL_CreateTextureFromSurface(context_->get_renderer(), surface);
    SDL_FreeSurface(surface);
    background_textures_[name] = std::make_shared<GSTexture>(texture, surface->w, surface->h);
    return background_textures_[name];
}

void gs::AssetManager::destroy_dynamic_background(const std::string& name)
{
    if (!background_textures_.contains(name))
    {
        const auto texture = background_textures_[name];
        background_textures_.erase(name);
        SDL_DestroyTexture(texture->get_texture());
    }
}
