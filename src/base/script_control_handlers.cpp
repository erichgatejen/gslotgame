/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_control_handlers.cpp
 *
 * Script engine.
 */

#include "script.h"
#include "script_directive.h"
#include "script_object_handlers.h"
#include "script_control_handlers.h"
#include "config.h"
#include "game.h"
#include "util.h"

// =====================================================================================================================
// Script Manager Handlers

void gs::script_handler_transition(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line) {
    // TRANSITION [game name]
    const auto target = get_directive<std::string>(d, 1, line, "[game name]");
    context->switch_to_game(target);
}

void gs::script_handler_sleep(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // SLEEP [milliseconds]
    const auto ms = get_directive<int>(d, 1, line, "[milliseconds]");
    sc.start_delay(ms);
}

void gs::script_handler_add_coin(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // ADD_COIN [# coins]
    const auto number = get_directive<int>(d, 1, line, "[# coins]");
    context->add_coins(number);
}

void gs::script_handler_freeze(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // FREEZE
    auto txt = context->get_asset_manager()->register_dynamic_background(freeze_name);
    context->get_game()->freeze_game(txt);
}

void gs::script_handler_unfreeze(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // UNFREEZE
    context->get_asset_manager()->destroy_dynamic_background(freeze_name);
    context->get_game()->unfreeze_game();
}

void gs::script_handler_signal(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // SIGNAL
    if (d.size() > 1)
    {
        // TODO does not handle multi term signal directives.
    }
    std::vector sig {d[1]};
    context->get_game()->offer_signal(sig);
}

void gs::script_handler_wait_signal(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // WAIT_SIGNAL [signal]
    context->wait_signal(d[1]);
}


