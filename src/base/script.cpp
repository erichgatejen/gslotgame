/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script.cpp
 *
 * Script engine.
 */

#include "script.h"
#include "script_directive.h"
#include "script_object_handlers.h"
#include "script_control_handlers.h"
#include "script_media_handlers.h"
#include "config.h"
#include "util.h"


// =====================================================================================================================
// Script Manager Members

void gs::ScriptContext::start_delay(const unsigned ms)
{
    delay_done_time_ = SDL_GetTicks() + ms;
}

std::map<gs::directive_name, void (*)(std::shared_ptr<gs::GSContext>, gs::ScriptContext& sc, gs::directive&, int)> gs_script_handler_table = {
    {gs::directive_transition, gs::script_handler_transition},
    {gs::directive_background, gs::script_handler_background},
    {gs::directive_object_register, gs::script_handler_object_register},
    {gs::directive_object_show, gs::script_handler_object_show},
    {gs::directive_object_hide, gs::script_handler_object_hide},
    {gs::directive_object_move, gs::script_handler_object_move},
    {gs::directive_object_resize, gs::script_handler_object_resize},
    {gs::directive_object_scale, gs::script_handler_object_scale},
    {gs::directive_object_move_action, gs::script_handler_object_move_action},
    {gs::directive_object_deregister, gs::script_handler_object_deregister},
    {gs::directive_play_sound, gs::script_handler_play_sound},
    {gs::directive_stop_sound_channel, gs::script_handler_stop_sound_channel},
    {gs::directive_play_video, gs::script_handler_play_video},
    {gs::directive_sleep, gs::script_handler_sleep},
    {gs::directive_add_coin, gs::script_handler_add_coin},
    {gs::directive_freeze, gs::script_handler_freeze},
    {gs::directive_unfreeze, gs::script_handler_unfreeze},
    {gs::directive_signal, gs::script_handler_signal},
{gs::directive_wait_signal, gs::script_handler_wait_signal}
};

bool gs::ScriptManager::op()
{
    // Trivial dismissals.
    if (const auto done = script_context_.get_delay_done_time(); done > 0)
    {
        if (const auto ticks = SDL_GetTicks(); ticks < done)
        {
            return true;  // Still waiting for a delay to expire
        }
        script_context_.reset_delay();
    }

    if (ip_ >= current_script_.size())
    {
        return false;
    }

    directive d = current_script_[ip_];
    process_globals(d);
    ip_++;
    if (!d.empty())
    {
        if (gs_script_handler_table.contains(d[0]))
        {
            gs_script_handler_table[d[0]](context_, script_context_, d, ip_-1);
        }
        else
        {
            throw std::runtime_error("Unknown script directive: " + d[0]);
        }
    }

    if (ip_ >= (current_script_.size()))
    {
        return false;
    }

    return true;
}

void gs::ScriptManager::process_globals(directive& d) const
{
    for (int idx = 0; idx < d.size(); idx++)
    {
        if (d[idx].starts_with(gs_global_flag) && d[idx].size() > 1)
        {
            d[idx] = context_->get_global(d[idx].substr(1));
        }
        else if  (d[idx].starts_with(gs_global_flag_num) && d[idx].size() > 1)
        {
            d[idx] = context_->get_global_num(d[idx].substr(1));
        }
    }
}

void gs::ScriptManager::run(const script& script) {
    script_queue_.push(script);
}

void gs::ScriptManager::slice() {

    if (is_running_) {
        is_running_ = op();
    }
    else
    {
        if (!script_queue_.empty()) {
            current_script_ = script_queue_.front();
            script_queue_.pop();
            is_running_ = true;
            ip_ = 0;
        }
    }

}

// =====================================================================================================================
// Script Utils

void gs::throw_bad_directive(const int index, const std::string& name, const int line, const std::string& expected,
    const std::string& cause = "")
{
    if (cause.empty())
    {
        throw std::runtime_error("Script error.  Directive '" + name + "' at line # " + std::to_string(line) +
           " incorrect.  Expecting '" + expected + "' at term # :" + std::to_string(index) );
    }
    else
    {
        throw std::runtime_error("Script error.  Directive '" + name + "' at line # " + std::to_string(line) +
            " incorrect.  Expecting '" + expected + "' at term # :" + std::to_string(index) + ".  Caused by: " + cause);
    }
}

template <>
std::string gs::get_directive<std::string>(directive& d, const int index, const int line,
    const std::string& expected)
{
    if (d.empty())
    {
        return "";
    }
    if (index >= d.size())
    {
        throw_bad_directive(index, d[0], line, expected);
    }
    return d[index];
}

template <>
int gs::get_directive<int>(directive& d, const int index,  const int line,
    const std::string& expected)
{
    if (d.empty())
    {
        return 0;
    }
    if (index >= d.size())
    {
        throw_bad_directive(index, d[0], line, expected);
    }

    int result = 0;
    try
    {
        result = std::stoi(d[index]);
    }
    catch (std::exception& e)
    {
        throw_bad_directive(index, d[0], line, expected, e.what());
    }

    return result;
}


//     auto frame_start_time = SDL_GetTicks();
