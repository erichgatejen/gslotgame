/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_object_handlers.cpp
 *
 * Script engine.
 */

#include "script.h"
#include "script_directive.h"
#include "script_object_handlers.h"
#include "config.h"
#include "util.h"

// =====================================================================================================================
// Script Manager Handlers

std::shared_ptr<gs::GSObject> gs::find_object(const std::shared_ptr<GSContext>& context, const std::string& object_name,
    const int line, const std::string& name)
{
    auto obj = context->get_object_manager()->get(object_name);
    if (obj == nullptr)
    {
        throw std::runtime_error("Script error.  Directive '" + name + "' at line # " + std::to_string(line) +
    " incorrect.  Requested object '" + object_name + "' does not exist.");
    }
    return obj;
}

void gs::script_handler_object_register(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_REGISTER [object name] [asset name] [x pos] [y pos] [priority] [optional: type]

    const auto object_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto asset_name = get_directive<std::string>(d, 2, line, "[asset name]");
    const auto x_pos = get_directive<int>(d, 3, line, "[x pos]");
    const auto y_pos = get_directive<int>(d, 4, line, "[y pos]");
    const auto priority = get_directive<int>(d, 4, line, "[priority]");

    auto obj = context->get_object_manager()->get_object(object_name, asset_name, true,
        x_pos, y_pos, priority);
    // TODO getting it will register it too.  Is that what we want?
    //context->get_object_manager()->register_object(obj);

    if (d.size() > 6)
    {
        obj->config(context->get_object_manager()->get_type(d[6]));
    }
}

void gs::script_handler_object_show(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_SHOW [object names...]
    for (int i = 1; i < d.size(); i++)
    {
        const auto object_name = get_directive<std::string>(d, i, line, "[object name]");
        const auto obj = find_object(context, object_name, line, d[0]);
        obj->show();
    }
}

void gs::script_handler_object_hide(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_HIDE [object names...]
    for (int i = 1; i < d.size(); i++)
    {
        const auto object_name = get_directive<std::string>(d, i, line, "[object name]");
        const auto obj = find_object(context, object_name, line, d[0]);
        obj->hide();
    }
}

void gs::script_handler_object_move(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_MOVE [object name] [x pos] [y pos]
    const auto object_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto x_pos = get_directive<int>(d, 2, line, "[x pos]");
    const auto y_pos = get_directive<int>(d, 3, line, "[y pos]");

    const auto obj = find_object(context, object_name, line, d[0]);
    obj->move(x_pos, y_pos);
}

void gs::script_handler_object_resize(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_RESIZE [object name] [x size] [y size]
    const auto object_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto x_size = get_directive<int>(d, 2, line, "[x size]");
    const auto y_size = get_directive<int>(d, 3, line, "[y size]");

    const auto obj = find_object(context, object_name, line, d[0]);
    obj->resize(x_size, y_size);
}

void gs::script_handler_object_scale(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_SCALE [object name] [start x size] [start y size] [target x size] [target y size] [rate] [optional: start time] [optional: end time]
    const auto object_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto obj = find_object(context, object_name, line, d[0]);

    const auto x_size_start = get_directive<int>(d, 2, line, "[start x size]]");
    const auto y_size_start = get_directive<int>(d, 3, line, "[start y size]");
    const auto x_size_target = get_directive<int>(d, 4, line, "[target x size]]");
    const auto y_size_target = get_directive<int>(d, 5, line, "[target y size]");
    const auto rate = get_directive<int>(d, 6, line, "[rate]");

    GSObjectOrder  order;
    if (d.size() > 9)
    {
        // With time
        const auto start_time = get_directive<int>(d, 7, line, "[optional: start time]]");
        const auto end_time = get_directive<int>(d, 8, line, "[optional: end time]]");
        order = GSObjectOrder::get_scale_order(x_size_start, y_size_start, x_size_target, y_size_target,
                                               rate, start_time, end_time);
    } else
    {
        order = GSObjectOrder::get_scale_order(x_size_start, y_size_start, x_size_target, y_size_target,
                                               rate, 0);
    }

    obj->resize(x_size_start, y_size_start);
    obj->show();
    obj->accept_order(order);
}

void gs::script_handler_object_move_action(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_MOVE_ACTION [object name] [start x pos] [start y pos] [target x pos] [target y pos] [rate] [optional: start time] [optional: end time]
    const auto object_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto obj = find_object(context, object_name, line, d[0]);

    const auto x_start = get_directive<int>(d, 2, line, "[start x size]]");
    const auto y_start = get_directive<int>(d, 3, line, "[start y size]");
    const auto x_target = get_directive<int>(d, 4, line, "[target x size]]");
    const auto y_target = get_directive<int>(d, 5, line, "[target y size]");
    const auto rate = get_directive<int>(d, 6, line, "[rate]");

    GSObjectOrder  order;
    if (d.size() > 9)
    {
        // With time
        const auto start_time = get_directive<int>(d, 7, line, "[optional: start time]]");
        const auto end_time = get_directive<int>(d, 8, line, "[optional: end time]]");
        order = GSObjectOrder::get_scale_order(x_start, y_start, x_target, y_target,
                                               rate, start_time, end_time);
    } else
    {
        order = GSObjectOrder::get_move_order(x_start, y_start, x_target, y_target,
                                               rate, 0);
    }

    obj->move(x_start, y_start);
    obj->show();
    obj->accept_order(order);
}

void gs::script_handler_object_deregister(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // OBJECT_DEREGISTER [object names...]
    for (int i = 1; i < d.size(); i++)
    {
        const auto object_name = get_directive<std::string>(d, i, line, "[object name]");
        find_object(context, object_name, line, d[0]);
        context->get_object_manager()->deregister_object(object_name);
    }
}

