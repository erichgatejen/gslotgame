/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_media_handlers.cpp
 *
 * Script engine.
 */

#include "script.h"
#include "script_directive.h"
#include "script_media_handlers.h"
#include "config.h"
#include "game.h"
#include "util.h"
#include "video.h"

// =====================================================================================================================
// Script Manager Handlers

std::shared_ptr<gs::GSAudio> gs::find_audio(const std::shared_ptr<GSContext>& context, const std::string& asset_name,
    const int line, const std::string& name)
{
    if (!context->get_asset_manager()->get_named_audios().contains(asset_name))
    {
        throw std::runtime_error("Script error.  Directive '" + name + "' at line # " + std::to_string(line) +
    " incorrect.  Requested audio asset '" + asset_name + "' does not exist.");
    }
    return context->get_asset_manager()->get_named_audio(asset_name);
}

void gs::script_handler_background(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // BACKGROUND [background name]
    const auto background_name = get_directive<std::string>(d, 1, line, "[background name]");
    context->get_game()->set_background(background_name);
}

void gs::script_handler_play_sound(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // PLAY_SOUND [asset name] [channel #] [optional: loops]

    const auto asset_name = get_directive<std::string>(d, 1, line, "[object name]");
    const auto channel = get_directive<int>(d, 2, line, "[channel #]");
    int loops = 0;
    if (d.size() > 3)
    {
        loops = get_directive<int>(d, 3, line, "[optional: loops]");
    }

    const auto audio = find_audio(context, asset_name, line, d[0]);
    Mix_PlayChannel(channel, audio->get_chunk(), loops);
}

void gs::script_handler_stop_sound_channel(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // STOP_SOUND_CHANNEL [channel #]
    const auto channel = get_directive<int>(d, 1, line, "[channel #]");
    Mix_HaltChannel(channel);
}

void gs::script_handler_play_video(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line)
{
    // PLAY_VIDEO [name] [path without file extensions]  [x_loc] [y_loc] [x_size] [y_size]
    Video vid;
    vid.name = get_directive<std::string>(d, 1, line, "[name]");
    vid.path = get_directive<std::string>(d, 2, line, "[path without file extensions]");
    auto x_loc = get_directive<int>(d, 3, line, "[x_loc]");
    auto y_loc = get_directive<int>(d, 4, line, "[y_loc]");
    vid.x_size = get_directive<int>(d, 5, line, "[x_size]");
    vid.y_size = get_directive<int>(d, 6, line, "[y_size]");

    context->get_video_manager()->play_video(vid, x_loc, y_loc);
}