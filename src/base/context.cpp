/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file context.cpp
 *
 * Main context.
 */


#include <utility>
#include <context.h>

#include <video.h>

#include <loader.h>

#include <assets.h>
#include <script.h>

// ===================================================================================================================
// GSGContext
//

gs::GSContext::GSContext(const std::shared_ptr<ConfigMain>& main_config, std::shared_ptr<GSVideoContext> video_context) :
    main_config_(main_config), video_context_(std::move(video_context))
{}

std::shared_ptr<gs::GSGame> gs::GSContext::get_game() const { return current_game_->game; }
std::shared_ptr<gs::GSLoadedGame> gs::GSContext::get_loaded_game() const { return current_game_; }

void gs::GSContext::init()
{
    assets_manager_ = std::make_shared<AssetManager>(shared_from_this());
    script_manager_ = std::make_shared<ScriptManager>(shared_from_this());
    object_manager_ = std::make_shared<ObjectManager>(shared_from_this());
    video_manager_ = std::make_shared<VideoManager>(shared_from_this());
    loader_ = std::make_shared<GSLoader>(shared_from_this());
}

gs::GSContext::~GSContext()
{
    dispose_assets();
    if (renderer_ != nullptr)
    {
        SDL_DestroyRenderer(renderer_);
        renderer_ = nullptr;
    }
    if (window_ != nullptr)
    {
        SDL_DestroyWindow(window_);
        window_ = nullptr;
    }
}

void gs::GSContext::dispose_assets() const
{
    assets_manager_->dispose_assets();
}

void gs::GSContext::set_window(SDL_Window * window)
{
    if (window_ != nullptr)
    {
        throw std::runtime_error("BUG: cannot reuse a context or set the window more than once.");
    }
    window_ = window;
}

void gs::GSContext::set_renderer(SDL_Renderer * renderer)
{
    if (renderer_ != nullptr)
    {
        throw std::runtime_error("BUG: cannot reuse a context or set the renderer more than once.");
    }
    renderer_ = renderer;
}
/*
void gs::GSGContext::setup(const std::string root_path, std::shared_ptr<GSGameProxy> game_proxy)
{
    assets_manager_->dispose_assets();
    game_proxy_ = game_proxy;
    root_path_ = root_path;

    config_ = std::make_shared<gs::GSConfig>(root_path);
}

*/
void gs::GSContext::remove_coins(const uint number)
{
    if (number > coins_)
    {
        throw std::runtime_error("BUG: tried to remove more coins than there are.");
    }
    coins_ -= number;
    coin_spoiled = true;
}

void gs::GSContext::switch_to_game(const std::string& name)
{

    if (current_game_ != nullptr)
    {
        current_game_->game->deactivate();
    }
    current_game_ = this->loader_->get_game(name);
    current_game_->game->activate();

}

std::string gs::GSContext::get_global(const std::string& name)
{
    if (!global_.contains(name))
    {
        return "";
    }
    return global_[name];
}

int gs::GSContext::get_global_num(const std::string& name)
{
    if (!global_.contains(name))
    {
        return 0;
    }
    return global_num_[name];
}

