/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file catalog.h
 *
 *  Game utils
 */


#ifndef GSLOTGAME_GAME_UTILS_H
#define GSLOTGAME_GAME_UTILS_H

#define TEXRENDER(_TEX_,_SRC_,_DEST_) SDL_RenderCopy(context_->get_renderer(), _TEX_->get_texture(), \
reinterpret_cast<const SDL_Rect *>(& _SRC_), reinterpret_cast<const SDL_Rect *>(& _DEST_));
#define TEXRENDERD(_TEX_,_DEST_) SDL_RenderCopy(context_->get_renderer(), _TEX_->get_texture(), \
nullptr, reinterpret_cast<const SDL_Rect *>(& _DEST_));
#define PLAYSOUNDASSET(__ASSET__) Mix_PlayChannel(1, asset_manager->get_named_audio(__ASSET__)->get_chunk(),0);
#define PLAYSOUNDASSETC(__ASSET__,__CHANNEL__) Mix_PlayChannel(__CHANNEL__, context_->get_asset_manager()->get_named_audio(__ASSET__)->get_chunk(),0);

namespace gs {

template <class T>
T get_outcome_value(const directive& outcome, const int spot)
{
    T v;
    return v;  // Make the IDE shut up.
}

template <>
inline std::string get_outcome_value<std::string>(const directive& outcome, const int spot)
{
    if (spot < 0 || spot >= outcome.size())
    {
        throw std::runtime_error("Outcome index out of range:  " + std::to_string(spot));
    }
    return outcome[spot];
}

template <>
inline int get_outcome_value<int>(const directive& outcome, const int spot)
{
    auto const val = get_outcome_value<std::string>(outcome, spot);
    int result;
    try
    {
        result = std::stoi(val);
    }
    catch (std::exception & e)
    {
        throw std::runtime_error("Outcome value '" + val + "' could not be taken as an integer:  " + std::string(e.what()));
    }
    return result;
}

}


#endif //GSLOTGAME_GAME_UTILS_H
