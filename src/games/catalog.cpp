/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2022
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file catalog.cpp
 *
 * Game catalog.  This is the interface between the base system and the games that will be loaded.
 */

#include "./catalog.h"

// ===================================================================================================================
// GAME CATALOG
//

std::shared_ptr<gs::GSGame> gs::get_instance(const std::string& registration_name, const std::string& impl_name,
                                        const std::shared_ptr<GSContext>& context)
{
    auto game = get_instance_common(registration_name, impl_name, context);
    if (game == nullptr)
    {
        game = get_instance_2024(registration_name, impl_name, context);
    }
    return game;
}