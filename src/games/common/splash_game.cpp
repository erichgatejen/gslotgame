/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2022
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file splash_game.h
 *
 * Simple display.
 */

#include <string>
#include <utility>

//#include <SDL.h>

#include "splash_game.h"

// ===================================================================================================================
// Simple Outcome  GAME
//

gs::common::SplashGame::SplashGame(std::string registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(std::move(registration_name), context)
{
    state_ = GSGameState::RENDERING_ACTIVE;
}

void gs::common::SplashGame::init_impl(GSGameState state, std::shared_ptr<ConfigGame> config)
{
    // NOP
}

void gs::common::SplashGame::activate()
{
    // NOP
}

void gs::common::SplashGame::deactivate()
{
    // NOP
}

bool gs::common::SplashGame::offer_signal(directive signal)
{
    state_ = GSGameState::ALLDONE;
    return false;
}

bool gs::common::SplashGame::offer_key(SDL_Keycode keycode)
{
    return false;
}

gs::GSGameState gs::common::SplashGame::game_loop_impl(const GSGameState state)
{
    return state_;
}

