/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file catalog.cpp
 *
 * Game catalog info for common.
 */

#include "../catalog.h"
#include "loader.h"

#include "splash_game.h"

// ===================================================================================================================
// GAME CATALOG
//

std::map<std::string, int> instance_map_common =
{
    { gs::catalog_game_splash_game, 0 }
};


std::shared_ptr<gs::GSGame> gs::get_instance_common(const std::string& registration_name, const std::string& impl_name,
                                                const std::shared_ptr<GSContext>& context)
{
    if (instance_map_common.contains(impl_name)) {
        switch(instance_map_common[impl_name])
        {
        case 0: // catalog_game_splash_game
            return std::make_shared<common::SplashGame>(registration_name, context);
        default:
            break;
        }
    }
    return nullptr;
}
