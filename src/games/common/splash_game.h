/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file splash_game.h
 *
 * Slash screen display.
 */

#ifndef GSLOTGAME_GCOMMON_SPLASH_GAME_H
#define GSLOTGAME_GCOMMON_SPLASH_GAME_H

#include "context.h"
#include "game.h"

namespace gs::common {

const std::string game_name_SPLASH = "common.SPLASH";

// ===================================================================================================================
// CONFIG
//

class SplashGame : public GSGame {

public:
    SplashGame(std::string registration_name, const std::shared_ptr<GSContext>& context);
    ~SplashGame() override = default;
    GSGAME_VIRTUAL_OVERRIDES
};

} // End namespace gs::common


#endif // GSLOTGAME_GCOMMON_SPLASH_GAME_H
