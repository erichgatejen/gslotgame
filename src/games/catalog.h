/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file catalog.h
 *
 *  Game catalog.  This is the interface between the base system and the games that will be loaded.
 */
#pragma once

#include "loader.h"
#include "context.h"

#ifndef GSLOTGAME_CATALOG_H
#define GSLOTGAME_CATALOG_H

namespace gs {

// ==========================================================================================================
// = Implementations - These names will go in the "implementation" field in game definition.

const std::string catalog_game_splash_game = "splash_game";

const std::string catalog_game_slot_3reel = "slot_3reel";
const std::string catalog_game_slot_4x4spot = "slot_4x4spot";
const std::string catalog_game_slot_match15 = "match_15";
const std::string catalog_game_slot_pick2 = "pick_2";
const std::string catalog_game_navigate = "navigate";

// ==========================================================================================================
// = Catalog Access

std::shared_ptr<GSGame> get_instance(const std::string& registration_name, const std::string& impl_name,
                                        const std::shared_ptr<GSContext>& context);

// Do not call these directly.
std::shared_ptr<GSGame> get_instance_common(const std::string& registration_name, const std::string& impl_name,
                                                const std::shared_ptr<GSContext>& context);
std::shared_ptr<GSGame> get_instance_2024(const std::string& registration_name, const std::string& impl_name,
                                                const std::shared_ptr<GSContext>& context);


} // end namespace gs




#endif //GSLOTGAME_CATALOG_H
