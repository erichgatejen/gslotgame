/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file navigate.h
 *
 * Navigate..
 */

#ifndef GSLOTGAME_G2022_NAVIGATE_GAME_H
#define GSLOTGAME_G2022_NAVIGATE_GAME_H

#include "context.h"
#include "game.h"

#include <bits/random.h>

#include "objects.h"

namespace gs::g2024
{

// ===================================================================================================================
// CONFIG
//

// ===================================================================================================================
// CLASSES AND FUNCTIONS
//

enum class NavigateActionState
{
    NEW,
    ENTRY_SCRIPT,
    RUNNING_ENTRY_SCRIPT,
    WAITING,
    SCRIPT_PENDING,
    SCRIPT,
    OUTCOME,
    OUTCOME_WAIT,
    DONE
};

enum class NavigateSpotType
{
    START,
    HOME,
    IMPASSABLE,
    OPEN
};

struct NavigateSpot
{
    NavigateSpotType    type;
    std::string         trigger;
    bool                boom_trigger;
    bool                visited;

    NavigateSpot() : type(NavigateSpotType::OPEN), boom_trigger(false), visited(false)
    {
    }
};

class Navigate final : public GSGame {

    pixel_size start_x_ = 0;
    pixel_size distance_x_ = 0;
    pixel_size start_y_ = 0;
    pixel_size distance_y_ = 0;
    pixel_size spot_size_x_ = 0;
    pixel_size spot_size_y_ = 0;

    int loc_x_, loc_y_;
    int observer_x_, observer_y_;

    std::vector<std::vector<std::shared_ptr<NavigateSpot>>> board_;
    std::vector<std::vector<std::shared_ptr<NavigateSpot>>> board_live_;

    std::string first_entry_script_;
    std::string entry_script_;
    std::string done_script_;

    std::string outcome_trigger_;
    directive current_outcome_;
    int       outcome_next_idx_;

    std::string pending_script_;

    GSGameState running_state_;
    NavigateActionState action_state_;

    void set_globals() const;
    void check_new_location();
    void do_outcome();
    void render_board();
    void render_remaining();
    void render_main();

public:
    Navigate(const std::string& registration_name, const std::shared_ptr<GSContext>& context);
    ~Navigate() override;
    GSGAME_VIRTUAL_OVERRIDES

};

}


#endif // GSLOTGAME_G2022_Navigate_GAME_H
