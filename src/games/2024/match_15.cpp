/****************************************************************************************************************
 * Game Slot System.
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file match_15.cpp
 *
 * Matching game with 15 slots.
 */

#include <string>

#include "config.h"

#include <SDL.h>
#include <SDL_mixer.h>

#include "match_15.h"

#include <loader.h>
#include <util.h>

#include "script.h"

#include "../utils.h"

// ===================================================================================================================
// Config, assets and tools.
//

const int number_free_spins = 5;

const std::string match1_object = "match15.match1";
const std::string match2_object = "match15.match2";

// Signals
const std::string signal_done = "DONE";
const std::string signal_left = "LEFT";
const std::string signal_right = "RIGHT";
const std::string signal_up = "UP";
const std::string signal_down = "DOWN";
const std::string signal_select = "SELECT";

// Configuration points.
const std::string config_signal_select = "signal_select";
const std::string config_global_first = "match15.first";

const std::string config_spots = "spots";
const std::string config_start_x = "start_x";
const std::string config_distance_x = "distance_x";
const std::string config_start_y = "start_y";
const std::string config_distance_y = "distance_y";
const std::string config_spot_size_x = "spot_size_x";
const std::string config_spot_size_y = "spot_size_y";
const std::string config_pick_asset_name = "pick_asset_name";

const std::string config_chooser_start_x = "chooser_start_x";
const std::string config_chooser_distance_x = "chooser_distance_x";
const std::string config_chooser_start_y = "chooser_start_y";
const std::string config_chooser_distance_y = "chooser_distance_y";
const std::string config_chooser_size_x = "chooser_size_x";
const std::string config_chooser_size_y = "chooser_size_y";
const std::string config_chooser_asset_name = "chooser_asset_name";

const std::string config_remaining_box_x = "remaining_box_x";
const std::string config_remaining_box_y = "remaining_box_y";
const std::string config_remaining_box_size_x = "remaining_box_size_x";
const std::string config_remaining_box_size_y = "remaining_box_size_y";
const std::string config_remaining_box_font_size = "remaining_box_font_size";

const std::string config_reveal_audio = "reveal_audio";

const std::string config_entry_script = "entry_script_name";
const std::string config_first_entry_script = "entry_first_script_name";  // first time the entry script is run.
const std::string config_done_script = "done_script";

// ===================================================================================================================
// Match 15
//

gs::g2024::Match15::Match15(const std::string& registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(registration_name, context), outcome_next_idx_(0), running_state_(), action_state_(Match15ActionState::NEW),
    cur_x_(0), cur_y_(0), picks_{}, remaining_texture_(nullptr)
{
}

gs::g2024::Match15::~Match15()
{
    SDL_DestroyTexture(remaining_texture_);
}

void gs::g2024::Match15::activate()
{
    if ((action_state_ == Match15ActionState::DONE)||(action_state_ == Match15ActionState::NEW))
    {
        // reset the board.
        // TODO maybe on deactivation?
        cur_x_ = 0;
        cur_y_ = 0;
        picks_.clear();
        chooser_texture_ = context_->get_asset_manager()->get_named_texture(chooser_asset_name_);
        picks_remaining_ = number_free_spins;
        picks_remaining_spoiled_ = false;

        action_state_ = Match15ActionState::ENTRY_SCRIPT;
    }
    // TODO need an error for a bad state?

    running_state_ = GSGameState::RENDERING_ACTIVE;
}

void gs::g2024::Match15::deactivate()
{
    action_state_ = Match15ActionState::DONE;
    running_state_ = GSGameState::DONE;
}

void gs::g2024::Match15::init_impl(GSGameState state, std::shared_ptr<ConfigGame> config)
{
    start_x_ = config->get_config<int>(config_start_x);
    distance_x_ = config->get_config<int>(config_distance_x);
    start_y_ = config->get_config<int>(config_start_y);
    distance_y_ = config->get_config<int>(config_distance_y);
    spot_size_x_ = config->get_config<int>(config_spot_size_x);
    spot_size_y_ = config->get_config<int>(config_spot_size_y);
    pick_asset_name_ = config->get_config<std::string>(config_pick_asset_name);

    chooser_start_x_ = config->get_config<int>(config_chooser_start_x);
    chooser_distance_x_ = config->get_config<int>(config_chooser_distance_x);
    chooser_start_y_ = config->get_config<int>(config_chooser_start_y);
    chooser_distance_y_ = config->get_config<int>(config_chooser_distance_y);
    chooser_spot_size_x_ = config->get_config<int>(config_chooser_size_x);
    chooser_spot_size_y_ = config->get_config<int>(config_chooser_size_y);
    chooser_asset_name_ = config->get_config<std::string>(config_chooser_asset_name);

    remaining_box_x_ = config->get_config<int>(config_remaining_box_x);
    remaining_box_y_ = config->get_config<int>(config_remaining_box_y);
    remaining_box_size_x_ = config->get_config<int>(config_remaining_box_size_x);
    remaining_box_size_y_ = config->get_config<int>(config_remaining_box_size_x);
    remaining_box_font_size_ = config->get_config<int>(config_remaining_box_font_size);

    reveal_audio_ = config->get_config<std::string>(config_reveal_audio);

    entry_script_ = config->get_config<std::string>(config_entry_script);
    first_entry_script_ = config->get_config<std::string>(config_first_entry_script);
    done_script_ = config->get_config<std::string>(config_done_script);

    if (config->configs.count(config_spots) > 0)
    {
        all_spots_ = split(config->get_config<std::string>(config_spots), script_term_delimiter);
    }
    else
    {
        throw std::runtime_error("No spots configured for Match15.");
    }
}

bool gs::g2024::Match15::offer_key(SDL_Keycode keycode)
{
    return false;
}

std::map<std::string, int> match15_map =
{
    { signal_left, 0 },
    { signal_right, 1 },
    { signal_up, 2 },
    { signal_down, 3 },
    { signal_select, 4 }
};


bool gs::g2024::Match15::offer_signal(const directive signal)
{
    switch(action_state_)
    {
    // NOP states
    case Match15ActionState::NEW:
    case Match15ActionState::ENTRY_SCRIPT:
    case Match15ActionState::OUTCOME:
    case Match15ActionState::OUTCOME_UNDERWAY:
    case Match15ActionState::DONE:
        break;

    case Match15ActionState::OUTCOME_WAIT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[done_script_]);
            action_state_ = Match15ActionState::DONE;

            // We don't need these anymore, if we needed them at all.
            context_->get_object_manager()->deregister_object(match1_object);
            context_->get_object_manager()->deregister_object(match2_object);
        }
        break;

    case Match15ActionState::RUNNING_ENTRY_SCRIPT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            action_state_ = Match15ActionState::WAITING;
        }
        break;

    case Match15ActionState::WAITING:
        if (!signal.empty())
        {
            switch(match15_map[signal[0]])
            {
            case 0: // signal_left:
                if (cur_x_ > 0)
                {
                    cur_x_--;
                }
                break;
            case 1: // signal_right:
                if (cur_x_ < 4)
                {
                    cur_x_++;
                }
                break;
            case 2: // signal_up:
                if (cur_y_ > 0)
                {
                    cur_y_--;
                }
                break;
            case 3: // signal_down:
                if (cur_y_ < 2)
                {
                    cur_y_++;
                }
                break;
            case 4: // signal_select:
                check_select();
                break;
            }
        }
        break;
    default:
        assert(false);
    }
    return false;
}

gs::GSGameState gs::g2024::Match15::game_loop_impl(const GSGameState state)
{
    auto asset_manager = context_->get_asset_manager();

    render_main();

    switch(action_state_)
    {
    case Match15ActionState::NEW:
        assert(false);  // Should never get here as activate will move to ENTRY_SCRIPT.

    case Match15ActionState::ENTRY_SCRIPT:
        {
            std::string run_script = entry_script_;
            if (context_->get_global(config_global_first).empty())
            {
                run_script = first_entry_script_;
                context_->set_global(config_global_first, "TRUE");
            }
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[run_script]);
            action_state_ = Match15ActionState::RUNNING_ENTRY_SCRIPT;
        }
        break;

    case Match15ActionState::RUNNING_ENTRY_SCRIPT:
        // This should never run, since the game is frozen.
        break;

    case Match15ActionState::WAITING:
        break;

    case Match15ActionState::OUTCOME:
        start_outcome();
        action_state_ = Match15ActionState::OUTCOME_UNDERWAY;
        break;

    case Match15ActionState::OUTCOME_UNDERWAY:
        do_outcome();
        break;

    case Match15ActionState::OUTCOME_WAIT:
        break;

    case Match15ActionState::DONE:
        break;
    }

    return running_state_;
}

void gs::g2024::Match15::start_outcome()
{
    current_outcome_ = context_->get_loaded_game()->config->outcomes[outcome_next_idx_];
    outcome_next_idx_++;

    outcome_step_ = 0;
    outcome_next_time_ = SDL_GetTicks() + 1000;
}

void gs::g2024::Match15::do_outcome()
{

    if (outcome_step_ < number_free_spins)
    {
        // Show tiles.
        if (SDL_GetTicks() > outcome_next_time_)
        {
            picks_[outcome_step_].texture_ = context_->get_asset_manager()->get_named_texture(current_outcome_[outcome_step_]);
            outcome_next_time_ = SDL_GetTicks() + 1000;
            outcome_step_++;
            if (!reveal_audio_.empty())
            {
                PLAYSOUNDASSETC(reveal_audio_,1);
            }
        }
    }
    else
    {
        action_state_ = Match15ActionState::OUTCOME_WAIT;
        if (current_outcome_[5] == "WIN")
        {
            if (current_outcome_.size() > 8)
            {
                // mark matches with cursors objects.
                auto pick = picks_[std::stoi(current_outcome_[7])];
                context_->get_object_manager()->get_object(match1_object, chooser_texture_, false,
                    pick.cur_x * chooser_distance_x_ + chooser_start_x_, pick.cur_y_ * chooser_distance_y_ + chooser_start_y_,
                        100,  chooser_spot_size_x_, chooser_spot_size_y_);
                pick = picks_[std::stoi(current_outcome_[8])];
                context_->get_object_manager()->get_object(match2_object, chooser_texture_, false,
                    pick.cur_x * chooser_distance_x_ + chooser_start_x_, pick.cur_y_ * chooser_distance_y_ + chooser_start_y_,
                        100,  chooser_spot_size_x_, chooser_spot_size_y_);
            }

            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[current_outcome_[6]]);
        }
        else if (current_outcome_[5] == "LOSE")
        {
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[current_outcome_[6]]);
        }
        else
        {
            throw std::runtime_error("Invalid outcome.  No WIN or LOSE");
        }

    }
}

void gs::g2024::Match15::check_select()
{
    // Do we mark one?
    if (picks_.size() < number_free_spins)
    {
        // New pick.
        for (const auto& item : picks_)
        {
            if ((item.cur_x == cur_x_)&&(item.cur_y_ == cur_y_))
            {
                // Already picked.
                return;
            }
        }

        // New pick.
        Match15Pick pick;
        pick.cur_x = cur_x_;
        pick.cur_y_ = cur_y_;
        pick.texture_ = context_->get_asset_manager()->get_named_texture(pick_asset_name_);
        picks_.push_back(pick);

        // Take the pick
        picks_remaining_--;
        picks_remaining_spoiled_ = true;
    }

    // Do we have the full board?
    if (picks_.size() == number_free_spins)
    {
        action_state_ = Match15ActionState::OUTCOME;
    }
}

// =======================================================================================
// Rendering

void gs::g2024::Match15::render_board()
{
    for (const auto& item: picks_)
    {
        auto dest = SDL_Rect{item.cur_x * distance_x_ + start_x_, item.cur_y_ * distance_y_ + start_y_,
            spot_size_x_, spot_size_y_};
        SDL_RenderCopy(context_->get_renderer(), item.texture_->get_texture(), nullptr, & dest );
    }
}

void gs::g2024::Match15::render_cursor() const
{
    const auto dest = SDL_Rect{cur_x_ * chooser_distance_x_ + chooser_start_x_, cur_y_ * chooser_distance_y_ + chooser_start_y_,
        chooser_spot_size_x_, chooser_spot_size_y_};
    SDL_RenderCopy(context_->get_renderer(), chooser_texture_->get_texture(), nullptr, & dest );
}

void gs::g2024::Match15::render_remaining()
{
    if (picks_remaining_spoiled_ || remaining_texture_ == nullptr)
    {

        if (remaining_texture_ != nullptr)
        {
            SDL_DestroyTexture(remaining_texture_);
        }

        const auto font = context_->get_asset_manager()->get_font();
        TTF_SetFontSize(font, remaining_box_font_size_);
        const auto text_surface = TTF_RenderText_Solid(font,
                                                 std::to_string(picks_remaining_).c_str(),
                                                 // TODO configurable.
                                                 SDL_Color{100, 200, 100, 0});
        if (text_surface == nullptr)
        {
            throw std::runtime_error(gs::format_string("Unable to remaining text.  %s", TTF_GetError()));
        }

        remaining_texture_ = SDL_CreateTextureFromSurface(context_->get_renderer(), text_surface);
        SDL_FreeSurface(text_surface);
        context_->coins_clean();

        picks_remaining_spoiled_ = false;
    }

    auto dest = SDL_Rect{remaining_box_x_, remaining_box_y_, remaining_box_size_x_, remaining_box_size_y_};

    if ((context_->get_coins() > 9) && (context_->get_coins() <  100))
    {
        dest.x = dest.x - 67;
        //dest.w = 67 + 67;
    }
    else if (context_->get_coins()>= 100)
    {
        dest.x = dest.x - (67 * 2);
        //dest.w = 67 + (67 * 2);
    }

    SDL_RenderCopy(context_->get_renderer(), remaining_texture_, nullptr, & dest );
}

void gs::g2024::Match15::render_main()
{
    auto asset_manager = context_->get_asset_manager();

    // == ALWAYS RENDERS ==========================================
    render_remaining();

    // == DEPENDENT RENDERS =======================================
    switch(action_state_)
    {
    case Match15ActionState::NEW:
    case Match15ActionState::WAITING:
        render_board();
        render_cursor();
        break;
    case Match15ActionState::OUTCOME:
    case Match15ActionState::OUTCOME_UNDERWAY:
    case Match15ActionState::DONE:
        render_board();
        break;
    default:
        break;
    }

}


