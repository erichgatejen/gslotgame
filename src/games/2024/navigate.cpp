/****************************************************************************************************************
 * Game Slot System.
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file navigate.cpp
 *
* Navigate.
 */

#include <string>

#include "config.h"

#include <SDL.h>
#include <SDL_mixer.h>

#include "navigate.h"

#include <loader.h>
#include <util.h>

#include "script.h"

#include "../utils.h"

// ===================================================================================================================
// Config, assets and tools.
//

const std::string board_mark_start = "S";
const std::string board_mark_home = "H";
const std::string board_mark_open = "O";
const std::string board_mark_impassable = "X";
const std::string board_mark_new_row = "NEW";

const std::string outcome_mark_home = "HOME";
const std::string outcome_mark_boom = "BOOM";

// Signals
const std::string signal_done = "DONE";
const std::string signal_left = "LEFT";
const std::string signal_right = "RIGHT";
const std::string signal_up = "UP";
const std::string signal_down = "DOWN";
const std::string signal_select = "SELECT";

// Globals
const std::string global_player_x = "Navigate.playerx";
const std::string global_player_y = "Navigate.playery";


// Configuration points.
const std::string config_global_first = "Navigate.first";
const std::string config_player_object_name = "Navigate.object.player";
const std::string config_observer_object_name = "Navigate.object.observer";

const std::string config_start_x = "start_x";
const std::string config_distance_x = "distance_x";
const std::string config_start_y = "start_y";
const std::string config_distance_y = "distance_y";
const std::string config_spot_size_x = "spot_size_x";
const std::string config_spot_size_y = "spot_size_y";

const std::string config_observer_x = "observer_x";
const std::string config_observer_y = "observer_y";

const std::string config_board = "board";

const std::string config_entry_script = "entry_script_name";
const std::string config_first_entry_script = "entry_first_script_name";  // first time the entry script is run.
const std::string config_done_script = "done_script";

// ===================================================================================================================
// SLOT 4x4Spot
//

gs::g2024::Navigate::Navigate(const std::string& registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(registration_name, context), loc_x_(0), loc_y_(0), observer_x_(0), observer_y_(0), outcome_next_idx_(0),
    running_state_(),
    action_state_(NavigateActionState::NEW)
{
}

gs::g2024::Navigate::~Navigate()
{
}

const int outcome_value_player_asset = 0;
const int outcome_value_observer_asset = 1;
const int outcome_value_result = 2;

void gs::g2024::Navigate::activate()
{
    if ((action_state_ == NavigateActionState::DONE)||(action_state_ == NavigateActionState::NEW))
    {
        // reset the board.
        board_live_.clear();
        for (auto srow : board_)
        {
            std::vector<std::shared_ptr<NavigateSpot>> drow;
            for (auto scol : srow)
            {
                auto spot = std::make_shared<NavigateSpot>(*scol);
                drow.push_back(spot);
            }
            board_live_.push_back(drow);
        }

        current_outcome_ = context_->get_loaded_game()->config->outcomes[outcome_next_idx_];
        outcome_next_idx_++;

        if (const auto out_tokens = split(get_outcome_value<std::string>(current_outcome_, outcome_value_result),
            script_order_delimiter); out_tokens[0] == outcome_mark_home)
        {
            outcome_trigger_ = out_tokens[1];
        }
        else if (out_tokens[0] == outcome_mark_boom)
        {
            for (auto idx = outcome_value_result+1; idx < current_outcome_.size(); idx++)
            {
                auto loc_tokens = split(current_outcome_[idx], script_order_delimiter);
                if (loc_tokens.size() < 2)
                {
                    throw std::runtime_error("Bad location toke in in outcome.");
                }

                const auto spot =  board_live_[std::atoi(loc_tokens[0].c_str())][atoi(loc_tokens[1].c_str())];
                outcome_trigger_ = out_tokens[1];
                spot->boom_trigger = true;
            }
        }
        else
        {
            throw std::runtime_error("No HOME or BOOM to end session.");
        }

        // Find start.
        for (auto row = 0; row < board_live_.size(); row++)
        {
            auto col = 0;
            for (const auto item : board_live_[row])
            {
                if (item->type == NavigateSpotType::START)
                {
                    loc_x_ = col;
                    loc_y_ = row;
                    board_live_[loc_y_][loc_x_]->visited = true;
                    row = board_live_.size();
                    break;
                }
                col++;
            }
            // TODO: check for found.
        }

        const auto player_asset = get_outcome_value<std::string>(current_outcome_, outcome_value_player_asset);
        auto obj = context_->get_object_manager()->get_object(config_player_object_name, player_asset,
            false, 0, 0, 40);
        obj->set_texture(context_->get_asset_manager()->get_named_texture(player_asset));  // In case the object already exists.

        const auto observer_asset = get_outcome_value<std::string>(current_outcome_, outcome_value_observer_asset);
        obj = context_->get_object_manager()->get_object(config_observer_object_name, observer_asset, false,
            observer_x_, observer_y_, 40);
        obj->set_texture(context_->get_asset_manager()->get_named_texture(observer_asset));  // In case the object already exists.

        action_state_ = NavigateActionState::ENTRY_SCRIPT;
    }
    // TODO need an error for a bad state?

    running_state_ = GSGameState::RENDERING_ACTIVE;
}

void gs::g2024::Navigate::deactivate()
{
    action_state_ = NavigateActionState::DONE;
    running_state_ = GSGameState::DONE;
    context_->get_object_manager()->deregister_object(config_player_object_name);
    context_->get_object_manager()->deregister_object(config_observer_object_name);
}

void gs::g2024::Navigate::init_impl(GSGameState state, const std::shared_ptr<ConfigGame> config)
{

    start_x_ = config->get_config<int>(config_start_x);
    distance_x_ = config->get_config<int>(config_distance_x);
    start_y_ = config->get_config<int>(config_start_y);
    distance_y_ = config->get_config<int>(config_distance_y);
    spot_size_x_ = config->get_config<int>(config_spot_size_x);
    spot_size_y_ = config->get_config<int>(config_spot_size_y);

    observer_x_ = config->get_config<int>(config_observer_x);
    observer_y_ = config->get_config<int>(config_observer_y);

    entry_script_ = config->get_config<std::string>(config_entry_script);
    first_entry_script_ = config->get_config<std::string>(config_first_entry_script);
    done_script_ = config->get_config<std::string>(config_done_script);

    // Board
    // "board": "O,O,O,O,O,O,O,H:WIN,NEW,O,O,O,O,O,O,O,NEW,S:READY,O,O,O,O,O,O",
    std::vector<std::shared_ptr<NavigateSpot>> row;
    for (const auto board_desc = split(config->get_config<std::string>(config_board),script_term_delimiter); auto token : board_desc)
    {
        if (token.starts_with(board_mark_start))
        {
            auto spot = std::make_shared<NavigateSpot>();
            spot->type = NavigateSpotType::START;
            if (auto t = split(token, script_order_delimiter); t.size() > 1)
            {
                spot->trigger = t[1];
            }
            row.push_back(spot);
        }
        else if (token.starts_with(board_mark_home))
        {
            auto spot = std::make_shared<NavigateSpot>();
            spot->type = NavigateSpotType::HOME;
            if (auto t = split(token, script_order_delimiter); t.size() > 1)
            {
                spot->trigger = t[1];
            }
            row.push_back(spot);
        }
        else if (token.starts_with(board_mark_open))
        {
            auto spot = std::make_shared<NavigateSpot>();
            if (auto t = split(token, script_order_delimiter); t.size() > 1)
            {
                spot->trigger = t[1];
            }
            row.push_back(spot);
        }
        else if (token.starts_with(board_mark_impassable))
        {
            auto spot = std::make_shared<NavigateSpot>();
            spot->type = NavigateSpotType::IMPASSABLE;
            if (auto t = split(token, script_order_delimiter); t.size() > 1)
            {
                spot->trigger = t[1];
            }
            row.push_back(spot);
        }
        else if (token.starts_with(board_mark_new_row))
        {
            board_.push_back(row);
            row.clear();
        }
        else
        {
            throw std::runtime_error("Bad board definition token: " + token);
        }
    }

    if (row.size() > 0)
    {
        board_.push_back(row);
    }

    // Outcome

}

bool gs::g2024::Navigate::offer_key(SDL_Keycode keycode)
{
    return false;
}

std::map<std::string, int> navigate_map =
{
    { signal_left, 0 },
    { signal_right, 1 },
    { signal_up, 2 },
    { signal_down, 3 },
    { signal_select, 4 }
};

bool gs::g2024::Navigate::offer_signal(const directive signal)
{
    switch(action_state_)
    {
    // NOP states
    case NavigateActionState::NEW:
    case NavigateActionState::ENTRY_SCRIPT:
    case NavigateActionState::OUTCOME:
    case NavigateActionState::DONE:
        break;

    case NavigateActionState::OUTCOME_WAIT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[done_script_]);
            action_state_ = NavigateActionState::DONE;
        }
        break;

    case NavigateActionState::RUNNING_ENTRY_SCRIPT:
    case NavigateActionState::SCRIPT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            // TODO may not be the best place to show the two characters.
            context_->get_object_manager()->get(config_player_object_name)->show();
            context_->get_object_manager()->get(config_observer_object_name)->show();
            action_state_ = NavigateActionState::WAITING;
        }
        break;

    case NavigateActionState::WAITING:
        if (!signal.empty())
        {
            switch(navigate_map[signal[0]])
            {
            case 0: // signal_left:
                if ((loc_x_ > 0) && board_live_[loc_y_][loc_x_-1]->type != NavigateSpotType::IMPASSABLE)
                {
                    loc_x_--;
                    check_new_location();
                }
                break;
            case 1: // signal_right:
                if (loc_x_ < board_live_[loc_y_].size()-1 && board_live_[loc_y_][loc_x_+1]->type != NavigateSpotType::IMPASSABLE)
                {
                    loc_x_++;
                    check_new_location();
                }
                break;
            case 2: // signal_up:
                if (loc_y_ > 0 && board_live_[loc_y_-1][loc_x_]->type != NavigateSpotType::IMPASSABLE)
                {
                    loc_y_--;
                    check_new_location();
                }
                break;
            case 3: // signal_down:
                if (loc_y_ < board_live_.size()-1 && board_live_[loc_y_+1][loc_x_]->type != NavigateSpotType::IMPASSABLE)
                {
                    loc_y_++;
                    check_new_location();
                }
                break;
            //case 4: // signal_select:
            //    check_select();
            //    break;
            }
        }
        break;
    default:
        assert(false);
    }
    return false;
}

gs::GSGameState gs::g2024::Navigate::game_loop_impl(const GSGameState state)
{
    auto asset_manager = context_->get_asset_manager();

    render_main();

    switch(action_state_)
    {
    case NavigateActionState::NEW:
        assert(false);  // Should never get here as activate will move to ENTRY_SCRIPT.

    case NavigateActionState::ENTRY_SCRIPT:
        {
            std::string run_script = entry_script_;
            if (context_->get_global(config_global_first).empty())
            {
                run_script = first_entry_script_;
                context_->set_global(config_global_first, "TRUE");
            }
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[run_script]);
            action_state_ = NavigateActionState::RUNNING_ENTRY_SCRIPT;
        }
        break;

    case NavigateActionState::RUNNING_ENTRY_SCRIPT:
        // This should never run, since the game is frozen.
        break;

    case NavigateActionState::WAITING:
        break;

    case NavigateActionState::SCRIPT_PENDING:
        //This gives the renderer a chance to resolve any object stuff before we start a script which may FREEZE things.
        set_globals();
        context_->get_script_manager()->run(context_->get_loaded_game()->config->get_script(pending_script_));
        action_state_ = NavigateActionState::SCRIPT;
        break;

    case NavigateActionState::SCRIPT:
        break;

    case NavigateActionState::OUTCOME:
        do_outcome();
        action_state_ = NavigateActionState::OUTCOME_WAIT;
        break;

    case NavigateActionState::OUTCOME_WAIT:
        break;

    case NavigateActionState::DONE:
        break;
    default:
        assert(false);
    }

    return running_state_;
}

// =======================================================================================
// Rendering

void gs::g2024::Navigate::render_board()
{
    if (action_state_ != NavigateActionState::ENTRY_SCRIPT && action_state_ != NavigateActionState::RUNNING_ENTRY_SCRIPT)
    {

        // player object needs to be put in the right place.
        const auto obj = context_->get_object_manager()->get(config_player_object_name);
        obj->move(start_x_ + (loc_x_ * distance_x_), start_y_ + (loc_y_ * distance_y_));
        // obj->show();  Don't do this.  The scripts my hide it.

        // observer object should show already.
    }
}

void gs::g2024::Navigate::render_remaining()
{
    // Nothing at this time.
}

void gs::g2024::Navigate::render_main()
{
    auto asset_manager = context_->get_asset_manager();

    // == ALWAYS RENDERS ==========================================
    render_remaining();
    render_board();

    // == DEPENDENT RENDERS =======================================
}

// =======================================================================================
// Logic

void gs::g2024::Navigate::set_globals() const
{
    context_->set_global(global_player_x, std::to_string(start_x_+(loc_x_*distance_x_)+(spot_size_x_/2)));
    context_->set_global(global_player_y, std::to_string(start_y_+(loc_y_*distance_y_)+(spot_size_y_/2)));
}

void gs::g2024::Navigate::do_outcome()
{
    set_globals();
    context_->get_script_manager()->run(context_->get_loaded_game()->config->
        get_script(outcome_trigger_));

    action_state_ = NavigateActionState::OUTCOME_WAIT;
}

void gs::g2024::Navigate::check_new_location()
{
    if (const auto spot = board_live_[loc_y_][loc_x_]; !spot->visited)
    {
        switch(spot->type)
        {
        case NavigateSpotType::HOME:
            action_state_ = NavigateActionState::OUTCOME;
            break;

        case NavigateSpotType::START:
        case NavigateSpotType::OPEN:
            if (spot->visited == false)
            {

                if (spot->boom_trigger)
                {
                    action_state_ = NavigateActionState::OUTCOME;
                }
                else if (spot->trigger != "")
                {
                    pending_script_ = spot->trigger;
                    action_state_ = NavigateActionState::SCRIPT_PENDING;
                }
            }
            break;

        case NavigateSpotType::IMPASSABLE:
            assert(false);
        }

        spot->visited = true;
    }

}





