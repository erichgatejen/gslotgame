/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file slot_4x4spot.h
 *
 * Cascading spot slot.
 */

#ifndef GSLOTGAME_G2022_SLOT_4X4SPOT_GAME_H
#define GSLOTGAME_G2022_SLOT_4X4SPOT_GAME_H

#include "context.h"
#include "game.h"

#include <bits/random.h>

#include "objects.h"

namespace gs::g2024
{

// ===================================================================================================================
// CONFIG
//

// ===================================================================================================================
// CLASSES AND FUNCTIONS
//

enum class Slot_4x4SpotActionState
{
    NEW,
    ENTRY_SCRIPT,
    RUNNING_ENTRY_SCRIPT,
    WAITING,
    SPINNING,
    SPIN_OUTCOME,
    SPIN_OUTCOME_UNDERWAY,
    SPIN_OUTCOME_COMPLETE,
    OUTCOME,
    OUTCOME_UNDERWAY,
    OUTCOME_WAIT,
    DONE
};

enum class Slot_4x4SpotState
{
    RUNNING,
    STOPPING,
    FROZEN
};

struct Slot_4x4SpotSpot
{
    int x, y, outcome;
    std::string name;
    std::string previous;
    pixel_size step;
    Slot_4x4SpotState state;
    Uint32 stop_time;

    Slot_4x4SpotSpot() : x(0), y(0), step(0), state(Slot_4x4SpotState::RUNNING) {};
};

class Slot_4x4Spot final : public GSGame {

    pixel_size start_x_ = 0;
    pixel_size distance_x_ = 0;
    pixel_size start_y_ = 0;
    pixel_size distance_y_ = 0;
    pixel_size spot_size_x_ = 0;
    pixel_size spot_size_y_ = 0;
    directive  all_spots_;

    pixel_size win_start_x_ = 0;
    pixel_size win_distance_x_ = 0;
    pixel_size win_start_y_ = 0;
    pixel_size win_distance_y_ = 0;
    pixel_size win_font_size_ = 0;
    std::string win_win_audio_asset_name_;
    std::string win_lose_audio_asset_name_;
    Uint32 win_wait_{};
    Uint32 lose_wait_{};

    std::string audio_spinning_name_;

    pixel_size freespins_x_ = 0;
    pixel_size freespins_y_ = 0;
    pixel_size freespins_font_size_ = 0;

    pixel_size meter_grand_x_ = 0;
    pixel_size meter_grand_y_ = 0;
    pixel_size meter_grand_size_x_ = 0;
    pixel_size meter_grand_size_y_ = 0;
    std::string meter_grand_asset_name_;
    pixel_size meter_major_x_ = 0;
    pixel_size meter_major_y_ = 0;
    pixel_size meter_major_size_x_ = 0;
    pixel_size meter_major_size_y_ = 0;
    std::string meter_major_asset_name_;
    pixel_size meter_minor_x_ = 0;
    pixel_size meter_minor_y_ = 0;
    pixel_size meter_minor_size_x_ = 0;
    pixel_size meter_minor_size_y_ = 0;
    std::string meter_minor_asset_name_;

    int meter_grand_threshhold_ = 0;
    int meter_major_threshhold_ = 0;
    int meter_minor_threshhold_ = 0;
    std::string meter_grand_script_name_;
    std::string meter_major_script_name_;
    std::string meter_minor_script_name_;
    std::string meter_lose_script_name_;

    pixel_size prize_meter_x_ = 0;
    pixel_size prize_meter_y_ = 0;
    pixel_size prize_meter_size_x_ = 0;
    pixel_size prize_meter_size_y_ = 0;

    std::string first_entry_script_;
    std::string entry_script_;
    std::string done_script_;

    directive current_outcome_;
    int       outcome_next_idx_;
    int       outcome_step_{};
    Uint32    outcome_next_time_{};
    int       point_scale_{};
    int       outcome_points_{};

    GSGameState running_state_;
    Slot_4x4SpotActionState action_state_;

    std::vector<Slot_4x4SpotSpot> spots_;           // row1, row1, row3, row4

    SDL_Texture* freespins_texture_;
    int freespins_remaining_{};
    bool freespins_remaining_spoiled_{};
    pixel_size freespins_current_x_{};
    pixel_size freespins_current_y_{};
    bool free_spins_show_;

    std::mt19937 rng_;
    std::uniform_int_distribution<std::mt19937::result_type> random_spot_;
    std::uniform_int_distribution<std::mt19937::result_type> random_nudge_;
    std::uniform_int_distribution<std::mt19937::result_type> random_stop_time_;

    int spin_outcome_step_{};
    bool spin_outcome_pending_{};
    Uint32 spin_outcome_step_timing_{};
    std::shared_ptr<GSTexture> spin_pay_textures_[4];

    void start_spin();
    void spinning();
    void start_spin_outcome();
    Uint32 do_spin_outcome_step(int step);
    void do_spin_outcome();
    void do_outcome() const;
    void render_board();
    void render_remaining();
    void render_main();

    [[nodiscard]] bool won_grand() const;
    [[nodiscard]] bool won_major() const;
    [[nodiscard]] bool won_minor() const;
    void reset_spin_pay();
    [[nodiscard]] std::shared_ptr<GSTexture> get_value_texture(const std::string&, int font_size) const;

public:
    Slot_4x4Spot(const std::string& registration_name, const std::shared_ptr<GSContext>& context);
    ~Slot_4x4Spot() override;
    GSGAME_VIRTUAL_OVERRIDES

};

}


#endif // GSLOTGAME_G2022_SLOT_4X4SPOT_GAME_H
