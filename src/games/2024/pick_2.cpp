/****************************************************************************************************************
 * Game Slot System.
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file pick_2.cpp
 *
 * Pick one of two options.
 */

#include <string>

#include "config.h"

#include <SDL.h>
#include <SDL_mixer.h>

#include "pick_2.h"

#include <loader.h>
#include <fmt/format.h>

#include "../utils.h"

#include "script.h"

// ===================================================================================================================
// Config, assets and tools.
//

const std::string outcome_win = "WIN";
const std::string outcome_lose = "LOSE";

// Priorities
const uint priority_chooser = 10;
const uint priority_door    = 50;
const uint priority_head    = 100;

// Outcomes
const int outcome_win_or_lose = 0;      // Probably not needed.
const int outcome_open_door_asset = 1;
const int outcome_script = 3;
//const std::string outcome_open_door_name = "_pick2_open_door_object_name";  // TODO may collide with other instances.

// Signals
const std::string signal_done = "DONE";
const std::string signal_left = "LEFT";
const std::string signal_right = "RIGHT";
const std::string signal_select = "SELECT";

// Configuration points.
//const std::string config_signal_spin = "signal_select";
const std::string config_global_first = "pick2.first";
const std::string config_global_x = "pick2.x";
const std::string config_global_y = "pick2.y";

const std::string config_spot_0_x = "spot_0_x";
const std::string config_spot_0_y = "spot_0_y";
const std::string config_spot_0_size_x = "spot_0_size_x";
const std::string config_spot_0_size_y = "spot_0_size_y";
const std::string config_spot_1_x = "spot_1_x";
const std::string config_spot_1_y = "spot_1_y";
const std::string config_spot_1_size_x = "spot_1_size_x";
const std::string config_spot_1_size_y = "spot_1_size_y";
const std::string config_spot_asset_name = "spot_asset_name";

const std::string config_pick_spot_object_name = "pick_spot_object_name";

const std::string config_picksign_x = "picksign_x";
const std::string config_picksign_y = "picksign_y";
const std::string config_picksign_size_x = "picksign_size_x";
const std::string config_picksign_size_y = "picksign_size_y";
const std::string config_picksign_asset_name = "picksign_asset_name";

const std::string config_chooser_size_x = "chooser_size_x";
const std::string config_chooser_size_y = "chooser_size_y";
const std::string config_chooser_asset_name = "chooser_asset_name";

const std::string config_entry_script = "entry_script_name";
const std::string config_first_entry_script = "entry_first_script_name";  // first time the entry script is run.
const std::string config_done_script = "done_script";

// ===================================================================================================================
// Match 15
//

gs::g2024::Pick2::Pick2(const std::string& registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(registration_name, context), outcome_next_idx_(0), running_state_(), action_state_(Pick2ActionState::NEW),
    which_pick_(0)
{
}

gs::g2024::Pick2::~Pick2()
{
}

void gs::g2024::Pick2::activate()
{
    if ((action_state_ == Pick2ActionState::DONE)||(action_state_ == Pick2ActionState::NEW))
    {
        // reset the board.
        // TODO maybe on deactivation?
        which_pick_ = 0;

        chooser_object_ = context_->get_object_manager()->get_object(chooser_asset_name_, chooser_asset_name_,
                                                        true, spot_0_x_, spot_0_y_, priority_chooser);
        pick_sign_object_ = context_->get_object_manager()->get_object(picksign_asset_name_, picksign_asset_name_,
                                                        true, picksign_x_, picksign_y_, priority_door);
        pick_sign_object_->show();
        action_state_ = Pick2ActionState::ENTRY_SCRIPT;
    }
    // TODO need an error for a bad state?

    running_state_ = GSGameState::RENDERING_ACTIVE;
}

void gs::g2024::Pick2::deactivate()
{
    action_state_ = Pick2ActionState::DONE;
    running_state_ = GSGameState::DONE;

    chooser_object_->hide();
    spot_open_door_object_->hide();
    pick_sign_object_->hide();
}

void gs::g2024::Pick2::init_impl(GSGameState state, std::shared_ptr<ConfigGame> config)
{
    spot_0_x_ = config->get_config<int>(config_spot_0_x);
    spot_0_y_ = config->get_config<int>(config_spot_0_y);
    spot_0_size_x_ = config->get_config<int>(config_spot_0_size_x);
    spot_0_size_y_ = config->get_config<int>(config_spot_0_size_y);

    spot_1_x_ = config->get_config<int>(config_spot_1_x);
    spot_1_y_ = config->get_config<int>(config_spot_1_y);
    spot_1_size_x_ = config->get_config<int>(config_spot_1_size_x);
    spot_1_size_y_ = config->get_config<int>(config_spot_1_size_y);
    //spot_asset_name_ = config->get_config<std::string>(config_picksign_asset_name);

    pick_spot_object_name_ =  config->get_config<std::string>(config_pick_spot_object_name);

    picksign_x_ = config->get_config<int>(config_picksign_x);
    picksign_y_ = config->get_config<int>(config_picksign_y);
    picksign_size_x_ = config->get_config<int>(config_picksign_size_x);
    picksign_size_y_ = config->get_config<int>(config_picksign_size_y);
    picksign_asset_name_ = config->get_config<std::string>(config_picksign_asset_name);

    chooser_size_x_ = config->get_config<int>(config_chooser_size_x);
    chooser_size_y_ = config->get_config<int>(config_chooser_size_y);
    chooser_asset_name_ = config->get_config<std::string>(config_chooser_asset_name);

    entry_script_ = config->get_config<std::string>(config_entry_script);
    first_entry_script_ = config->get_config<std::string>(config_first_entry_script);
    done_script_ = config->get_config<std::string>(config_done_script);
}

bool gs::g2024::Pick2::offer_key(SDL_Keycode keycode)
{
    return false;
}

std::map<std::string, int> Pick2_map =
{
    { signal_left, 0 },
    { signal_right, 1 },
    { signal_select, 2 }
};


bool gs::g2024::Pick2::offer_signal(const directive signal)
{
    switch(action_state_)
    {
    // NOP states
    case Pick2ActionState::NEW:
    case Pick2ActionState::ENTRY_SCRIPT:
    case Pick2ActionState::OUTCOME:
    case Pick2ActionState::DONE:
        break;

    case Pick2ActionState::OUTCOME_WAIT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[done_script_]);
            action_state_ = Pick2ActionState::DONE;

        }
        break;

    case Pick2ActionState::RUNNING_ENTRY_SCRIPT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            action_state_ = Pick2ActionState::WAITING;
        }
        break;

    case Pick2ActionState::WAITING:
        if (!signal.empty())
        {
            switch(Pick2_map[signal[0]])
            {
            case 0: // signal_left:
                if (which_pick_ > 0)
                {
                    which_pick_ = 0;
                }
                break;
            case 1: // signal_right:
                if (which_pick_ < 1)
                {
                    which_pick_ = 1;
                }
                break;
            case 2: // signal_select:
                action_state_ = Pick2ActionState::OUTCOME;
                break;
            default:
                assert(false);
            }
        }
        break;
    default:
        assert(false);
    }
    return false;
}

gs::GSGameState gs::g2024::Pick2::game_loop_impl(const GSGameState state)
{
    auto asset_manager = context_->get_asset_manager();

    render_main();

    switch(action_state_)
    {
    case Pick2ActionState::NEW:
        assert(false);  // Should never get here as activate will move to ENTRY_SCRIPT.

    case Pick2ActionState::ENTRY_SCRIPT:
        {
            std::string run_script = entry_script_;
            if (context_->get_global(config_global_first).empty())
            {
                run_script = first_entry_script_;
                context_->set_global(config_global_first, "TRUE");
            }
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[run_script]);
            action_state_ = Pick2ActionState::RUNNING_ENTRY_SCRIPT;
        }
        break;

    case Pick2ActionState::RUNNING_ENTRY_SCRIPT:
        // This should never run, since the game is frozen.
        break;

    case Pick2ActionState::WAITING:
        break;

    case Pick2ActionState::OUTCOME:
        start_outcome();
        action_state_ = Pick2ActionState::OUTCOME_WAIT;
        break;

    case Pick2ActionState::OUTCOME_WAIT:
    case Pick2ActionState::DONE:
        break;
    }

    return running_state_;
}

void gs::g2024::Pick2::start_outcome()
{
    current_outcome_ = context_->get_loaded_game()->config->outcomes[outcome_next_idx_];
    outcome_next_idx_++;

    pixel_size x, y;
    if (which_pick_ == 0)
    {
        x = spot_0_x_;
        y = spot_0_y_;
    }
    else
    {
        x = spot_1_x_;
        y = spot_1_y_;
    }
    spot_open_door_object_ = context_->get_object_manager()->get_object(pick_spot_object_name_,
                                                get_outcome_value<std::string>(current_outcome_, outcome_open_door_asset),
                                                true, x, y, priority_door);
    // dont show spot_open_door_object_->show();
    context_->set_global(config_global_x, std::to_string(x));
    context_->set_global(config_global_y, std::to_string(y));

    context_->get_script_manager()->run(context_->get_loaded_game()->config->
            get_script(get_outcome_value<std::string>(current_outcome_, outcome_script)));

    action_state_ = Pick2ActionState::OUTCOME_WAIT;
}

// =======================================================================================
// Rendering

void gs::g2024::Pick2::render_board()
{
    // Objects will render themselves.
}

void gs::g2024::Pick2::render_cursor() const
{
    switch(which_pick_)
    {
    case 0:
        chooser_object_->move(spot_0_x_, spot_0_y_);
        chooser_object_->show();
        break;
    case 1:
        chooser_object_->move(spot_1_x_, spot_1_y_);
        chooser_object_->show();
        break;
    default:
        chooser_object_->hide();
    }
}

void gs::g2024::Pick2::render_remaining()
{

}

void gs::g2024::Pick2::render_main()
{
    auto asset_manager = context_->get_asset_manager();

    // == ALWAYS RENDERS ==========================================
    render_remaining();

    // == DEPENDENT RENDERS =======================================
    switch(action_state_)
    {
    case Pick2ActionState::NEW:
    case Pick2ActionState::WAITING:
        render_board();
        render_cursor();
        break;
    case Pick2ActionState::OUTCOME:
    case Pick2ActionState::DONE:
        render_board();
        break;
    default:
        break;
    }

}


