/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2022, 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file slot_3reel.h
 *
 * Three reel slot.
 *
 * The winbox is 1120x400 in size and at x=40,y=260
 */

#ifndef GSLOTGAME_G2022_SLOT_3REEL_GAME_H
#define GSLOTGAME_G2022_SLOT_3REEL_GAME_H

#include "context.h"
#include "game.h"

#include <bits/random.h>

namespace gs::g2024
{

    // ===================================================================================================================
    // CONFIG
    //

    const std::string signal_done = "DONE";

    const std::string config_signal_spin = "signal_spin";
    const std::string config_spots = "spots";
    const std::string config_spots_exclude_from_random = "exclude_from_random";

    const std::string game_name_slot_3reel = "2024.slot_3reel";

    const std::string reel3_metadata_reels = "reels";
    const std::string reel3_metadata_spots = "spots";

    const std::string reel3_metadata_audioasset_slot_reels_running = "slot_reels_running";
    const std::string reel3_metadata_audioasset_slot_reel_stop  = "slot_reel_stop";

    const int reel3_stop_1_min_time_ms = 200;
    const int reel3_stop_1_max_time_ms = 300;
    const int reel3_stop_2_min_time_ms = 200;
    const int reel3_stop_2_max_time_ms = 300;

    const pixel_size asset_reel3_spot_size_x = 360;
    const pixel_size asset_reel3_spot_size_y = 400;
    const pixel_size asset_reel3_spot_half_size_y = asset_reel3_spot_size_y / 2;
    const pixel_size asset_reel3_spot_step = 80;

    const pixel_size pos_start_tolerance_y = 100;

    const pixel_size pos_x_spacing = 20;

    // First column
    const pixel_size pos_reel3_spot_x_1 = 40;
    const pixel_size pos_reel3_spot_y_1 = 60;
    const pixel_size pos_reel3_spot_x_2 = pos_reel3_spot_x_1;
    const pixel_size pos_reel3_spot_y_2 = pos_reel3_spot_y_1 + asset_reel3_spot_half_size_y;
    const pixel_size pos_reel3_spot_x_3 = pos_reel3_spot_x_1;
    const pixel_size pos_reel3_spot_y_3 = pos_reel3_spot_y_2 + asset_reel3_spot_size_y;

    // Second column
    const pixel_size pos_reel3_spot_x_4 = pos_reel3_spot_x_1 + asset_reel3_spot_size_x + pos_x_spacing;
    const pixel_size pos_reel3_spot_y_4 = pos_reel3_spot_y_1;
    const pixel_size pos_reel3_spot_x_5 = pos_reel3_spot_x_4;
    const pixel_size pos_reel3_spot_y_5 = pos_reel3_spot_y_2;
    const pixel_size pos_reel3_spot_x_6 = pos_reel3_spot_x_4;
    const pixel_size pos_reel3_spot_y_6 = pos_reel3_spot_y_3;

    // Third column
    const pixel_size pos_reel3_spot_x_7 = pos_reel3_spot_x_4+ asset_reel3_spot_size_x + pos_x_spacing;
    const pixel_size pos_reel3_spot_y_7 = pos_reel3_spot_y_1;
    const pixel_size pos_reel3_spot_x_8 = pos_reel3_spot_x_7;
    const pixel_size pos_reel3_spot_y_8 = pos_reel3_spot_y_2;
    const pixel_size pos_reel3_spot_x_9 = pos_reel3_spot_x_7;
    const pixel_size pos_reel3_spot_y_9 = pos_reel3_spot_y_3;

    enum class Reel3GameActionState
    {
        WAITING,
        START_SPIN,
        STOP_REEL_1,
        ROLLING_REEL_1,
        STOP_REEL_2,
        ROLLING_REEL_2,
        STOP_REEL_3,
        ROLLING_REEL_3,
        LANDING_3,
        RESULT_WAIT,
        RESULT,
        DONE
    };


    // ===================================================================================================================
    // CLASSES AND FUNCTIONS
    //

#define GS_REEL_MEMBERS(_NUM_) SDL_Rect dest_reel3_slot_##_NUM_;

    class Slot_3Reel final : public GSGame {

        GSGameState running_state_;
        std::string  signal_spin_;

        directive all_spots_;
        directive spots_exclude_random_;
        directive current_outcome_;
        int       outcome_next_idx_;

        uint frame_count_ = 1;
        uint stop_stage_ = 0;
        time_value next_stop_;
        int sound_reels_channel_;

        SDL_Texture * coin_texture_ = nullptr;

        SDL_Rect pos_spin_button_;
        std::vector<SDL_KeyCode> start_keys_;
        std::string audio_spinning_name_;
        std::string audio_stop_name_;
        std::string audio_ramp_name_;

        std::string spot_names_[9];
        GS_REEL_MEMBERS(1)
        GS_REEL_MEMBERS(2)
        GS_REEL_MEMBERS(3)
        GS_REEL_MEMBERS(4)
        GS_REEL_MEMBERS(5)
        GS_REEL_MEMBERS(6)
        GS_REEL_MEMBERS(7)
        GS_REEL_MEMBERS(8)
        GS_REEL_MEMBERS(9)

        int current_wheel_roll_1_ = asset_reel3_spot_half_size_y;
        int current_wheel_roll_2_ = asset_reel3_spot_half_size_y;
        int current_wheel_roll_3_ = asset_reel3_spot_half_size_y;

        std::mt19937 rng_;
        std::uniform_int_distribution<std::mt19937::result_type> random6_;
        std::uniform_int_distribution<std::mt19937::result_type> random_y_size_;
        std::uniform_int_distribution<std::mt19937::result_type> random_stop_1_;
        std::uniform_int_distribution<std::mt19937::result_type> random_stop_2_;

        Reel3GameActionState action_state_;

        void render_wheel(int current_wheel_roll, int pos_x, const std::string& spot_1, const std::string& spot_2, const std::string& spot_3) const;
        void render_wheels() const;
        void render_coins();
        void render_main();
        void rotate_spots();

        std::string get_random_spot();
        std::string get_outcome_spot(int num, bool dont_clean = false);
        void start_spin_positions();
        static uint flip_spot(uint spot);
        void check_stop_time(const time_value random_length, const Reel3GameActionState next_state);

        [[nodiscard]] Reel3GameActionState check_result() const;

    public:
        Slot_3Reel(const std::string& registration_name, const std::shared_ptr<GSContext>& context);
        ~Slot_3Reel() override;
        GSGAME_VIRTUAL_OVERRIDES

    };

}


#endif // GSLOTGAME_G2022_SLOT_3REEL_GAME_H
