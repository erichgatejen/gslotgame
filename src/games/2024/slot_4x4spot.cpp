/****************************************************************************************************************
 * Game Slot System.
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file slot_4x4spot.cpp
 *
* Cascading spot slot.
 */

#include <string>

#include "config.h"

#include <SDL.h>
#include <SDL_mixer.h>

#include "slot_4x4spot.h"

#include <loader.h>
#include <util.h>

#include "script.h"

#include "../utils.h"

// ===================================================================================================================
// Config, assets and tools.
//

const int total_spots = 16;
const int number_free_spins = 5;
const int nudge_size = 2;
const int max_nudge = 20;
const int max_spin_wait = 3000;

//  0    1      2     3    4      5      6     7     8     9    10    11    12    13    14    15    16 17  18  19, 20
// "csbo,csdrop,cslem,csol,csdrop,csdrop,csman,csdis,cslem,csav,csman,csdis,cslem,csdis,csveg,csman,0, 100, 0, 0"
const int outcome_value_spot_start = 16;
const int outcome_script_spot = 20;
const std::string outcome_script_nop = "x";

//const int spin_outcome_step_time = 1200;


//const std::string match1_object = "Slot_4x4Spot.match1";
//const std::string match2_object = "Slot_4x4Spot.match2";

// Signals
const std::string signal_select = "SPIN";
const std::string signal_done = "DONE";

// Configuration points.
const std::string config_signal_select = "signal_select";
const std::string config_global_first = "Slot_4x4Spot.first";

const std::string config_point_scale = "point_scale";

const std::string config_start_x = "start_x";
const std::string config_distance_x = "distance_x";
const std::string config_start_y = "start_y";
const std::string config_distance_y = "distance_y";
const std::string config_spot_size_x = "spot_size_x";
const std::string config_spot_size_y = "spot_size_y";
const std::string config_spots = "spots";

const std::string config_audio_spinning_name = "audio_running";

const std::string config_win_start_x = "win_start_x";
const std::string config_win_distance_x = "win_distance_x";
const std::string config_win_start_y = "win_start_y";
const std::string config_win_distance_y = "win_distance_y";
const std::string config_win_font_size = "win_font_size";
const std::string config_win_wait = "win_wait";
const std::string config_lose_wait = "lose_wait";
const std::string config_win_win_audio_asset_name = "win_win_audio_asset_name";
const std::string config_win_lose_audio_asset_name = "win_lose_audio_asset_name";

const std::string config_freespins_x = "freespins_x";
const std::string config_freespins_y = "freespins_y";
const std::string config_freespins_font_size = "freespins_font_size";

const std::string config_meter_grand_x = "meter_grand_x";
const std::string config_meter_grand_y = "meter_grand_y";
const std::string config_meter_grand_size_x = "meter_grand_size_x";
const std::string config_meter_grand_size_y = "meter_grand_size_y";
const std::string config_meter_grand_asset_name = "meter_grand_asset_name";
const std::string config_meter_major_x = "meter_major_x";
const std::string config_meter_major_y = "meter_major_y";
const std::string config_meter_major_size_x = "meter_major_size_x";
const std::string config_meter_major_size_y = "meter_major_size_y";
const std::string config_meter_major_asset_name = "meter_major_asset_name";
const std::string config_meter_minor_x = "meter_minor_x";
const std::string config_meter_minor_y = "meter_minor_y";
const std::string config_meter_minor_size_x = "meter_minor_size_x";
const std::string config_meter_minor_size_y = "meter_minor_size_y";
const std::string config_meter_minor_asset_name = "meter_minor_asset_name";

const std::string config_meter_grand_threshhold = "meter_grand_threshhold";
const std::string config_meter_major_threshhold = "meter_major_threshhold";
const std::string config_meter_minor_threshhold = "meter_minor_threshhold";
const std::string config_meter_grand_script_name = "meter_grand_script_name";
const std::string config_meter_major_script_name = "meter_major_script_name";
const std::string config_meter_minor_script_name = "meter_minor_script_name";
const std::string config_meter_lose_script_name = "meter_lose_script_name";

const std::string config_prize_meter_x = "prize_meter_x";
const std::string config_prize_meter_y = "prize_meter_y";
const std::string config_prize_meter_size_x = "prize_meter_size_x";
const std::string config_prize_meter_size_y = "prize_meter_size_y";

const std::string config_entry_script = "entry_script_name";
const std::string config_first_entry_script = "entry_first_script_name";  // first time the entry script is run.
const std::string config_done_script = "done_script";

// ===================================================================================================================
// SLOT 4x4Spot
//

gs::g2024::Slot_4x4Spot::Slot_4x4Spot(const std::string& registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(registration_name, context), outcome_next_idx_(0), running_state_(), action_state_(Slot_4x4SpotActionState::NEW),
    freespins_texture_(nullptr), free_spins_show_(false)
{
    std::random_device dev;
    rng_ = std::mt19937(dev());
}

gs::g2024::Slot_4x4Spot::~Slot_4x4Spot()
{
    SDL_DestroyTexture(freespins_texture_);
}

void gs::g2024::Slot_4x4Spot::activate()
{
    if ((action_state_ == Slot_4x4SpotActionState::DONE)||(action_state_ == Slot_4x4SpotActionState::NEW))
    {
        // reset the board.
        // TODO maybe on deactivation?
        freespins_remaining_ = number_free_spins;
        freespins_remaining_spoiled_ = false;

        reset_spin_pay();

        spots_.clear();
        spots_.resize(total_spots);
        for (int idx = 0; idx < total_spots; idx++)
        {
            spots_[idx] = Slot_4x4SpotSpot{};
            spots_[idx].name = all_spots_[random_spot_(rng_)];
        }

        // TODO maybe move this.  Not sure if I want this dynamic.
        // Row 1
        spots_[0].x = start_x_;
        spots_[0].y = start_y_;
        spots_[0].outcome = 0;
        spots_[1].x = start_x_ + distance_x_;
        spots_[1].y = start_y_;
        spots_[1].outcome = 1;
        spots_[2].x = start_x_ + (distance_x_*2);
        spots_[2].y = start_y_;
        spots_[2].outcome = 2;
        spots_[3].x = start_x_ + (distance_x_*3);
        spots_[3].y = start_y_;
        spots_[3].outcome = 3;

        // Row 2
        spots_[4].x = start_x_;
        spots_[4].y = start_y_ + distance_y_;
        spots_[4].outcome = 4;
        spots_[5].x = start_x_ + distance_x_;
        spots_[5].y = start_y_ + distance_y_;
        spots_[5].outcome = 5;
        spots_[6].x = start_x_ + (distance_x_*2);
        spots_[6].y = start_y_ + distance_y_;
        spots_[6].outcome = 6;
        spots_[7].x = start_x_ + (distance_x_*3);
        spots_[7].y = start_y_ + distance_y_;
        spots_[7].outcome = 7;

        // Row 3
        spots_[8].x = start_x_;
        spots_[8].y = start_y_ + (distance_y_ * 2);
        spots_[8].outcome = 8;
        spots_[9].x = start_x_ + distance_x_;
        spots_[9].y = start_y_ + (distance_y_ * 2);
        spots_[9].outcome = 9;
        spots_[10].x = start_x_ + (distance_x_*2);
        spots_[10].y = start_y_ + (distance_y_ * 2);
        spots_[10].outcome = 10;
        spots_[11].x = start_x_ + (distance_x_*3);
        spots_[11].y = start_y_ + (distance_y_ * 2);
        spots_[11].outcome = 11;

        // Row 4
        spots_[12].x = start_x_;
        spots_[12].y = start_y_ + (distance_y_ * 3);
        spots_[12].outcome = 12;
        spots_[13].x = start_x_ + distance_x_;
        spots_[13].y = start_y_ + (distance_y_ * 3);
        spots_[13].outcome = 13;
        spots_[14].x = start_x_ + (distance_x_*2);
        spots_[14].y = start_y_ + (distance_y_ * 3);
        spots_[14].outcome = 14;
        spots_[15].x = start_x_ + (distance_x_*3);
        spots_[15].y = start_y_ + (distance_y_ * 3);
        spots_[15].outcome = 15;

        outcome_points_ = 0;
        freespins_remaining_spoiled_ = true;

        action_state_ = Slot_4x4SpotActionState::ENTRY_SCRIPT;
    }
    // TODO need an error for a bad state?

    running_state_ = GSGameState::RENDERING_ACTIVE;
}

void gs::g2024::Slot_4x4Spot::deactivate()
{
    action_state_ = Slot_4x4SpotActionState::DONE;
    running_state_ = GSGameState::DONE;
}

void gs::g2024::Slot_4x4Spot::init_impl(GSGameState state, std::shared_ptr<ConfigGame> config)
{
    point_scale_ = config->get_config<int>(config_point_scale);

    start_x_ = config->get_config<int>(config_start_x);
    distance_x_ = config->get_config<int>(config_distance_x);
    start_y_ = config->get_config<int>(config_start_y);
    distance_y_ = config->get_config<int>(config_distance_y);
    spot_size_x_ = config->get_config<int>(config_spot_size_x);
    spot_size_y_ = config->get_config<int>(config_spot_size_y);

    win_start_x_ = config->get_config<int>(config_win_start_x);
    win_distance_x_ = config->get_config<int>(config_win_distance_x);
    win_start_y_ = config->get_config<int>(config_win_start_y);
    win_distance_y_ = config->get_config<int>(config_win_distance_y);
    win_font_size_ = config->get_config<int>(config_win_font_size);
    win_wait_ = config->get_config<int>(config_win_wait);
    lose_wait_ =  config->get_config<int>(config_lose_wait);
    win_win_audio_asset_name_ = config->get_config<std::string>(config_win_win_audio_asset_name);
    win_lose_audio_asset_name_ = config->get_config<std::string>(config_win_lose_audio_asset_name);

    audio_spinning_name_ = config->get_config<std::string>(config_audio_spinning_name);

    freespins_x_ = config->get_config<int>(config_freespins_x);
    freespins_y_ = config->get_config<int>(config_freespins_y);
    freespins_font_size_ = config->get_config<int>(config_freespins_font_size);

    meter_grand_x_ = config->get_config<int>(config_meter_grand_x);
    meter_grand_y_ = config->get_config<int>(config_meter_grand_y);
    meter_grand_size_x_ = config->get_config<int>(config_meter_grand_size_x);
    meter_grand_size_y_ = config->get_config<int>(config_meter_grand_size_y);
    meter_grand_asset_name_ = config->get_config<std::string>(config_meter_grand_asset_name);
    meter_major_x_ = config->get_config<int>(config_meter_major_x);
    meter_major_y_ = config->get_config<int>(config_meter_major_y);
    meter_major_size_x_ = config->get_config<int>(config_meter_major_size_x);
    meter_major_size_y_ = config->get_config<int>(config_meter_major_size_y);
    meter_major_asset_name_ = config->get_config<std::string>(config_meter_major_asset_name);
    meter_minor_x_ = config->get_config<int>(config_meter_minor_x);
    meter_minor_y_ = config->get_config<int>(config_meter_minor_y);
    meter_minor_size_x_ = config->get_config<int>(config_meter_minor_size_x);
    meter_minor_size_y_ = config->get_config<int>(config_meter_minor_size_y);
    meter_minor_asset_name_ = config->get_config<std::string>(config_meter_minor_asset_name);

    meter_grand_threshhold_ = config->get_config<int>(config_meter_grand_threshhold);
    meter_major_threshhold_ = config->get_config<int>(config_meter_major_threshhold);
    meter_minor_threshhold_ = config->get_config<int>(config_meter_minor_threshhold);
    meter_grand_script_name_ = config->get_config<std::string>(config_meter_grand_script_name);
    meter_major_script_name_ = config->get_config<std::string>(config_meter_major_script_name);
    meter_minor_script_name_ = config->get_config<std::string>(config_meter_minor_script_name);
    meter_lose_script_name_ = config->get_config<std::string>(config_meter_lose_script_name);

    prize_meter_x_ = config->get_config<int>(config_prize_meter_x);
    prize_meter_y_ = config->get_config<int>(config_prize_meter_y);
    prize_meter_size_x_ = config->get_config<int>(config_prize_meter_size_x);
    prize_meter_size_y_ = config->get_config<int>(config_prize_meter_size_y);

    entry_script_ = config->get_config<std::string>(config_entry_script);
    first_entry_script_ = config->get_config<std::string>(config_first_entry_script);
    done_script_ = config->get_config<std::string>(config_done_script);

    if (config->configs.contains(config_spots))
    {
        all_spots_ = split(config->get_config<std::string>(config_spots), script_term_delimiter);
    }
    else
    {
        throw std::runtime_error("No spots configured for Slot_4x4Spot.");
    }
    random_spot_ = std::uniform_int_distribution<std::mt19937::result_type>(0, all_spots_.size()-1);
    random_nudge_ = std::uniform_int_distribution<std::mt19937::result_type>(0, max_nudge);
    random_stop_time_ = std::uniform_int_distribution<std::mt19937::result_type>(500, max_spin_wait);

}

bool gs::g2024::Slot_4x4Spot::offer_key(SDL_Keycode keycode)
{
    return false;
}

std::map<std::string, int> Slot_4x4Spot_map =
{
    { signal_select, 0 }
};


bool gs::g2024::Slot_4x4Spot::offer_signal(const directive signal)
{
    switch(action_state_)
    {
    // NOP states
    case Slot_4x4SpotActionState::NEW:
    case Slot_4x4SpotActionState::ENTRY_SCRIPT:
    case Slot_4x4SpotActionState::SPINNING:
    case Slot_4x4SpotActionState::SPIN_OUTCOME:
    case Slot_4x4SpotActionState::SPIN_OUTCOME_UNDERWAY:
    case Slot_4x4SpotActionState::OUTCOME:
    case Slot_4x4SpotActionState::OUTCOME_UNDERWAY:
        break;

    case Slot_4x4SpotActionState::DONE:
        freespins_remaining_ = number_free_spins;
        break;

    case Slot_4x4SpotActionState::OUTCOME_WAIT:
    case Slot_4x4SpotActionState::SPIN_OUTCOME_COMPLETE:
        if (!signal.empty() && signal[0] == signal_done)
        {
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[done_script_]);
            action_state_ = Slot_4x4SpotActionState::DONE;

        }
        break;

    case Slot_4x4SpotActionState::RUNNING_ENTRY_SCRIPT:
        if (!signal.empty() && signal[0] == signal_done)
        {
            action_state_ = Slot_4x4SpotActionState::WAITING;
        }
        break;

    case Slot_4x4SpotActionState::WAITING:
        if (!signal.empty())
        {
            switch(Slot_4x4Spot_map[signal[0]])
            {
            case 0: // signal_spin:
                freespins_remaining_--;
                freespins_remaining_spoiled_ = true;
                reset_spin_pay();
                start_spin();
                action_state_ = Slot_4x4SpotActionState::SPINNING;
                break;
            default:
                assert(false);
            }
        }
        break;
    default:
        assert(false);
    }
    return false;
}

gs::GSGameState gs::g2024::Slot_4x4Spot::game_loop_impl(const GSGameState state)
{
    auto asset_manager = context_->get_asset_manager();

    render_main();

    switch(action_state_)
    {
    case Slot_4x4SpotActionState::NEW:
        assert(false);  // Should never get here as activate will move to ENTRY_SCRIPT.

    case Slot_4x4SpotActionState::ENTRY_SCRIPT:
        {
            std::string run_script = entry_script_;
            if (context_->get_global(config_global_first).empty())
            {
                run_script = first_entry_script_;
                context_->set_global(config_global_first, "TRUE");
            }
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[run_script]);
            action_state_ = Slot_4x4SpotActionState::RUNNING_ENTRY_SCRIPT;
        }
        break;

    case Slot_4x4SpotActionState::RUNNING_ENTRY_SCRIPT:
        // This should never run, since the game is frozen.
        break;

    case Slot_4x4SpotActionState::WAITING:
        free_spins_show_ = true;
        break;

    case Slot_4x4SpotActionState::SPINNING:
        spinning();
        break;

    case Slot_4x4SpotActionState::SPIN_OUTCOME:
        start_spin_outcome();
        action_state_ = Slot_4x4SpotActionState::SPIN_OUTCOME_UNDERWAY;
        break;

    case Slot_4x4SpotActionState::SPIN_OUTCOME_UNDERWAY:
        do_spin_outcome();
        break;

    case Slot_4x4SpotActionState::SPIN_OUTCOME_COMPLETE:
        if (freespins_remaining_ == 0 || (!current_outcome_[outcome_script_spot].empty() && current_outcome_[outcome_script_spot] != outcome_script_nop))
        {
            action_state_ = Slot_4x4SpotActionState::OUTCOME;
        } else
        {
            action_state_ = Slot_4x4SpotActionState::WAITING;
        }
        break;

    case Slot_4x4SpotActionState::OUTCOME:
        do_outcome();
        action_state_ = Slot_4x4SpotActionState::OUTCOME_WAIT;
        break;

    case Slot_4x4SpotActionState::OUTCOME_WAIT:
        break;

    case Slot_4x4SpotActionState::DONE:
        break;
    }

    return running_state_;
}

// =======================================================================================
// Rendering

void gs::g2024::Slot_4x4Spot::render_board()
{
    if (action_state_ != Slot_4x4SpotActionState::ENTRY_SCRIPT && action_state_ != Slot_4x4SpotActionState::RUNNING_ENTRY_SCRIPT)
    {
        for (const auto& item: spots_)
        {
            if (item.step == 0)
            {
                auto dest = SDL_Rect{item.x, item.y, spot_size_x_, spot_size_y_} ;
                SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(item.name)->get_texture(),
                    nullptr, & dest );
            }
            else
            {
                // new
                auto dest = SDL_Rect{item.x, item.y, spot_size_x_, item.step};
                auto obj = SDL_Rect{0, spot_size_y_ - item.step, spot_size_x_, item.step};
                SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(item.name)->get_texture(),
                    & obj, & dest );

                // previous
                dest = SDL_Rect{item.x, item.y + item.step, spot_size_x_, spot_size_y_ - item.step};
                obj = SDL_Rect{0, 0, spot_size_x_, spot_size_y_ - item.step};
                SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(item.previous)->get_texture(),
                    & obj, & dest );
            }
        }

        for (int idx = 0; idx < 4; idx++)
        {
            if (spin_pay_textures_[idx] != nullptr)
            {
                auto xstart = win_start_x_ + (win_distance_x_ * idx);
                const auto dest = SDL_Rect{xstart, win_start_y_ + (win_distance_y_ * idx),
                    spin_pay_textures_[idx]->get_width(), spin_pay_textures_[idx]->get_height()};
                SDL_RenderCopy(context_->get_renderer(), spin_pay_textures_[idx]->get_texture(),NULL, & dest );
            }
        }
    }

}

void gs::g2024::Slot_4x4Spot::render_remaining()
{
    if (freespins_remaining_spoiled_ || freespins_texture_ == nullptr)
    {

        if (freespins_texture_ != nullptr)
        {
            SDL_DestroyTexture(freespins_texture_);
        }

        const auto font = context_->get_asset_manager()->get_font();
        TTF_SetFontSize(font, freespins_font_size_);
        const auto text_surface = TTF_RenderText_Solid(font,
                                                 std::to_string(freespins_remaining_).c_str(),
                                                 // TODO configurable.
                                                 SDL_Color{50, 130, 50, 0});
        if (text_surface == nullptr)
        {
            throw std::runtime_error(gs::format_string("Unable to freespin text.  %s", TTF_GetError()));
        }

        freespins_current_x_ = text_surface->w;
        freespins_current_y_ = text_surface->h;
        freespins_texture_ = SDL_CreateTextureFromSurface(context_->get_renderer(), text_surface);
        SDL_FreeSurface(text_surface);
        context_->coins_clean();

        freespins_remaining_spoiled_ = false;
    }

    if (free_spins_show_)
    {
        const auto dest = SDL_Rect{freespins_x_ + 20, freespins_y_ + 20, freespins_current_x_, freespins_current_y_};
        SDL_RenderCopy(context_->get_renderer(), freespins_texture_, nullptr, & dest );
    }

    // Prizes
    if (won_grand())
    {
        const auto dest = SDL_Rect{meter_grand_x_, meter_grand_y_, meter_grand_size_x_, meter_grand_size_y_};
        SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(meter_grand_asset_name_)->get_texture(),
            nullptr, & dest );
    }
    if (won_major())
    {
        const auto dest = SDL_Rect{meter_major_x_, meter_major_y_, meter_major_size_x_, meter_major_size_y_};
        SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(meter_major_asset_name_)->get_texture(),
            nullptr, & dest );

    }
    if (won_minor())
    {
        const auto dest = SDL_Rect{meter_minor_x_, meter_minor_y_, meter_minor_size_x_, meter_minor_size_y_};
        SDL_RenderCopy(context_->get_renderer(), context_->get_asset_manager()->get_named_texture(meter_minor_asset_name_)->get_texture(),
            nullptr, & dest );
    }

    // Prize meter
    float bar_y_pct = 100.0;
    if (outcome_points_ < point_scale_)
    {
        bar_y_pct = static_cast<float>(outcome_points_) / static_cast<float>(point_scale_);
    }

    const auto y_par_size =  static_cast<int>(std::round(prize_meter_size_y_ * bar_y_pct));
    auto y_offset = prize_meter_size_y_ - y_par_size;
    if (y_offset < 0)
    {
        y_offset = 0;
    }

    SDL_Rect rect;
    rect.x = prize_meter_x_;
    rect.y = prize_meter_y_ + y_offset;
    rect.w = prize_meter_size_x_;
    rect.h = y_par_size;

    // TODO make configurable.
    SDL_SetRenderDrawColor(context_->get_renderer(), 255, 0, 0, 255);
    SDL_RenderFillRect(context_->get_renderer(), &rect);
}

void gs::g2024::Slot_4x4Spot::render_main()
{
    auto asset_manager = context_->get_asset_manager();

    // == ALWAYS RENDERS ==========================================
    render_remaining();
    render_board();

    // == DEPENDENT RENDERS =======================================
}

// =======================================================================================
// Logic

void gs::g2024::Slot_4x4Spot::start_spin()
{
    current_outcome_ = context_->get_loaded_game()->config->outcomes[outcome_next_idx_];
    outcome_next_idx_++;

    for (auto& item : spots_)
    {
        item.state = Slot_4x4SpotState::RUNNING;
        item.previous = item.name;
        item.name = all_spots_[random_spot_(rng_)];
        //item.step = random_nudge_(rng_) * nudge_size;  This caused it to act very strangely.
        item.step = 0;
        item.stop_time = SDL_GetTicks() + random_stop_time_(rng_);
    }

    Mix_PlayChannel(0, context_->get_asset_manager()->get_named_audio(audio_spinning_name_)->get_chunk(),0);
}

void gs::g2024::Slot_4x4Spot::spinning()
{
    const auto now = SDL_GetTicks();
    int frozen = 0;
    for (auto& item : spots_)
    {

        switch (item.state)
        {
        case Slot_4x4SpotState::RUNNING:
            if (item.step == 0)
            {
                item.previous = item.name;
                if (item.stop_time < now)
                {
                    item.state = Slot_4x4SpotState::STOPPING;
                    item.name = get_outcome_value<std::string>(current_outcome_, item.outcome);
                }
                else
                {
                    item.name = all_spots_[random_spot_(rng_)];
                }
            }
            else
            {
                // Nudge it
                item.step = item.step + nudge_size;
                if (item.step >= spot_size_y_)
                {
                    item.step = 0;
                }
            }
            break;

        case Slot_4x4SpotState::STOPPING:
            if (item.step == 0)
            {
                item.state = Slot_4x4SpotState::FROZEN;
                frozen++;

            } else
            {
                // Nudge it
                item.step = item.step + nudge_size;
                if (item.step >= spot_size_y_)
                {
                    item.step = 0;
                }
            }
            break;

        case Slot_4x4SpotState::FROZEN:
            frozen++;
            break;
        }

    }

    if (frozen == spots_.size())
    {
        action_state_ = Slot_4x4SpotActionState::SPIN_OUTCOME;
        Mix_HaltChannel(0);
    }
}

void gs::g2024::Slot_4x4Spot::start_spin_outcome()
{
    spin_outcome_step_timing_ = SDL_GetTicks();
    spin_outcome_step_ = 0;
}

void gs::g2024::Slot_4x4Spot::do_spin_outcome()
{
    if (spin_outcome_step_timing_ < SDL_GetTicks())
    {
        switch(spin_outcome_step_)
        {
        case 0:
        case 1:
        case 2:
        case 3:
            spin_outcome_step_timing_ = do_spin_outcome_step(spin_outcome_step_);
            spin_outcome_step_++;
            break;

        case 4:
            // Go to outcome.
            action_state_ = Slot_4x4SpotActionState::SPIN_OUTCOME_COMPLETE;
            break;
        default:
            assert(false);
        }
    }
}

Uint32 gs::g2024::Slot_4x4Spot::do_spin_outcome_step(const int step)
{

    // Points
    const auto val = get_outcome_value<std::string>(current_outcome_, outcome_value_spot_start + step);
    spin_pay_textures_[step] = get_value_texture(val, win_font_size_);
    const auto ival = get_outcome_value<int>(current_outcome_, outcome_value_spot_start + step);
    outcome_points_ += ival;

    // Sound
    Uint32 wait_time = 0;
    if (ival > 0)
    {
        PLAYSOUNDASSETC(win_win_audio_asset_name_, 2);
        wait_time = SDL_GetTicks() + win_wait_;
    }
    else
    {
        PLAYSOUNDASSETC(win_lose_audio_asset_name_, 2);
        wait_time = SDL_GetTicks() + lose_wait_;
    }

    return wait_time;
}

std::shared_ptr<gs::GSTexture> gs::g2024::Slot_4x4Spot::get_value_texture(const std::string& val, const int font_size) const
{
    const auto font = context_->get_asset_manager()->get_font();
    TTF_SetFontSize(font, font_size);
    const auto text_surface = TTF_RenderText_Solid(font, val.c_str(),
                                             // TODO configurable.
                                             SDL_Color{50, 250, 100, 0});
    if (text_surface == nullptr)
    {
        throw std::runtime_error(gs::format_string("Unable to render value text.  %s", TTF_GetError()));
    }

    auto tex = SDL_CreateTextureFromSurface(context_->get_renderer(), text_surface);
    SDL_FreeSurface(text_surface);
    return std::make_shared<GSTexture>(tex, text_surface->w, text_surface->h);
}

void gs::g2024::Slot_4x4Spot::do_outcome() const
{
    if ((current_outcome_.size() > outcome_script_spot)&&(!current_outcome_[outcome_script_spot].empty())&&
        (current_outcome_[outcome_script_spot] != outcome_script_nop))
    {
        context_->get_script_manager()->run(context_->get_loaded_game()->config->get_script(current_outcome_[outcome_script_spot]));
    }
    else
    {
        std::string sname;
        if (outcome_points_ >= meter_grand_threshhold_)
        {
            sname = meter_grand_script_name_;
        }
        else if (outcome_points_ >= meter_major_threshhold_)
        {
            sname = meter_major_script_name_;
        }
        else if (outcome_points_ >= meter_minor_threshhold_)
        {
            sname = meter_minor_script_name_;
        }
        else
        {
            sname = meter_lose_script_name_;
        }
        context_->get_script_manager()->run(context_->get_loaded_game()->config->get_script(sname));
    }

}

bool gs::g2024::Slot_4x4Spot::won_grand() const
{
    if (outcome_points_ >= meter_grand_threshhold_)
    {
        return true;
    }
    return false;
}
bool gs::g2024::Slot_4x4Spot::won_major() const
{
    if (outcome_points_ >= meter_major_threshhold_)
    {
        return true;
    }
    return false;
}
bool gs::g2024::Slot_4x4Spot::won_minor() const
{
    if (outcome_points_ >= meter_minor_threshhold_)
    {
        return true;
    }
    return false;
}

void gs::g2024::Slot_4x4Spot::reset_spin_pay()
{
    spin_pay_textures_[0] = nullptr;
    spin_pay_textures_[1] = nullptr;
    spin_pay_textures_[2] = nullptr;
    spin_pay_textures_[3] = nullptr;
}

