/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file pick_2.h
 *
 * Pick one of two options.
 */

#ifndef GSLOTGAME_G2022_PICK_2_GAME_H
#define GSLOTGAME_G2022_PICK_2_GAME_H

#include "context.h"
#include "game.h"

#include <bits/random.h>

#include "objects.h"

namespace gs::g2024
{

// ===================================================================================================================
// CONFIG
//

// ===================================================================================================================
// CLASSES AND FUNCTIONS
//

enum class Pick2ActionState
{
    NEW,
    ENTRY_SCRIPT,
    RUNNING_ENTRY_SCRIPT,
    WAITING,
    OUTCOME,
    OUTCOME_WAIT,
    DONE
};

class Pick2 final : public GSGame
{
    pixel_size spot_0_x_ = 0;
    pixel_size spot_0_y_ = 0;
    pixel_size spot_0_size_x_ = 0;
    pixel_size spot_0_size_y_ = 0;

    pixel_size spot_1_x_ = 0;
    pixel_size spot_1_y_ = 0;
    pixel_size spot_1_size_x_ = 0;
    pixel_size spot_1_size_y_ = 0;
    std::string spot_asset_name_;

    std::string pick_spot_object_name_;

    pixel_size picksign_x_ = 0;
    pixel_size picksign_y_ = 0;
    pixel_size picksign_size_x_ = 0;
    pixel_size picksign_size_y_ = 0;
    std::string picksign_asset_name_;

    pixel_size chooser_size_x_ = 0;
    pixel_size chooser_size_y_ = 0;
    std::string chooser_asset_name_;

    std::string first_entry_script_;
    std::string entry_script_;
    std::string done_script_;

    directive current_outcome_;
    int       outcome_next_idx_;
    Uint32    outcome_next_time_;

    GSGameState running_state_;
    Pick2ActionState action_state_;

    int which_pick_;        // 0 for left, 1 for right, 2 for neither
    std::shared_ptr<GSObject> chooser_object_;
    std::shared_ptr<GSObject> spot_open_door_object_;
    std::shared_ptr<GSObject> pick_sign_object_;

    void start_outcome();
    //void check_select();
    void render_board();
    void render_cursor() const;
    void render_remaining();
    void render_main();
    //void check_result() const;

public:
    Pick2(const std::string& registration_name, const std::shared_ptr<GSContext>& context);
    ~Pick2() override;
    GSGAME_VIRTUAL_OVERRIDES

};

}

#endif // GSLOTGAME_G2022_PICK_2_GAME_H
