/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file match_15.h
 *
 * Match 15.
 */

#ifndef GSLOTGAME_G2022_MATCH_15_GAME_H
#define GSLOTGAME_G2022_MATCH_15_GAME_H

#include "context.h"
#include "game.h"

#include <bits/random.h>

#include "objects.h"

namespace gs::g2024
{

// ===================================================================================================================
// CONFIG
//

// ===================================================================================================================
// CLASSES AND FUNCTIONS
//

enum class Match15ActionState
{
    NEW,
    ENTRY_SCRIPT,
    RUNNING_ENTRY_SCRIPT,
    WAITING,
    OUTCOME,
    OUTCOME_UNDERWAY,
    OUTCOME_WAIT,
    DONE
};

struct Match15Pick
{
    int cur_x, cur_y_;
    std::string name_;
    std::shared_ptr<GSTexture> texture_;
};

class Match15 final : public GSGame {

    pixel_size start_x_ = 0;
    pixel_size distance_x_ = 0;
    pixel_size start_y_ = 0;
    pixel_size distance_y_ = 0;
    pixel_size spot_size_x_ = 0;
    pixel_size spot_size_y_ = 0;
    std::string pick_asset_name_;

    pixel_size chooser_start_x_ = 0;
    pixel_size chooser_distance_x_ = 0;
    pixel_size chooser_start_y_ = 0;
    pixel_size chooser_distance_y_ = 0;
    pixel_size chooser_spot_size_x_ = 0;
    pixel_size chooser_spot_size_y_ = 0;
    std::string chooser_asset_name_;

    pixel_size remaining_box_x_ = 0;
    pixel_size remaining_box_y_ = 0;
    pixel_size remaining_box_size_x_ = 0;
    pixel_size remaining_box_size_y_ = 0;
    pixel_size remaining_box_font_size_ = 0;

    std::string reveal_audio_;

    std::string first_entry_script_;
    std::string entry_script_;
    std::string done_script_;

    directive all_spots_;
    directive current_outcome_;
    int       outcome_next_idx_;
    int       outcome_step_;
    Uint32    outcome_next_time_;

    GSGameState running_state_;
    Match15ActionState action_state_;

    int cur_x_, cur_y_;
    std::shared_ptr<GSTexture> chooser_texture_;
    std::vector<Match15Pick> picks_;

    SDL_Texture* remaining_texture_;
    int picks_remaining_;
    bool picks_remaining_spoiled_;

    void start_outcome();
    void do_outcome();
    void check_select();
    void render_board();
    void render_cursor() const;
    void render_remaining();
    void render_main();
    //void check_result() const;

public:
    Match15(const std::string& registration_name, const std::shared_ptr<GSContext>& context);
    ~Match15() override;
    GSGAME_VIRTUAL_OVERRIDES

};

}


#endif // GSLOTGAME_G2022_MATCH_15_GAME_H
