/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file slot_3reel.h
 *
 * A three reel slot.
 */

#include <string>

#include "config.h"

#include <SDL.h>
#include <SDL_mixer.h>

#include "./slot_3reel.h"

#include <loader.h>
#include <util.h>
#include "../utils.h"

#include "script.h"

// ===================================================================================================================
// Assets and tools.
//

// Configuration points.

const char flag_reel_ramp = '!';   // Can be used with the first spot in the 3rd reel.  !5.0! indicating a 5.0 second play of the reel ramp.
const int flag_reel_spot = 6;

// Audio
const std::string config_audio_spinning_name = "3reel_audio_running";
const std::string config_audio_stop_name = "3reel_audio_stop";
const std::string config_audio_ramp_name = "3reel_audio_ramp";

// Static points
const std::string texture_main_slot_spin_on = "main_slot_spin_on";

// Button
const gs::pixel_size spin_pushed_size_x = 200;
const gs::pixel_size spin_pushed_size_y = 200;
const gs::pixel_size pos_spin_pushed_x = 1200;
const gs::pixel_size pos_spin_pushed_y = 660;

// Coin box
const gs::pixel_size coin_box_size_x = 120;
const gs::pixel_size coin_box_size_y = 120;
const gs::pixel_size pos_coin_box_x = 1239;
const gs::pixel_size pos_coin_box_y = 226;

#define WHEEL_STOP_SOUND PLAYSOUNDASSET(audio_stop_name_)
#define WHEEL_RAMP_SOUND PLAYSOUNDASSET(audio_ramp_name_)

// ===================================================================================================================
// Simple pick one of two choices.
//

#define REEL_SET_SLOT(_NUM_)  (dest_reel3_slot_##_NUM_ ).h = asset_reel3_spot_size_y; (dest_reel3_slot_##_NUM_ ).w = asset_reel3_spot_size_x; \
  (dest_reel3_slot_##_NUM_ ).x = pos_reel3_spot_x_##_NUM_;  (dest_reel3_slot_##_NUM_ ).y = pos_reel3_spot_y_##_NUM_;

gs::g2024::Slot_3Reel::Slot_3Reel(const std::string& registration_name, const std::shared_ptr<GSContext>& context) :
    GSGame(registration_name, context), running_state_(GSGameState::NEW), outcome_next_idx_(0), next_stop_(0),
    sound_reels_channel_(0),
    pos_spin_button_(),
    action_state_()
{
    REEL_SET_SLOT(1)
    REEL_SET_SLOT(2)
    REEL_SET_SLOT(3)
    REEL_SET_SLOT(4)
    REEL_SET_SLOT(5)
    REEL_SET_SLOT(6)
    REEL_SET_SLOT(7)
    REEL_SET_SLOT(8)
    REEL_SET_SLOT(9)

    std::random_device dev;
    rng_ = std::mt19937(dev());

    random_y_size_ = std::uniform_int_distribution<std::mt19937::result_type>(0, asset_reel3_spot_size_y - 1);
    random_stop_1_ = std::uniform_int_distribution<std::mt19937::result_type>(
        reel3_stop_1_min_time_ms, reel3_stop_1_max_time_ms);
    random_stop_2_ = std::uniform_int_distribution<std::mt19937::result_type>(
        reel3_stop_2_min_time_ms, reel3_stop_2_max_time_ms);
}

gs::g2024::Slot_3Reel::~Slot_3Reel()
{
    SDL_DestroyTexture(coin_texture_);
}

void gs::g2024::Slot_3Reel::activate()
{
    running_state_ = GSGameState::RENDERING_ACTIVE;
    action_state_ = Reel3GameActionState::WAITING;
}

void gs::g2024::Slot_3Reel::deactivate()
{
    running_state_ = GSGameState::WAIT_START;
}

void gs::g2024::Slot_3Reel::init_impl(GSGameState state, std::shared_ptr<ConfigGame> config)
{
    if (config->configs.contains(config_spots))
    {
        all_spots_ = split(config->configs[config_spots], script_term_delimiter);
        spots_exclude_random_ = split(config->configs[config_spots_exclude_from_random], script_term_delimiter);

        // TODO the actual number of reels.
        random6_ = std::uniform_int_distribution<std::mt19937::result_type>(0, all_spots_.size()-1);

        for (int idx = 0; idx < 9; idx++)
        {
            spot_names_[idx] = get_random_spot();
        }
    }
    else
    {
        throw std::runtime_error("No spots configured for slot_3reel.");
    }

    // Configured signals
    if (config->configs.contains(config_signal_spin))
    {
        signal_spin_ = config->configs[config_signal_spin];
    }

    auto assets = context_->get_asset_manager();

    // Audio
    audio_spinning_name_ = config->get_config<std::string>(config_audio_spinning_name);
    audio_stop_name_ = config->get_config<std::string>(config_audio_stop_name);
    audio_ramp_name_ = config->get_config<std::string>(config_audio_ramp_name);

    // Textures
    pos_spin_button_ = {pos_spin_pushed_x, pos_spin_pushed_y, spin_pushed_size_x, spin_pushed_size_y};

    action_state_ = Reel3GameActionState::WAITING;
}

bool gs::g2024::Slot_3Reel::offer_key(SDL_Keycode keycode)
{
    return false;
}

bool gs::g2024::Slot_3Reel::offer_signal(const directive signal)
{
    if (action_state_ == Reel3GameActionState::WAITING)
    {
        if (!signal.empty() && signal[0] == signal_spin_)
        {
            if (context_->get_coins() > 0) {
                if (outcome_next_idx_ >= context_->get_loaded_game()->config->outcomes.size())
                {
                    throw std::runtime_error("Outcome of outcomes for slot_3reel.");
                }
                current_outcome_ = context_->get_loaded_game()->config->outcomes[outcome_next_idx_];
                outcome_next_idx_++;

                context_->remove_coins(1);
                action_state_ = Reel3GameActionState::START_SPIN;
                running_state_ = GSGameState::RENDERING_QUIET;
            }
        }
    }
    else if (action_state_ == Reel3GameActionState::RESULT_WAIT)
    {
        if (!signal.empty() && signal[0] == signal_done)
        {
            action_state_ = Reel3GameActionState::WAITING;
        }
    }

    return false;
}

gs::GSGameState gs::g2024::Slot_3Reel::game_loop_impl(const GSGameState state)
{
    const auto asset_manager = context_->get_asset_manager();

    render_wheels();
    render_main();

    switch(action_state_)
    {
        case Reel3GameActionState::WAITING:
            break;

        case Reel3GameActionState::START_SPIN:
            start_spin_positions();
            next_stop_ = SDL_GetTicks() + random_stop_1_(rng_);
            action_state_ = Reel3GameActionState::ROLLING_REEL_1;
            stop_stage_ = 0;
            sound_reels_channel_ = PLAYSOUNDASSET(audio_spinning_name_);
            break;

        case Reel3GameActionState::STOP_REEL_1:
            if ((stop_stage_ > 2) &&
                ( (current_wheel_roll_1_ >= (asset_reel3_spot_half_size_y - asset_reel3_spot_step)) &&
                  (current_wheel_roll_1_ <= (asset_reel3_spot_half_size_y + asset_reel3_spot_step)) )  )
            {
                current_wheel_roll_1_ = asset_reel3_spot_half_size_y;
                stop_stage_ = 0;
                action_state_ = Reel3GameActionState::ROLLING_REEL_2;
                WHEEL_STOP_SOUND
                break;
            }
            // Otherwise keep rolling.

        case Reel3GameActionState::ROLLING_REEL_1:
            current_wheel_roll_1_ += asset_reel3_spot_step;
            current_wheel_roll_2_ += asset_reel3_spot_step;
            current_wheel_roll_3_ += asset_reel3_spot_step;
            rotate_spots();
            check_stop_time(random_stop_2_(rng_), Reel3GameActionState::STOP_REEL_1);
            break;

        case Reel3GameActionState::STOP_REEL_2:
            if ((stop_stage_ > 2) &&
                ( (current_wheel_roll_2_ >= (asset_reel3_spot_half_size_y - asset_reel3_spot_step)) &&
                  (current_wheel_roll_2_ <= (asset_reel3_spot_half_size_y + asset_reel3_spot_step)) )  )
            {
                current_wheel_roll_2_ = asset_reel3_spot_half_size_y;
                stop_stage_ = 0;
                action_state_ = Reel3GameActionState::ROLLING_REEL_3;
                WHEEL_STOP_SOUND;

                // Reel ramp?
                if (current_outcome_[flag_reel_spot].starts_with(flag_reel_ramp))
                {
                    const auto tokens = split(current_outcome_[flag_reel_spot], flag_reel_ramp);
                    try
                    {
                        const auto time = std::stof(tokens[1]);
                        next_stop_ = SDL_GetTicks() + (1000 * time);

                    } catch (std::exception& e)
                    {
                        throw std::runtime_error("Bad flagged outcome entry: '" + current_outcome_[flag_reel_spot] +
                            "'.   cause:" + e.what());
                    }
                    WHEEL_RAMP_SOUND;
                }

                break;
            }
            // Otherwise keep rolling.

        case Reel3GameActionState::ROLLING_REEL_2:
            current_wheel_roll_2_ += asset_reel3_spot_step;
            current_wheel_roll_3_ += asset_reel3_spot_step;
            rotate_spots();
            check_stop_time(random_stop_2_(rng_), Reel3GameActionState::STOP_REEL_2);
            break;

        case Reel3GameActionState::STOP_REEL_3:
            if ((stop_stage_ > 2) &&
                ( (current_wheel_roll_3_ >= (asset_reel3_spot_half_size_y - asset_reel3_spot_step)) &&
                  (current_wheel_roll_3_ <= (asset_reel3_spot_half_size_y + asset_reel3_spot_step)) )  )
            {
                current_wheel_roll_3_ = asset_reel3_spot_half_size_y; // asset_reel3_spot_step;
                rotate_spots();
                action_state_ = Reel3GameActionState::LANDING_3;
                WHEEL_STOP_SOUND;
                break;
            }

        case Reel3GameActionState::ROLLING_REEL_3:
            current_wheel_roll_3_ += asset_reel3_spot_step;
            rotate_spots();
            check_stop_time(random_stop_2_(rng_), Reel3GameActionState::STOP_REEL_3);
            break;

        case Reel3GameActionState::LANDING_3:
            action_state_ = Reel3GameActionState::RESULT;
            break;

        case Reel3GameActionState::RESULT:
            Mix_HaltChannel(sound_reels_channel_);
            action_state_ = check_result();
            return GSGameState::OUTCOME;

        case Reel3GameActionState::RESULT_WAIT:
            break;

        case Reel3GameActionState::DONE:
            // Just render until the end.
            break;
    }

    frame_count_++;

    if (running_state_ != GSGameState::EVENTS_INJECTED_COMPLETE)
    {
        running_state_ = state;
    }
    return running_state_;
}

gs::g2024::Reel3GameActionState gs::g2024::Slot_3Reel::check_result() const
{

    Reel3GameActionState next = Reel3GameActionState::WAITING;
    if ((current_outcome_.size() > 10)&&(current_outcome_[9] == "SCRIPT"))
    {
        if (context_->get_loaded_game()->config->scripts.contains(current_outcome_[10]))
        {
            next = Reel3GameActionState::RESULT_WAIT;
            context_->get_script_manager()->run(context_->get_loaded_game()->config->scripts[current_outcome_[10]]);
        }
    }
    return next;
}


// =======================================================================================
// Rendering

void gs::g2024::Slot_3Reel::render_wheel(int current_wheel_roll, int pos_x, const std::string& spot_1,
    const std::string& spot_2, const std::string& spot_3) const
{
    if (current_wheel_roll == 0)
    {
        SDL_Rect src =  {0, 0, asset_reel3_spot_size_x, asset_reel3_spot_size_y };
        SDL_Rect dest =  {pos_x, pos_reel3_spot_y_1, asset_reel3_spot_size_x, asset_reel3_spot_size_y };
        TEXRENDER(context_->get_asset_manager()->get_named_texture(spot_2),src,dest);
        dest = {pos_x, pos_reel3_spot_y_1 + asset_reel3_spot_size_y, asset_reel3_spot_size_x, asset_reel3_spot_size_y };
        TEXRENDER(context_->get_asset_manager()->get_named_texture(spot_3),src,dest);
    }
    else
    {
        // 1
        SDL_Rect src =  {0, asset_reel3_spot_size_y - current_wheel_roll, asset_reel3_spot_size_x, current_wheel_roll };
        SDL_Rect dest =  {pos_x, pos_reel3_spot_y_1, asset_reel3_spot_size_x, current_wheel_roll };
        TEXRENDER(context_->get_asset_manager()->get_named_texture(spot_1),src,dest);

        // 2
        src =  {0, 0, asset_reel3_spot_size_x, asset_reel3_spot_size_y };
        dest =  {pos_x, pos_reel3_spot_y_1 + current_wheel_roll, asset_reel3_spot_size_x, asset_reel3_spot_size_y };
        TEXRENDER(context_->get_asset_manager()->get_named_texture(spot_2),src,dest);

        // 3
        src =  {0, 0, asset_reel3_spot_size_x, asset_reel3_spot_size_y - current_wheel_roll};
        dest.y = dest.y + asset_reel3_spot_size_y;
        dest.h = asset_reel3_spot_size_y - current_wheel_roll;
        TEXRENDER(context_->get_asset_manager()->get_named_texture(spot_3),src,dest);
    }
}

void gs::g2024::Slot_3Reel::render_wheels() const
{
    render_wheel(current_wheel_roll_1_, pos_reel3_spot_x_1, spot_names_[0], spot_names_[1], spot_names_[2]);
    render_wheel(current_wheel_roll_2_, pos_reel3_spot_x_4, spot_names_[3], spot_names_[4], spot_names_[5]);
    render_wheel(current_wheel_roll_3_, pos_reel3_spot_x_7, spot_names_[6], spot_names_[7], spot_names_[8]);
}

uint gs::g2024::Slot_3Reel::flip_spot(const uint spot)
{
    switch(spot)
    {
        case 0:
            return 2;
        case 1:
            return 1;
        case 2:
            return 0;
        default:
            assert(false);
    }
}

std::string gs::g2024::Slot_3Reel::get_outcome_spot(const int num, const bool dont_clean)
{
    if (dont_clean || !current_outcome_[num].starts_with(flag_reel_ramp))
    {
        return current_outcome_[num];
    }
    auto chop = split(current_outcome_[num], flag_reel_ramp);
    return chop[chop.size()-1];
}

void gs::g2024::Slot_3Reel::rotate_spots()
{
    if (current_wheel_roll_1_ >= asset_reel3_spot_size_y)
    {
        current_wheel_roll_1_ = 0;
        spot_names_[2] = spot_names_[1];
        spot_names_[1] = spot_names_[0];
        if (action_state_ == Reel3GameActionState::STOP_REEL_1)
        {
            spot_names_[0] = get_outcome_spot(flip_spot(stop_stage_));
            stop_stage_++;
        }
        else
        {
            spot_names_[0]  = get_random_spot();
        }
    }
    if (current_wheel_roll_2_ >= asset_reel3_spot_size_y)
    {
        current_wheel_roll_2_ = 0;
        spot_names_[5] = spot_names_[4];
        spot_names_[4] = spot_names_[3];
        if (action_state_ == Reel3GameActionState::STOP_REEL_2) {
            spot_names_[3] = get_outcome_spot(flip_spot(stop_stage_) + 3);
            stop_stage_++;
        }
        else
        {
            spot_names_[3] = get_random_spot();
        }
    }
    if (current_wheel_roll_3_ >= asset_reel3_spot_size_y)
    {
        current_wheel_roll_3_ = 0;
        spot_names_[8] = spot_names_[7];
        spot_names_[7] = spot_names_[6];
        if (action_state_ == Reel3GameActionState::STOP_REEL_3)
        {
            spot_names_[6] = get_outcome_spot(flip_spot(stop_stage_) + 6);
            stop_stage_++;
        }
        else
        {
            spot_names_[6] = get_random_spot();
        }
    }
}

void gs::g2024::Slot_3Reel::start_spin_positions()
{
    bool not_found = true;
    while(not_found)
    {
        current_wheel_roll_1_ = random_y_size_(rng_);
        current_wheel_roll_2_ = random_y_size_(rng_);
        current_wheel_roll_3_ = random_y_size_(rng_);

        if ( (abs((current_wheel_roll_1_ - current_wheel_roll_2_)) > pos_start_tolerance_y) &&
             (abs((current_wheel_roll_1_ - current_wheel_roll_3_)) > pos_start_tolerance_y) &&
             (abs((current_wheel_roll_2_ - current_wheel_roll_3_)) > pos_start_tolerance_y) )
        {
            not_found = false;
        }
    }
}

void gs::g2024::Slot_3Reel::render_coins()
{
    if (context_->are_coins_spoiled() || coin_texture_ == nullptr )
    {

        if (coin_texture_ != nullptr)
        {
            SDL_DestroyTexture(coin_texture_);
        }

        const auto font = context_->get_asset_manager()->get_font();
        TTF_SetFontSize(font, 140);
        const auto text_surface = TTF_RenderText_Solid(font,
                                                          format_string("%d", context_->get_coins()).c_str(),
                                                          // TODO configurable.
                                                          SDL_Color{50, 250, 100, 0});
        if (text_surface == nullptr)
        {
            throw std::runtime_error(gs::format_string("Unable to render coin text.  %s", TTF_GetError()));
        }

        coin_texture_ = SDL_CreateTextureFromSurface(context_->get_renderer(), text_surface);
        SDL_FreeSurface(text_surface);
        context_->coins_clean();
    }

    SDL_Rect dest = SDL_Rect{pos_coin_box_x, pos_coin_box_y, coin_box_size_x, coin_box_size_y};

    if (context_->get_coins() > 9)
    {
        dest.x = dest.x - 10;
    }

    SDL_RenderCopy(context_->get_renderer(), coin_texture_, nullptr, & dest );
}

void gs::g2024::Slot_3Reel::render_main()
{
    auto asset_manager = context_->get_asset_manager();

    // == ALWAYS RENDERS ==========================================
    render_coins();

    // == DEPENDENT RENDERS =======================================
    switch(action_state_)
    {
        // RESTING RENDERS
        case Reel3GameActionState::WAITING:
            if (context_->get_coins() > 0)
            {
                TEXRENDERD(context_->get_asset_manager()->get_named_texture(texture_main_slot_spin_on), pos_spin_button_);
            }
            // SDL_RenderCopy(context_->get_renderer(), start_button_on->get_texture(), nullptr,
            //               reinterpret_cast<const SDL_Rect *>(&pos_spin_button));
            break;

            // SPINNING RENDERS
        case Reel3GameActionState::START_SPIN:
        case Reel3GameActionState::STOP_REEL_1:
        case Reel3GameActionState::ROLLING_REEL_1:
        case Reel3GameActionState::STOP_REEL_2:
        case Reel3GameActionState::ROLLING_REEL_2:
        case Reel3GameActionState::STOP_REEL_3:
        case Reel3GameActionState::ROLLING_REEL_3:
        case Reel3GameActionState::LANDING_3:
        case Reel3GameActionState::RESULT_WAIT:
        case Reel3GameActionState::RESULT:

            // NOP RENDERS
        case Reel3GameActionState::DONE:
            //TEXRENDERD(start_button_off,pos_spin_button);
            break;
    }

}

void gs::g2024::Slot_3Reel::Slot_3Reel::check_stop_time(const time_value random_length, const Reel3GameActionState next_state)
{
    if (next_stop_ < SDL_GetTicks())
    {
        next_stop_ = SDL_GetTicks() + random_length;
        action_state_ = next_state;
    }
}

std::string gs::g2024::Slot_3Reel::Slot_3Reel::get_random_spot()
{
    std::string spot;
    while (spot == "")
    {
        spot = all_spots_[random6_(rng_)];
        for (auto exclude : spots_exclude_random_)
        {
            if (spot == exclude)
            {
                spot = "";
                break;
            }
        }
    }
    return spot;
}

