/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file catalog.cpp
 *
 * Game catalog info for common.
 */

#include "../catalog.h"
#include "loader.h"

#include "slot_3reel.h"
#include "slot_4x4spot.h"
#include "match_15.h"
#include "pick_2.h"
#include "navigate.h"

// ===================================================================================================================
// GAME CATALOG
//

std::map<std::string, int> instance_map_2024 =
{
    { gs::catalog_game_slot_3reel, 0 },
    {gs::catalog_game_slot_match15, 1 },
    {gs::catalog_game_slot_pick2, 2 },
    { gs::catalog_game_slot_4x4spot, 3 },
    { gs::catalog_game_navigate, 4 }

};


std::shared_ptr<gs::GSGame> gs::get_instance_2024(const std::string& registration_name, const std::string& impl_name,
                                                const std::shared_ptr<GSContext>& context)
{
    if (instance_map_2024.count(impl_name) > 0) {
        switch(instance_map_2024[impl_name])
        {
        case 0: // catalog_game_splash_game
            return std::make_shared<g2024::Slot_3Reel>(registration_name, context);
        case 1: // catalog_game_slot_match15
            return std::make_shared<g2024::Match15>(registration_name, context);
        case 2: // catalog_game_slot_pick2
            return std::make_shared<g2024::Pick2>(registration_name, context);
        case 3: // catalog_game_slot_4x4spot
            return std::make_shared<g2024::Slot_4x4Spot>(registration_name, context);
        case 4: // catalog_game_navigate
            return std::make_shared<g2024::Navigate>(registration_name, context);
        default:
            break;
        }
    }
    return nullptr;
}
