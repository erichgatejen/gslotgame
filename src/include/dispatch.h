/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file dispatch.h
 *
 * Dispatch engine.  The primary loop lives in here.
 */

#ifndef GSLOTGAME_DISPATCH_H
#define GSLOTGAME_DISPATCH_H

#include <game.h>

#include "../../3rdparty/json.hpp"
using json = nlohmann::json;
#include <SDL.h>

#include "context.h"

namespace gs {

const std::string dispatcher_name = "dispatcher";

const SDL_Keycode default_coin_key = SDLK_c;
const SDL_Keycode special_quit_key = 'p';
const SDL_Keycode special_confirm_key = 'q';

class GSDispatch final
{

    std::shared_ptr<GSContext> context_;
    unsigned frame_done_time_{};
    char coin_key_;
    bool pending_quit_key;

    void prerender() const;
    void postrender() const;
    void complete_render() const;
    void reset_frame_time();
    void setup_coin_key();

public:
    GSDispatch() = default;
    ~GSDispatch() = default;

    void entry_point(std::shared_ptr<GSContext> context);

    GSGameState live_render(GSGameState state);
};

} // end namespace gs

#endif //GSLOTGAME_DISPATCH_H
