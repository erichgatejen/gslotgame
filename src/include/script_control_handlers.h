/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_control_handlers.h
 *
 *  Script engine.
 */

#ifndef GSLOTGAME_SCRIPT_CONTROL_HANDLERS_H
#define GSLOTGAME_SCRIPT_CONTROL_HANDLERS_H

#include <queue>

#include "context.h"

namespace gs {
void script_handler_transition(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_sleep(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_add_coin(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_freeze(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_unfreeze(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_signal(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_wait_signal(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);

const std::string freeze_name = "1___freeze___1";
} // end namespace gs


#endif //GSLOTGAME_SCRIPT_CONTROL_HANDLERS_H
