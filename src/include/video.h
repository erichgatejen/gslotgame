/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2022. 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file video.h
 *
 * Video player.
 */

#ifndef GSLOTGAME_VIDEO_H
#define GSLOTGAME_VIDEO_H

#include "config.h"
#include "context.h"

#include <SDL_mixer.h>

namespace gs {

const std::string video_file_suffix = ".ogv";
const std::string audio_file_suffix = ".wav";

struct Video
{
    std::string name;
    std::string path;
    pixel_size x_size;
    pixel_size y_size;
};

class VideoManager
{
    std::shared_ptr<GSContext> context_;
    std::FILE * file_;
    Mix_Music * music_;

    SDL_Rect target_rect_;
    SDL_Texture * target_texture_ = nullptr;

public:
    VideoManager(const std::shared_ptr<GSContext>& context) : context_(context), file_(nullptr), music_(nullptr),
                                                       target_rect_()
    {
    }

    void play_video(const Video& video, pixel_size x_loc, pixel_size y_loc);
    bool play_loop();

    SDL_Rect * get_rect() { return & target_rect_; }
    SDL_Texture * get_texture() const { return target_texture_; }

};

} // end namespace gs


#endif //GSLOTGAME_VIDEO_H
