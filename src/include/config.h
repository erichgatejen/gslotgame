/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file config.h
 *
 * Configuration.
 */

#ifndef GSLOTGAME_CONFIG_H
#define GSLOTGAME_CONFIG_H

#include <SDL_keycode.h>
#include <string>

#include "types.h"

#include "../../3rdparty/json.hpp"
using json = nlohmann::json;

namespace gs
{

// ===================================================================================================================
// Types

using directive = std::vector<std::string>;
using script = std::vector<directive>;

// ===================================================================================================================
// Hard config

constexpr uint number_audio_channels = 8;
constexpr uint frames_per_second = 60;
constexpr time_value milliseconds_per_frame = 1000 / frames_per_second;

constexpr char script_term_delimiter = ',';
constexpr char script_order_delimiter = ':';

// ===================================================================================================================
// Naming

const std::string config_file_name_main = "main.json";
const std::string config_file_name_assets = "assets.json";

// -- Common config
const std::string config_metadata = "metadata";
const std::string config_default = "default";
const std::string config_name = "name";

// -- Main config
const std::string config_main_resolution_x = "resolution_x";
const std::string config_main_resolution_y = "resolution_y";
const std::string config_main_entry_game   = "entry_game";
const std::string config_main_coin_key     = "coin_key";

// -- Assets config
const std::string config_assets_fonts = "fonts";
const std::string config_assets_backgrounds = "backgrounds";
const std::string config_assets_textures = "textures";
const std::string config_assets_audio = "audio";

// -- Game config
const std::string config_game_implementation = "implementation";
const std::string config_game_background = "background";
const std::string config_game_persist = "persist";
const std::string config_game_keys = "keys";
const std::string config_game_keys_signal = "signal";
const std::string config_game_keys_script = "script";
const std::string config_game_outcomes = "outcomes";
const std::string config_game_config = "config";

const std::string config_game_script = config_game_keys_script;

// ===================================================================================================================
// Configuration objects

// -- ConfigMain ----------------------------------------------------------------------------------------------

struct ConfigMain {

    // Passed
    std::filesystem::path   root_path;

    // Loaded
    pixel_size     x_resolution{}, y_resolution{};
    std::string    entry_game;
    char           coin_key = 0;

    explicit ConfigMain(const std::filesystem::path& path);
    ~ConfigMain() = default;

    void load(const std::filesystem::path& path);
};

// -- ConfigAssets ----------------------------------------------------------------------------------------------

struct ConfigNamedAsset
{
    std::string             name;
    std::filesystem::path   path;
};

struct ConfigAssetTextureTransparency
{
    ConfigNamedAsset      t;
    int                   r{}, g{}, b{};
};

class ConfigAssets {

public:

    // Loaded
    std::map<std::string, ConfigAssetTextureTransparency> textures;
    std::map<std::string, ConfigNamedAsset> backgrounds;
    std::map<std::string, ConfigNamedAsset> fonts;
    std::map<std::string, ConfigNamedAsset> audio;

    explicit ConfigAssets(const std::filesystem::path& path);
    ~ConfigAssets() = default;

    void load(const std::filesystem::path& path);
};

// -- ConfigGame ----------------------------------------------------------------------------------------------

struct ConfigGameKeys
{
    directive                signal;
    script                   key_script;
};

class ConfigGame {

public:
    std::string     game_name;
    std::string     implementation;
    std::string     background;
    bool            persist{};
    std::map<SDL_Keycode, std::shared_ptr<ConfigGameKeys>> keys;
    std::vector<directive> outcomes;
    std::map<std::string, std::string> configs;
    std::map<std::string, script>       scripts;

    explicit ConfigGame(const std::filesystem::path& path, const std::string& name);
    ~ConfigGame() = default;

    void load(const std::filesystem::path& path, const std::string& name);

    template<class T>
    T get_config(const std::string& name);

    script get_script(const std::string& name);
};

template<>
inline std::string ConfigGame::get_config<std::string>(const std::string& name)
{
    if (!configs.contains(name))
    {
        throw std::runtime_error("Game config for '" + game_name + "' does not contain config for '" + name + "'");
    }
    return configs[name];
}

template<>
inline int ConfigGame::get_config<int>(const std::string& name)
{
    int result;
    const auto val = get_config<std::string>(name);
    try
    {
        result = std::stoi(val);
    }
    catch (std::invalid_argument& iae)
    {
        throw std::runtime_error("Config point '" + name + "' for game '" + game_name + " not a valid integer.  value='" +
            val + "'");
    }
    catch (std::exception& e)
    {
        throw std::runtime_error("Config point '" + name + "for game '" + game_name + " could not be converted to an integer.  value='" +
        val + "'  cause: " + e.what());
    }
    return result;
}

} // end namespace gs

#endif //GSLOTGAME_CONFIG_H
