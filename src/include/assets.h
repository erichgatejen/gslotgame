/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file assets.h
 *
 *  Assets and handlers.
 *
 *  NOTES:
 *  - Asset names, either file or as named in the manager, may NOT have a ! character.
 *
 */

#ifndef GSLOTGAME_ASSETS_H
#define GSLOTGAME_ASSETS_H

#include <string>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#include "types.h"
#include "context.h"
#include "config.h"

namespace gs {

const std::string reserved_texture_name_snapshot = "_snapshot";

class GSTexture
{
    SDL_Texture * texture_;
    pixel_size width_;
    pixel_size height_;

public:
    GSTexture(const std::shared_ptr<GSContext>& context, std::string asset_path, int red = -1, int green = -1, int blue = -1);
    GSTexture(SDL_Texture * texture, pixel_size width, pixel_size height);
    ~GSTexture();

    [[nodiscard]] SDL_Texture * get_texture() const { return texture_; };
    [[nodiscard]] pixel_size get_width() const { return width_; };
    [[nodiscard]] pixel_size get_height() const { return height_; };

};

class GSAudio
{
    Mix_Chunk * chunk_;

public:
    GSAudio(const std::shared_ptr<GSContext>& context, const std::string& asset_path);
    ~GSAudio();

    [[nodiscard]] Mix_Chunk * get_chunk() const { return chunk_; }
};

class AssetManager
{
    std::shared_ptr<GSContext> context_;
    std::map<std::string, std::shared_ptr<GSTexture>> background_textures_;
    std::map<std::string, std::shared_ptr<GSTexture>> named_textures_;
    std::map<std::string, std::shared_ptr<GSAudio>> named_audio_;
    std::map<std::string, TTF_Font*> fonts_;
    TTF_Font * default_font_ = nullptr;

public:
    explicit AssetManager(const std::shared_ptr<GSContext>& context) { context_ = context; };
    virtual ~AssetManager() = default;

    void load(const std::shared_ptr<ConfigAssets>& config);
    void dispose_assets();

    std::map<std::string, std::shared_ptr<GSTexture>> get_background_textures() { return background_textures_; }
    std::shared_ptr<GSTexture> get_background_texture(const std::string& name);
    std::map<std::string, std::shared_ptr<GSTexture>> get_named_textures() { return named_textures_; }
    std::shared_ptr<GSTexture> get_named_texture(const std::string& name);
    std::map<std::string, std::shared_ptr<GSAudio>> get_named_audios() { return named_audio_; }
    std::shared_ptr<GSAudio> get_named_audio(const std::string& name);
    std::shared_ptr<GSTexture> register_dynamic_background(const std::string& name);
    void destroy_dynamic_background(const std::string& name);
    [[nodiscard]] TTF_Font * get_font() const { return default_font_; }
};


} // end namespace gs




#endif //GSLOTGAME_ASSETS_H
