/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script.h
 *
 *  Script engine.
 */

#ifndef GSLOTGAME_SCRIPT_H
#define GSLOTGAME_SCRIPT_H

#include <queue>

#include "context.h"
#include "config.h"
#include "objects.h"

namespace gs {

// ===================================================================================================================
// Script classes

class ScriptContext
{
    Uint32 delay_done_time_ = 0;

public:
    void start_delay(Uint32 ms);
    [[nodiscard]] Uint32 get_delay_done_time() const { return delay_done_time_; };
    void reset_delay() { delay_done_time_ = 0;};
};

class ScriptManager final
{
    std::shared_ptr<GSContext> context_;
    std::queue<script>         script_queue_;
    script  current_script_;
    uint    ip_;
    bool    is_running_;
    bool    is_frozen_;

    ScriptContext   script_context_;

    bool op();
    void process_globals(directive& d) const;

public:
    explicit ScriptManager(const std::shared_ptr<GSContext>& context): ip_(0), is_running_(false), is_frozen_(false)
    {
        context_ = context;
    }

    ~ScriptManager() = default;

    void run(const script& script);
    void slice();

};

// ===================================================================================================================
// Script utils

void throw_bad_directive(int index, const std::string& name, int line, const std::string& expected, const std::string& cause);

template <class T>
T get_directive(directive& d, const int index, const int line, const std::string& expected)
{
    return d[index];
}

template <>
std::string get_directive<std::string>(directive& d, const int index, const int line,
    const std::string& expected);

template <>
int get_directive<int>(directive& d, const int index, const int line,
    const std::string& expected);

} // end namespace gs

#endif //GSLOTGAME_SCRIPT_H
