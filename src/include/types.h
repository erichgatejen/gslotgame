/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file types.h
 *
 * General and common types
 */

#include <SDL_stdinc.h>

#ifndef GSLOTGAME_TYPES_H
#define GSLOTGAME_TYPES_H

namespace gs
{
    using pixel_size = int;

    using time_value = Uint32;
    #define GS_MAX_TIME SDL_MAX_UINT32

} // end namespace gs



#endif //GSLOTGAME_TYPES_H
