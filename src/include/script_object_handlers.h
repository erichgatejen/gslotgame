/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_object_handlers.h
 *
 *  Script engine.
 */

#ifndef GSLOTGAME_SCRIPT_OBJECT_HANDLERS_H
#define GSLOTGAME_SCRIPT_OBJECT_HANDLERS_H

#include <queue>

#include "context.h"

namespace gs {
std::shared_ptr<gs::GSObject> find_object(const std::shared_ptr<GSContext>& context, const std::string& object_name, int line,
                                          const std::string& name);

void script_handler_object_register(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_show(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_hide(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_move(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_resize(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_scale(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_move_action(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_object_deregister(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
} // end namespace gs


#endif //GSLOTGAME_SCRIPT_OBJECT_HANDLERS_H
