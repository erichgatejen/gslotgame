/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_object_handlers.h
 *
 *  Script engine.
 */

#ifndef GSLOTGAME_SCRIPT_MEDIA_HANDLERS_H
#define GSLOTGAME_SCRIPT_MEDIA_HANDLERS_H

#include <queue>

#include "context.h"

namespace gs {
std::shared_ptr<GSAudio> find_audio(const std::shared_ptr<GSContext>& context, const std::string& asset_name,
                                         const int line, const std::string& name);

void script_handler_background(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_play_sound(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_stop_sound_channel(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);
void script_handler_play_video(std::shared_ptr<GSContext> context, ScriptContext& sc, directive& d, int line);

} // end namespace gs


#endif //GSLOTGAME_SCRIPT_MEDIA_HANDLERS_H
