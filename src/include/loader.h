/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file loader.h
 *
 * Game loader.
 */


#ifndef GSLOTGAME_LOADER_H
#define GSLOTGAME_LOADER_H

#include <random>
#include "game.h"
#include "config.h"

namespace gs {

class GSLoader;

struct GSLoadedGame
{
    std::shared_ptr<ConfigGame> config;
    std::shared_ptr<GSGame> game;

    explicit GSLoadedGame(const std::shared_ptr<ConfigGame>& config, const std::shared_ptr<GSGame>& game);
};

//using game_name = std::string;
using game_map = std::map<std::string, std::shared_ptr<GSLoadedGame>>;

class GSLoader : public std::enable_shared_from_this<GSLoader>
{
    game_map currently_loaded_;
    //std::string current_listener_;
    std::shared_ptr<GSContext> context_;

public:
    explicit GSLoader(const std::shared_ptr<GSContext>& context) { context_ = context; };
    virtual ~GSLoader() = default;

    std::shared_ptr<GSLoadedGame> get_game(const std::string& name);
};

} // end namespace gs

#endif //GSLOTGAME_LOADER_H
