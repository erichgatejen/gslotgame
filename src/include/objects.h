/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file objects.h
 *
 *  Objects and handlers.
 */


#ifndef GSLOTGAME_OBJECTS_H
#define GSLOTGAME_OBJECTS_H

#include <string>
#include <variant>

#include <SDL.h>
#include <SDL_image.h>

#include "types.h"
#include "context.h"
#include "assets.h"
#include "util.h"

namespace gs {

enum class GSObjectType
{
    STATIC,
    CENTERED,
};

enum class GSObjectOrderType
{
    STATIC,
    MOVE,
    MIGRATE,
    SCALE,
    //RELEASE_PENDING,
    NONE
};

struct GSObjectScaler
{
    pixel_size target_x;
    pixel_size target_y;
    float rate_x;
    float rate_y;
    float current_x;
    float current_y;
};

struct GSObjectMover
{
    pixel_size target_x;
    pixel_size target_y;
    float rate_x;
    float rate_y;
    float current_x;
    float current_y;
};

struct GSObjectOrder
{
    GSObjectOrderType type;
    time_value start_time;
    time_value end_time = GS_MAX_TIME;
    std::variant<GSObjectScaler,GSObjectMover> oper;

    //static GSObjectOrder get_release_pending_order(time_value start_time);
    static GSObjectOrder get_scale_order(pixel_size start_x, pixel_size start_y, pixel_size target_x,
                                         pixel_size target_y, uint rate_x, time_value start_time,
                                         time_value end_time = GS_MAX_TIME);
    static GSObjectOrder get_move_order(pixel_size start_x, pixel_size start_y, pixel_size target_x,
                                     pixel_size target_y, uint rate_x, time_value start_time,
                                     time_value end_time = GS_MAX_TIME);
};

enum class GSObjectOrderState
{
    WAITING,
    ACTIVE,
    DONE
};

struct GSObjectOrderActive
{
    GSObjectOrderState state = GSObjectOrderState::WAITING;
    GSObjectOrder order;
    time_value last_time{};
};

using object_orders = std::vector<GSObjectOrder>;

class GSObject
{
    GSObjectType type_;
    std::shared_ptr<GSContext> context_;
    std::string name_;
    std::variant<std::shared_ptr<GSTexture>> texture_;
    std::shared_ptr<NumericallyOrderedList<GSObjectOrderActive, time_value>> order_queue_;
    uint priority_ = 1;

    bool show_;
    bool pending_action_ = false;
    int x_, y_, x_size_, y_size_, x_size_original_, y_size_original_;

    void object_action() const;
    void handle_order_action(GSObjectOrderActive * order, time_value fixed_time);
    void handle_order_action__scale(GSObjectOrderActive * order, time_value fixed_time);
    void handle_order_action__move(GSObjectOrderActive * order, time_value fixed_time);
    void order_action();

public:

    GSObject(const std::shared_ptr<GSContext>& context, const std::string& name, std::shared_ptr<GSTexture> texture, uint priority = 1);
    ~GSObject() = default;

    void config(const GSObjectType type) { type_ = type; }

    void pending_action(bool p = false) { pending_action_ = p; };
    void show(bool show = true);
    void hide();

    std::string get_name() const { return name_; }
    void move(const int x, const int y) { x_ = x; y_ = y; };
    int get_x() const { return x_; }
    int get_y() const { return y_; }
    [[nodiscard]] int get_x_size() const { return x_; }
    [[nodiscard]] int get_y_size() const { return y_; }
    void resize(const int x, const int y) { x_size_ = x; y_size_ = y; }
    void set_priority(const uint priority) { priority_ = priority; }
    uint get_priority() const { return priority_; }
    void set_texture(std::shared_ptr<GSTexture> texture) { texture_ = texture; }

    void reset();

    void accept_order(const GSObjectOrder & order);
    void accept_orders(object_orders & orders);

    void game_loop_action();

};

class ObjectManager
{
    std::shared_ptr<GSContext> context_;
    std::map<std::string, std::shared_ptr<GSObject>> object_map_;
    std::map<std::string,GSObjectType> type_name_2_enum_mapping;
    std::shared_ptr<NumericallyOrderedList<GSObjectOrderActive, time_value>> order_queue_;
    std::shared_ptr<NumericallyOrderedList<std::shared_ptr<GSObject>, uint>> priority_list_;

public:
    ObjectManager(const std::shared_ptr<GSContext>& context);
    virtual ~ObjectManager() = default;

    std::shared_ptr<GSObject> get_object(const std::string& name, const std::string& asset_name, bool reset,
                                         int x_pos, int y_pos, uint priority, int x_size = -1,
                                         int y_size = -1);
    std::shared_ptr<GSObject> get_object(const std::string& name, std::shared_ptr<GSTexture> texture, bool reset,
                                         int x_pos, int y_pos, uint priority, int x_size = -1,
                                         int y_size = -1);

    void register_object(const std::shared_ptr<GSObject>& object);
    void deregister_object(const std::string& name);
    void set_object_asset(const std::string& name, const std::string& asset_name);
    std::shared_ptr<GSObject> get(const std::string& name);
    bool is_registered(const std::string& name) const;
    GSObjectType get_type(const std::string& type_name);

    void game_loop_action() const;
};


} // end namespace gs


#endif //GSLOTGAME_OBJECTS_H
