/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file main.h
 *
 * Main context and entrypoint.
 */

#ifndef GSLOTGAME_MAIN_H
#define GSLOTGAME_MAIN_H

#endif //GSLOTGAME_MAIN_H
