/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file script_directive.h
 *
 *  Script engine directives.
 */

#ifndef GSLOTGAME_SCRIPT_DIRECTIVE_H
#define GSLOTGAME_SCRIPT_DIRECTIVE_H

#include <string>

namespace gs {

// ===================================================================================================================
// Script directives

using directive_name = std::string;

// TRANSITION [game name]
const directive_name directive_transition = "TRANSITION";

// OBJECT_REGISTER [object name] [asset name] [x pos] [y pos] [priority] [optional:type]
// [optional:type] STATIC or CENTERED
const directive_name directive_object_register = "OBJECT_REGISTER";

// BACKGROUND [background name]
const directive_name directive_background = "BACKGROUND";

// OBJECT_SHOW [object names...]
const directive_name directive_object_show = "OBJECT_SHOW";

// OBJECT_HIDE [object names...]
const directive_name directive_object_hide = "OBJECT_HIDE";

// OBJECT_MOVE [object name] [x pos] [y pos]
const directive_name directive_object_move = "OBJECT_MOVE";

// OBJECT_RESIZE [object name] [x size] [y size]
const directive_name directive_object_resize = "OBJECT_RESIZE";

// OBJECT_SCALE [object name] [start x size] [start y size] [target x size] [target y size] [rate] [optional: start time] [optional: end time]
// Automatically SHOWs the object.
// [start time] [end time] in milliseconds from issuance.
const directive_name directive_object_scale = "OBJECT_SCALE";

// OBJECT_MOVE_ACTION [object name] [start x pos] [start y pos] [target x pos] [target y pos] [rate] [optional: start time] [optional: end time]
// Automatically SHOWs the object.
// [start time] [end time] in milliseconds from issuance.
const directive_name directive_object_move_action = "OBJECT_MOVE_ACTION";

// OBJECT_DEREGISTER [object names...]
const directive_name directive_object_deregister = "OBJECT_DEREGISTER";

// PLAY_SOUND [asset name] [channel #] [optional: loops]
// [optional: loops] Use -1 for indefinite looping.
const directive_name directive_play_sound = "PLAY_SOUND";

// PLAY_VIDEO [name] [path without file extensions] [x_loc] [y_loc] [x_size] [y_size]
const directive_name directive_play_video = "PLAY_VIDEO";

// STOP_SOUND_CHANNEL [channel #]
const directive_name directive_stop_sound_channel = "STOP_SOUND_CHANNEL";

// SLEEP [ milliseconds ]
const directive_name directive_sleep = "SLEEP";

// ADD_COIN [ # coins ]
const directive_name directive_add_coin = "ADD_COIN";

// FREEZE
const directive_name directive_freeze = "FREEZE";

// UNFREEZE
const directive_name directive_unfreeze = "UNFREEZE";

// SIGNAL [ signal specification ]
const directive_name directive_signal = "SIGNAL";

// WAIT_SIGNAL [ signal specification ]
const directive_name directive_wait_signal = "WAIT_SIGNAL";

} // end namespace gs

#endif //GSLOTGAME_SCRIPT_DIRECTIVE_H
