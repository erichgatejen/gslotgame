/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file game.h
 *
 * Game base.
 */

#ifndef GSLOTGAME_GAME_H
#define GSLOTGAME_GAME_H

#include "../../3rdparty/json.hpp"
using json = nlohmann::json;
#include <SDL.h>

#include "context.h"
#include "assets.h"

namespace gs {


// ===================================================================================================================
// GSGAME
//

enum class GSGameState
{
    NEW,
    RENDERING_QUIET,    // Ignore inputs
    EVENTS_INJECTED,    // Same as RENDERING_QUIET but events were injected into the game from outside.  The game should render as normal waiting for the EVENT_COMPLETE signal, where it should return to normal behavior.
    EVENTS_INJECTED_COMPLETE,
    RENDERING_ACTIVE,   // Only accept inputs here.
    OUTCOME,
    PENDING_TRANSITION,
    PENDING_OUTCOME,
    DONE,
    ALLDONE,
    WAIT_START,
    FROZEN
};

class GSGame // : public GSListener
{
protected:
    GSGameState state_;
    std::shared_ptr<GSContext> context_;
    std::string registration_name_;
    std::string name_current_background_;
    std::string name_texture_background_;
    std::shared_ptr<GSTexture> texture_background_;
    std::shared_ptr<GSTexture> frozen_texture_background_;

    virtual GSGameState game_loop_impl(GSGameState state) = 0;

    // DO not get any config from the context.  Use the passed config only.
    virtual void init_impl(GSGameState state, std::shared_ptr<ConfigGame> config) = 0;

public:
    GSGame(std::string registration_name, std::shared_ptr<GSContext> context);
    virtual ~GSGame() = default;
    void init(GSGameState state, const std::shared_ptr<ConfigGame>& config);       // Called by loader.

    GSGameState game_loop(GSGameState state);

    void set_background(const std::string& background_name) { name_current_background_ = background_name; };
    std::shared_ptr<GSTexture> get_background();
    [[nodiscard]] GSGameState get_game_state() const { return state_; }
    void freeze_game(const std::shared_ptr<GSTexture>& bt) { frozen_texture_background_ = bt;}
    [[nodiscard]] bool is_game_frozen() const { return frozen_texture_background_ != nullptr; }
    void unfreeze_game() { frozen_texture_background_ = nullptr; }

    // Interface
    virtual void activate() = 0;                        // Called when context switches to game.
    virtual void deactivate() = 0;                      // Called when context switches to another game.
    virtual bool offer_signal(const directive signal) = 0;
    virtual bool offer_key(SDL_Keycode keycode) = 0;
    //virtual void yield_context_transition() = 0;
    //virtual void yield_context_outcome() = 0;

};

#define GSGAME_VIRTUAL_OVERRIDES  virtual GSGameState game_loop_impl(const GSGameState state) override; \
    virtual void activate() override; \
    virtual void deactivate() override; \
    virtual void init_impl(GSGameState state, std::shared_ptr<ConfigGame> config) override; \
    virtual bool offer_signal(const directive signal) override; \
    virtual bool offer_key(SDL_Keycode keycode) override; \
    //virtual void event_rcv(GSEvent & event) override; \
    //virtual void yield_context_transition() override; \
    //virtual void yield_context_outcome() override; \
    //virtual bool is_active_listener() override; \

} // end namespace gs

#endif //GSLOTGAME_GAME_H
