/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file main.h
 *
 * Main entrypoint.
 */

#ifndef GSLOTGAME_GSLOTGAME_H
#define GSLOTGAME_GSLOTGAME_H

#include "config.h"
#include "context.h"

namespace gs {

class GSlotGame {

    std::shared_ptr<GSContext> context_;

    // Once per app run.
    void init() const;

    // Could be recycled.
    void setup();
    void load_assets() const;
    void load_games();
    void run_loop();

    // Once per app run.
    void close();

public:
    GSlotGame() = default;
    virtual ~GSlotGame() = default;

    void entry_point(const std::filesystem::path& path);
};

} // end namespace gs

#endif //GSLOTGAME_GSLOTGAME_H
