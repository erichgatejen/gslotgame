/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2024
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file util.h
 *
 * Utilities
 */

#ifndef GSLOTGAME_UTIL_H
#define GSLOTGAME_UTIL_H

#include <string>

#include "types.h"

#include "../../3rdparty/json.hpp"
using json = nlohmann::json;

namespace gs
{

// ===================================================================================================================
// JSON/MAP Config

json load_config(const std::string& name, const std::filesystem::path& path);
int config_get_int(json& j, const std::string& key);
std::string config_get_string(json& j, const std::string& key);

template <typename T>
T config_map_get(std::map<std::string, T> & m, const std::string& key)
{
    if (m.count(key) < 1)
    {
        throw std::runtime_error("Configuration (in map) does not contain key " + key);
    }
    return m[key];
}

template <typename T>
T config_map_get(std::map<std::string, std::string> & m, const std::string& key)
{
    if (m.count(key) < 1)
    {
        throw std::runtime_error("Configuration (in map) does not contain key " + key);
    }
    return m[key];
}

// ===================================================================================================================
// Formatting

template<typename... T>
std::string format_string(const char * format, T... args)
{
    const size_t size = snprintf(nullptr, 0, format, args...);
    std::string result;
    result.reserve(size + 1);
    result.resize(size);
    snprintf(& result[0], size + 1, format, args...);
    return result;
}

std::string lazy_to_upper(std::string_view view);

template <typename T, typename N>
struct NumericallyOrderedListNode
{
    std::shared_ptr<NumericallyOrderedListNode> next;
    N numeric;
    T data;
    bool marked = false;

    NumericallyOrderedListNode(const N n, T d) : numeric(n), data(d) {}
};

// ===================================================================================================================
// Numerically Ordered List

template <typename T, typename N>
class NumericallyOrderedListWalker;

template <typename T, typename N>
class NumericallyOrderedList {

    std::shared_ptr<NumericallyOrderedListNode<T,N>> head_;
    T null_data_;

public:
    explicit NumericallyOrderedList(T null_data) : null_data_(null_data) {}

    friend class NumericallyOrderedListWalker<T,N>;

    void insert(const N numeric, T data)
    {
        auto new_node = std::make_shared<NumericallyOrderedListNode<T,N>>(numeric, data);

        if (head_ == nullptr)
        {
            head_ = new_node;
        }
        else if (head_->numeric > numeric)
        {
            new_node->next = head_;
            head_ = new_node;
        }
        else
        {
            auto current = head_;
            while (true)
            {
                if (current->next == nullptr)
                {
                    current->next = new_node;
                    break;
                }
                if ((numeric >= current->numeric) && (numeric < current->next->numeric))
                {
                    new_node->next = current->next;
                    current->next = new_node;
                    break;
                }
                current = current->next;
            }
        }
    }

    void remove(T data)
    {
        if (head_ != nullptr)
        {
            if (head_->data == data)
            {
                head_ = head_->next;
            }
            else
            {
                auto node = head_;
                while(node->next != nullptr)
                {
                    if (node->next->data == data)
                    {
                        node->next = node->next->next;
                        break;
                    }
                    node = node->next;
                }
            }
        }

    }

    bool has_more()
    {
        if (head_ != nullptr)
        {
            return true;
        }
        return false;
    }

    N first_numeric_value()
    {
        N result = 0;
        if (head_ != nullptr)
        {
            result = head_->numeric;
        }
        return result;
    }

    T pop_first()
    {
        T result = null_data_;
        if (head_ != nullptr)
        {
            result = head_->data;
            head_ = head_->next;
        }
        return result;
    }
};

template <typename T, typename N>
class NumericallyOrderedListWalker
{
    std::shared_ptr<gs::NumericallyOrderedList<T,N>> list_;
    std::shared_ptr<NumericallyOrderedListNode<T,N>> current_;
    std::shared_ptr<NumericallyOrderedListNode<T,N>> working_;

public:
    explicit NumericallyOrderedListWalker(std::shared_ptr<gs::NumericallyOrderedList<T,N>> list) : list_(list)
    {
        current_ = list->head_;
    }

    T * next()
    {
        if (current_ == nullptr)
        {
            return & list_->null_data_;
        }

        working_ = current_;
        current_ = current_->next;
        return & working_->data;
    }

    bool has_more()
    {
        if (current_ == nullptr)
        {
            return false;
        }
        return true;
    }

    void remove()
    {
        working_->marked = true;
    }

    void done()
    {
        if (list_->head_ == nullptr) return;

        while ((list_->head_ != nullptr) && (list_->head_->marked))
        {
            list_->head_ =  list_->head_->next;
        }

        auto rover = list_->head_;
        while (rover != nullptr)
        {
           if ((rover->next != nullptr) && (rover->next->marked))
           {
               rover->next = rover->next->next;
           }
           else
           {
               rover = rover->next;
           }
        }
    }

};

// ===================================================================================================================
// General

bool ends_with(std::string const & value, std::string const & ending);
std::vector<std::string> split(const std::string& text, char delim);


} // end namespace gs

#endif //GSLOTGAME_UTIL_H
