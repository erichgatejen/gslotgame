/****************************************************************************************************************
 * Game Slot System.
 *
 * \copyright (c) Erich P Gatejen 2021
 * \license Boost Software License - Version 1.0 - August 17th, 2003
 * \author Erich Gatejen
 *
 * \file context.h
 *
 * Main context.
 */

#ifndef GSLOTGAME_CONTEXT_H
#define GSLOTGAME_CONTEXT_H

#include <map>
#include <SDL.h>

#include "config.h"

const std::string gs_global_flag = "$";
const std::string gs_global_flag_num = "%";

namespace gs {

    class VideoManager;
    struct GSLoadedGame;

    enum class GSSystemState {
    NEW,
    INIT_COMPLETE,
    LOAD_COMPLETE,
    RUN_LOOP,
    QUIT_ORDERED,
    CLOSE_COMPLETE
};

// Abstracting this because it will get more complicated in the future.
struct GSVideoContext
{
    SDL_Rect rect{};
    SDL_Texture * texture = nullptr;

    void set(SDL_Rect r, SDL_Texture * t);
};

class GSGame;
class AssetManager;
class ScriptManager;
class ObjectManager;
class GSLoader;
//class ObjectManager;
//class EventManager;
class GSContext : public std::enable_shared_from_this<GSContext>  {

    // Config
    std::shared_ptr<ConfigMain> main_config_;

    // One time
    SDL_Window * window_ = nullptr;
    SDL_Renderer * renderer_ = nullptr;

    // Reloadable.
    std::shared_ptr<AssetManager> assets_manager_;
    std::shared_ptr<ObjectManager> object_manager_;
    std::shared_ptr<ScriptManager> script_manager_;
    std::shared_ptr<VideoManager> video_manager_;
    std::shared_ptr<GSLoader> loader_;

    std::shared_ptr<GSLoadedGame> current_game_;

    std::shared_ptr<GSVideoContext> video_context_;

    // State
    GSSystemState state_ = GSSystemState::NEW;
    uint coins_ = 0;
    bool coin_spoiled = true;
    std::string current_signal_wait_;

    // Globals
    std::map<std::string,std::string> global_;
    std::map<std::string,int> global_num_;

public:
    explicit GSContext(const std::shared_ptr<ConfigMain>& main_config, std::shared_ptr<GSVideoContext> video_context);
    virtual ~GSContext();
    void init();

    void dispose_assets() const;

    void set_window(SDL_Window * window);
    void set_renderer(SDL_Renderer * renderer);

    SDL_Window * get_window() const { return window_; }
    SDL_Renderer * get_renderer() const { return renderer_; }
    std::shared_ptr<AssetManager> get_asset_manager() { return assets_manager_; }
    std::shared_ptr<ObjectManager> get_object_manager() { return object_manager_; }
    std::shared_ptr<ScriptManager> get_script_manager() { return script_manager_; }
    std::shared_ptr<VideoManager> get_video_manager() { return video_manager_; }
    std::shared_ptr<GSLoader> get_loader() { return loader_; }

    std::shared_ptr<GSGame> get_game() const;
    std::shared_ptr<GSLoadedGame> get_loaded_game() const;    // includes configuration.
    void switch_to_game(const std::string& name);

    std::shared_ptr<ConfigMain> get_main_config() { return main_config_; }

    void set_state(const GSSystemState state) { state_ = state; }
    GSSystemState get_state() const { return state_; }
    void order_quit() { state_ = GSSystemState::QUIT_ORDERED; }
    std::string get_root_path() const { return main_config_->root_path; }

    uint get_coins() const { return coins_; }
    void add_coins(const uint coins) { coins_ += coins; coin_spoiled = true; }
    void remove_coins(uint number);
    bool are_coins_spoiled() const { return coin_spoiled; };
    void coins_clean() { coin_spoiled = false; };

    void set_global(const std::string& name, const std::string& value) { global_[name] = value; }
    std::string get_global(const std::string& name);

    // The use case for these has gone away, plus they should be done with templates instead.
    void set_global_num(const std::string& name, const int value) { global_num_[name] = value; }
    int get_global_num(const std::string& name);

    void wait_signal(const std::string& signal) { current_signal_wait_ = signal; }
    std::string get_wait_signal() { return current_signal_wait_; }
};

#define CGSCONFIG  context_->get_config()
#define CGSGAME context_->get_game()->get_config()
#define CGSGAME_MY_NAME CGSGAME->name

} // end namespace gs

#endif //GSLOTGAME_CONTEXT_H
