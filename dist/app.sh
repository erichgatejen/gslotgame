#!/usr/bin/env sh

rm -Rf *.app

mkdir GslotGame.app
mkdir GslotGame.app/Contents
mkdir GslotGame.app/Contents/MacOS
mkdir GslotGame.app/Contents/Resources

cp Info.plist GslotGame.app/Contents
cp icon.icns GslotGame.app/Contents/Resources
cp ../build/cmake-build-debug/gslotgame GslotGame.app/Contents/MacOS/GslotGame


