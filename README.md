# Gslotgame

What is this?  Every Christmas Tonya gets a game of some sort, rather than just presents.  The presents are the prizes.   
These games are physical, electronic or both.  When this project was started (2021) the game was a working video slot 
machine, complete with a physical console to drop play coins and interact with the game.  It has since become a more
general content and game engine.

The first version took me weekend to create.  I had a choice between learning a game engine, not knowing if it would
actually be suitable for my needs, or just belt this out.  The priority has always been speed of content creation over
the engine itself.

# Notes 2024

For the 2024 game, I turned to this again.  There were two major changes.  First, I ported it to linux, specifically 
Ubuntu 23.  I wanted to run it on a [MAME](https://www.mamedev.org/) box I had connected to a large screen in the 
rec room.  I developed on the same version on a laptop, which worked fine, but when put on the other box, it constantly
crashed.  In the end, the laptop ran he game because I had no time to resolve the crash.  

Second, scripting replaced a lot of the in-code workflow for the subgames.  I will likely extend this to replacing ALL 
the workflow in the next version.  Additionally, I have an entire python-ish scripting language I implemented
for a closed-source project (just not ready for prime time, and I'm not sure if it will ever be).  I think it might
be well suited for this sort of stuff and should be easy to include.

The changes and fixes made it easier to add content, but unfortunately I only had three days left after getting this
working.  So as always, as time runs out, stuff gets hardcoded and code quality drops.  This year was no exception.

# Notes 2022

The game2022 branch has the second version which is less slot machine focused and more about interactivity.  Yes, you 
will find a slot like part to it too.

The biggest change was to move a lot of the game flow logic out of code and into configuration.  This stuff caused
the big hardcoded snarl in the 2021 version.  Also the configuration itself changed from csv to json.  While it made
my personal method of design to implementation a bit more difficult, overall it is a lot easier to understand, see
what is happening and work with.

The base engine and the individual subgames are have been separated.  The subgames are still static linked, but I've done
it in a way that it should be relatively easy to turn them into loadable modules.

The most interesting new feature of the engine is that it can play video.  Unfortunately I had a lot of trouble getting
the video and audio if they were in the same OGG container.  The audio has to be provided as a separate WAV file, but 
at least they are in sync.

Finally, as before as the deadline drew near, I had to cut some corners and make some hacks.  There was no delaying
Christmas.

# Notes 2021

The game2021 branch has the first version which was the slot machine.

You might notice though that this slot machine has predetermined outcome?  Well what kind of fun would Christmas be if 
you didn't win any prizes?  So yeah, all the games since the second one have been rigged.  This game is the 14th.  
Some ask if this was my greatest one yet, but I don't think so.  I had a Jeopardy setup complete with the question/answer 
board, consoles for the players to hit the button, lights flash, sound and the works.  I recruited two guests to play 
along with her.  Naturally, they were given preordained answers which won them just enough money to buy one of the gag 
gifts.

Like I said, it's rigged.  It wouldn't take a lot to unrig it.

You will see code that implements the various features but does not provide the assets associated.  Franky, I just don't
have time to vet the licenses for them.  I did provide the csv configuration file as well another small example.  
Likewise, you will notice the code is of varying quality.  As I ran out of time, it went downhill and more and more 
corners were cut or things were hardcoded.

Why a csv file for configuration?  Since the very first game, I have always used Microsoft excel to design the games.
So it was easier to match the designed outcomes and various desired features to the actual configuration.

So why am I providing this?  I was looking for examples I could use to make this game and there really wasn't much.
Maybe this could help someone else trying to do something similar.  

It is likely I will maintain this over the long haul, as I would use it for another game if suitable.  But those changes 
would likely to happen in the runup to Christmas.

# Building

The application is statically linked, so make sure you have the static library versions of any listed below.

It assumes the following where build and installed source:
SDL (>=2.0.16)              On ubuntu I had to install it with apt (to gather dependencies) and then build/install from source.
SDL_image (>=2.0.5)
SDL_mixer (>=2.0.4)
Freetype (>=2.11.1)  
SDL_ttf (>=2.0.15)  Some of the newer versions don't build properly and no one is fixing it.  2.0.18 worked for me.
fmt string formatting library (9.1.0)  Newer versions go to war with std::fmt and I don't have time to fix that.
libogg (>=1.3.5)
libtheora (>=1.1.1)
libvorbis (>=1.3.7)
libmpg123 (>=1.31.1)

I had no problem using the latest versions of these in a more recent build. 

ZLIB library needs to be installed (usually already is).

I used cmake through CLion, so it should be fairly easy to get it compiling and running.

The dist/ directory doesn't really do anything.  I went that route when I had trouble getting it running on OSX in
native screen mode.  

# Porting

I originally started this in windows, but had to switch or OSX.  Additionally, the latest version works on linux (tested)
on Ubuntu 23).  It shouldn't be too hard getting it working on windows.  In fact, probably easier than resolving all the
version conflicts between ubuntu and the dependencies.

# License

This is licensed under Boost Software License - Version 1.0 - August 17th, 2003.  I picked it because it is very permissive
and most c++ users understand it.

